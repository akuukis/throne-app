const path = require('path');

module.exports = (packageDir) => ({
  root: true,
  parser: "@typescript-eslint/parser",
  plugins: [
    "@typescript-eslint",
    "import",
    "react"
  ],
  parserOptions: {
    tsconfigRootDir: path.resolve(__dirname, '..', 'packages', packageDir),
    project: ["./tsconfig.json"],
  },
  extends: [
    "eslint:recommended",
    "plugin:@typescript-eslint/eslint-recommended",
    "plugin:@typescript-eslint/recommended",
    "plugin:@typescript-eslint/recommended-requiring-type-checking",
    "plugin:react/recommended",
    "plugin:react-hooks/recommended",
    "plugin:import/recommended",
    "plugin:import/typescript",
  ],
  settings: {
    react: {
      version: "detect",
    },
    "import/resolver": {
      webpack: {
        // Because webpack config is async, need to copy/paste the relevant parts.
        config: {
          resolve: {
            extensions: [ ".js", ".jsx", ".ts", ".tsx" ],
            alias: {
              '@coinmetro/app': path.resolve(__dirname, '..', 'packages', 'app', 'src'),
              '@coinmetro/kyc-legal': path.resolve(__dirname, '..', 'packages', 'kyc-legal', 'src'),
              '@coinmetro/kyc-natural': path.resolve(__dirname, '..', 'packages', 'kyc-natural', 'src'),
              '@coinmetro/services': path.resolve(__dirname, '..', 'packages', 'services', 'src'),
              '@coinmetro/shared': path.resolve(__dirname, '..', 'packages', 'shared', 'src'),
            }
          }
        }
      }
    },
    "import/external-module-folders": [
      "node_modules",
      "src",  // Don't import parents via `../`, but instead path by `src/..`.
    ]
  },
  env: {
    "browser": true,
    "node": true,
    "es2017": true,
  },
  globals: {
    "TradingView": "readonly",
    "Intercom": "readonly",
    "dataLayer": "readonly"
  },
  rules: {
    // "class-name-casing": ["error", { allowUnderscorePrefix: true }],  // TODO: use "naming-conventions".
    "@typescript-eslint/explicit-function-return-type": "off",
    "@typescript-eslint/indent": ["error", 2, {
      "ignoredNodes": [
        "JSXAttribute", "JSXSpreadAttribute",  // conflict with "react/jsx-indent-props" - https://github.com/typescript-eslint/typescript-eslint/issues/415
      ],
      "SwitchCase": 1,  // had to set explicitly due "ignoredNodes" line above.
    }],
    "@typescript-eslint/no-misused-promises": ["error", { checksVoidReturn: false }],
    "@typescript-eslint/no-unused-vars": ["error", { args: "none", ignoreRestSiblings: true }],
    "@typescript-eslint/prefer-optional-chain": "error",
    "@typescript-eslint/require-await": "off",
    "@typescript-eslint/no-explicit-any": "off",
    "@typescript-eslint/no-namespace": "off",
    "@typescript-eslint/member-delimiter-style": "error",
    "@typescript-eslint/consistent-type-definitions": ["error", "interface"],  // TS nudges towards interface (https://www.typescriptlang.org/docs/handbook/2/everyday-types.html#differences-between-type-aliases-and-interfaces)
    "no-control-regex": "off",
    "react/display-name": "off",
    "react/jsx-indent": ["error", 2, { checkAttributes: true, indentLogicalExpressions: true }],
    "react/jsx-indent-props": ["error", 2],
    "react/no-unescaped-entities": ["error", { "forbid": [">", "}"] }],
    "react/prop-types": ["error", { skipUndeclared: true }],
    "react-hooks/rules-of-hooks": "error",
    "react-hooks/exhaustive-deps": "error",

    // Organize import section.
    "sort-imports": ["error", { "ignoreDeclarationSort": true, "ignoreMemberSort": false }],  // only order within line.
    "no-multiple-empty-lines": ["error", { "max": 2, "maxEOF": 1, "maxBOF": 0 }],
    "import/no-webpack-loader-syntax": "error",
    "import/no-named-as-default": "off",  // zustand examples show `import create from 'zustand'`.
    "import/no-self-import": "error",
    // "import/no-cycle": "error",  // TODO: solve redux cycles.
    "import/no-useless-path-segments": "error",
    // "import/no-extraneous-dependencies": "error",  // TODO: fix "crypto" and "util".
    "import/no-relative-parent-imports": "warn",
    // "import/no-unused-modules": ["warn", {"unusedExports": true, "missingExports": true}],
    "import/first": "error",
    "import/newline-after-import": ["error", { "count": 2 }],
    "import/no-useless-path-segments": "error",
    "import/order": ["error", {
      "newlines-between": "always",
      "groups": [ "builtin", "external", "internal", "parent", "unknown", "sibling"],
      "pathGroups": [
        { "pattern": "react", "group": "external", "position": "before" },
        { "pattern": `@coinmetro/${packageDir}/assets/**`, "group": "internal" },
        { "pattern": `@coinmetro/${packageDir}/**`, "group": "internal", "position": "after" },
        { "pattern": "@coinmetro/**", "group": "internal", "position": "before" },
      ],
      "pathGroupsExcludedImportTypes": ["builtin"],
      "alphabetize": {order: "asc", caseInsensitive: true},
    }],
    "no-restricted-imports": ["error", {
      "paths": [{
        "name": "react-redux",
        "importNames": ["useSelector", "useDispatch"],
        "message": "Please import from \"@coinmetro/app/redux/*\" instead."
      }, {
        "name": "@apollo/client",
        "importNames": ["useQuery"],
        "message": "Please import from \"src/common\" instead."
      }, {
        "name": "@apollo/client",
        "importNames": ["useMutation"],
        "message": "Please import from \"src/api/useMutation*\" instead."
      }]
    }],

    // TODO: stricter types.
    "@typescript-eslint/no-unsafe-call": "off",
    "@typescript-eslint/no-unsafe-member-access": "off",
    "@typescript-eslint/no-unsafe-assignment": "off",
    "@typescript-eslint/no-floating-promises": "off",
    "@typescript-eslint/no-unsafe-return": "off",
    "@typescript-eslint/explicit-module-boundary-types": "off",
    "@typescript-eslint/restrict-template-expressions": "off",
    "@typescript-eslint/restrict-plus-operands": "off",
    "@typescript-eslint/unbound-method": "off",
  },
  "overrides": [
    {
      "files": ["*.ts", "*.tsx"],
      "rules": {
        "keyword-spacing": "error",
        "quotes": ["error", "double"],
        "semi": "error",
        "arrow-spacing": "error",
        "object-curly-spacing": ["error", "always"],
        "comma-dangle": ["error", "always-multiline"],
        "@typescript-eslint/require-await": "error",
        "react/prop-types": "off",
        "react/jsx-curly-brace-presence": ["error", { props: "never", children: "never" }],
        "react/jsx-tag-spacing": ["error", {
          closingSlash: "never",
          beforeSelfClosing: "always",
          afterOpening: "never",
          beforeClosing: "never"
        }],
        "react/jsx-wrap-multilines": ["error", {
          declaration: "parens-new-line",
          assignment: "parens-new-line",
          return: "parens-new-line",
          arrow: "parens-new-line",
          condition: "parens-new-line",
          logical: "parens-new-line",
          prop: "parens-new-line"
        }],
        "react/function-component-definition": [
          "error",
          {
            namedComponents: "function-declaration",
            unnamedComponents: "arrow-function"
          },
        ],
        "react/jsx-first-prop-new-line": ["error", "multiline-multiprop"],
        "react/jsx-max-props-per-line": ["error", { "when": "multiline" }],
        "react/jsx-closing-bracket-location": ["error", "line-aligned"],  // default 'tag-aligned' conflicts with "@typescript-eslint/indent".
        "react/forbid-dom-props": ["warn", { "forbid": ["style"] }],
        "react/forbid-component-props": ["warn", { "forbid": ["style"] }],
      },
    },
    {
      "files": ["*.stories.tsx"],
      "rules": {
        "react/function-component-definition": [
          "error",
          {
            namedComponents: "function-declaration",
            unnamedComponents: "arrow-function"
          },
        ],
        "react/forbid-dom-props": "off",
        "react/forbid-component-props": "off",
      },
    },
  ],
});
