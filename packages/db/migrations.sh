DEFAULT_STATE='state-Throne9-init'
DEFAULT_PASSWORD='qwerty'
project_folder="$(basename $(dirname $(dirname $(dirname $(realpath \"${0}\")))))"
DEFAULT_NETWORK="${project_folder}_network_dev"

STATE="${1:-$DEFAULT_STATE}"  # state-Throne9-init
PASSWORD="${2:-$DEFAULT_PASSWORD}"  # qwerty
NETWORK="${3:-$DEFAULT_NETWORK}"  # throne-app_network_dev
echo $0 $STATE $PASSWORD $NETWORK
docker run -i --network $NETWORK --rm mysql mysql -h t-db-mysql -u root --password=$PASSWORD -e 'DROP DATABASE `tronis`;'
docker run -i --network $NETWORK --rm mysql mysql -h t-db-mysql -u root --password=$PASSWORD -e 'CREATE DATABASE `tronis`;'
docker run -i --network $NETWORK --rm mysql mysql -h t-db-mysql -u root --password=$PASSWORD tronis < ../db/migrations/0-limited-db-user.sql
docker run -i --network $NETWORK --rm mysql mysql -h t-db-mysql -u root --password=$PASSWORD tronis < ../db/migrations/1-tables.sql
docker run -i --network $NETWORK --rm mysql mysql -h t-db-mysql -u root --password=$PASSWORD tronis < ../db/migrations/2-foreign-keys.sql
docker run -i --network $NETWORK --rm mysql mysql -h t-db-mysql -u root --password=$PASSWORD tronis < ../db/migrations/3a-constants.sql
docker run -i --network $NETWORK --rm mysql mysql -h t-db-mysql -u root --password=$PASSWORD tronis < ../db/migrations/3b-constants-qr-codes.sql
docker run -i --network $NETWORK --rm mysql mysql -h t-db-mysql -u root --password=$PASSWORD tronis < ../db/migrations/4-updates.sql
docker run -i --network $NETWORK --rm mysql mysql -h t-db-mysql -u root --password=$PASSWORD tronis < ../db/migrations/$STATE.sql
