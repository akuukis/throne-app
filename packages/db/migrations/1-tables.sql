-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: t-db-mysql:3306
-- Generation Time: Jul 30, 2021 at 08:10 AM
-- Server version: 8.0.22
-- PHP Version: 7.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tronis`
--

-- --------------------------------------------------------

-- --------------------------------------------------------

--
-- Table structure for table `camps`
--

CREATE TABLE `camps` (
                         `id` int NOT NULL,
                         `latitude` float DEFAULT NULL,
                         `longitude` float DEFAULT NULL,
                         `title` varchar(30) DEFAULT NULL,
                         `display_id` int DEFAULT NULL,
                         `resource_id` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `castles`
--

CREATE TABLE `castles` (
                           `id` int NOT NULL,
                           `title` varchar(30) NOT NULL,
                           `latitude` float NOT NULL,
                           `longitude` float NOT NULL,
                           `team_id` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `characters`
--

CREATE TABLE `characters` (
                              `id` int NOT NULL,
                              `qr_id` int NOT NULL,
                              `owner_user_id` int NOT NULL,
                              `owner_team_id` int NOT NULL,
                              `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                              `tribute_time` timestamp NULL DEFAULT NULL,
                              `tribute_user_id` int DEFAULT NULL,
                              `tribute_team_id` int DEFAULT NULL,
                              `tribute_period_id` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `character_kills`
--

CREATE TABLE `character_kills` (
                                   `id` int NOT NULL,
                                   `team_id` int NOT NULL,
                                   `scored_team_id` int NOT NULL,
                                   `amount` int NOT NULL,
                                   `period_id` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `devices`
--

CREATE TABLE `devices` (
                           `id` int NOT NULL,
                           `name` varchar(20) DEFAULT NULL,
                           `team_id` int DEFAULT NULL,
                           `is_admin` tinyint NOT NULL DEFAULT '0',
                           `auth_token` varchar(60) DEFAULT NULL COMMENT 'should be stored on device and sent to identify device',
                           `is_enabled` tinyint NOT NULL DEFAULT '0',
                           `is_marketplace` tinyint NOT NULL DEFAULT '0',
                           `created` timestamp NULL,
                           `comment` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `equipment_allocated`
--

CREATE TABLE `equipment_allocated` (
                                       `id` int NOT NULL,
                                       `team_id` int NOT NULL,
                                       `user_id` int NOT NULL,
                                       `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                                       `add` int NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `equipment_bought`
--

CREATE TABLE `equipment_bought` (
                                    `id` int NOT NULL,
                                    `team_id` int NOT NULL,
                                    `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                                    `user_id` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE `events` (
                          `id` int NOT NULL,
                          `name` varchar(30) NOT NULL,
                          `code` varchar(20) NOT NULL,
                          `state` enum('inactive','active','spent') NOT NULL DEFAULT 'inactive',
                          `period_id` int DEFAULT NULL,
                          `current_weight` int DEFAULT '0',
                          `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `events_votes`
--

CREATE TABLE `events_votes` (
                                `id` int NOT NULL,
                                `team_id` int DEFAULT NULL,
                                `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                                `event_id` int NOT NULL,
                                `weight_id` int NOT NULL,
                                `period_id` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `events_weights`
--

CREATE TABLE `events_weights` (
                                  `id` int NOT NULL,
                                  `name` varchar(30) NOT NULL,
                                  `code` varchar(20) NOT NULL,
                                  `weight` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `flag_status`
--

CREATE TABLE `flag_status` (
                               `id` int NOT NULL,
                               `name` varchar(20) NOT NULL,
                               `score_reason_id` int NOT NULL,
                               `self_score_reason_id` int NOT NULL,
                               `next_status` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `flag_status_log`
--

CREATE TABLE `flag_status_log` (
                                   `id` int NOT NULL,
                                   `team_id` int NOT NULL,
                                   `changer_team_id` int NOT NULL,
                                   `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                                   `current_status_id` int NOT NULL,
                                   `action` enum('score','register') NOT NULL,
                                   `period_id` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- --------------------------------------------------------

--
-- Table structure for table `periods`
--

CREATE TABLE `periods` (
                           `id` int NOT NULL,
                           `title` varchar(20) NOT NULL,
                           `started` timestamp NULL DEFAULT NULL,
                           `ended` timestamp NULL DEFAULT NULL,
                           `next_periods_id` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `qr_categories`
--

CREATE TABLE `qr_categories` (
                                 `id` int NOT NULL,
                                 `title` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `qr_codes`
--

CREATE TABLE `qr_codes` (
                            `id` int NOT NULL,
                            `qrid` varchar(10) NOT NULL,
                            `is_used` tinyint NOT NULL DEFAULT '0',
                            `category_id` int DEFAULT NULL,
                            `owner_team_id` int DEFAULT NULL,
                            `last_scan_id` int DEFAULT NULL,
                            `uuid` int NOT NULL,
                            `details` varchar(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `qr_scans`
--

CREATE TABLE `qr_scans` (
                            `id` int NOT NULL,
                            `qr_id` int DEFAULT NULL,
                            `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                            `device_id` int DEFAULT NULL,
                            `scanner_team_id` int DEFAULT NULL,
                            `scanner_user_id` int DEFAULT NULL,
                            `type_id` int DEFAULT NULL,
                            `remove` tinyint NOT NULL DEFAULT '0',
                            `is_fail` tinyint NOT NULL DEFAULT '0',
                            `qr_raw` varchar(10) NOT NULL,
                            `error` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `qr_scan_types`
--

CREATE TABLE `qr_scan_types` (
                                 `id` int NOT NULL,
                                 `title` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
                                 `is_enabled` tinyint NOT NULL DEFAULT '0',
                                 `category_id` int NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `recipe`
--

CREATE TABLE `recipe` (
                          `id` int NOT NULL,
                          `title` int DEFAULT NULL,
                          `created_first_id` int DEFAULT NULL,
                          `score_reason_id` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `recipe2resource`
--

CREATE TABLE `recipe2resource` (
                                   `recipe_id` int NOT NULL,
                                   `resource_id` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `recipe_log`
--

CREATE TABLE `recipe_log` (
                              `id` int NOT NULL,
                              `recipe_id` int NOT NULL,
                              `team_id` int NOT NULL,
                              `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                              `starts` DATETIME NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `expansions`
--

CREATE TABLE `expansions` (
                            `id` int NOT NULL,
                            `show_code` varchar(16) NOT NULL,
                            `reward_code` varchar(16) NOT NULL,
                            `owner_team_id` int DEFAULT NULL,
                            `rewarded_team_id` int DEFAULT NULL,
                            `showed` timestamp NULL DEFAULT NULL,
                            `rewarded` timestamp NULL DEFAULT NULL,
                            `camp_id` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `resources`
--

CREATE TABLE `resources` (
                             `id` int NOT NULL,
                             `type_id` int NOT NULL,
                             `team_id` int DEFAULT NULL,
                             `last_scan_id` int DEFAULT NULL,
                             `camp_id` int DEFAULT NULL,
                             `qr_id` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `resource_action`
--

CREATE TABLE `resource_action` (
                                   `id` int NOT NULL,
                                   `name` varchar(40) NOT NULL,
                                   `from` enum('outside','available','locked','spent') NOT NULL DEFAULT 'outside',
                                   `to` enum('outside','available','locked','spent') NOT NULL DEFAULT 'outside'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `resource_log`
--

CREATE TABLE `resource_log` (
                                `id` int NOT NULL,
                                `team_id` int NOT NULL,
                                `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                                `action_id` int NOT NULL,
                                `amount` int NOT NULL,
                                `details` varchar(60) NOT NULL,
                                `resource_type_id` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `resource_total`
--

CREATE TABLE `resource_total` (
                                  `id` int NOT NULL,
                                  `team_id` int NOT NULL,
                                  `resource_type_id` int NOT NULL,
                                  `total_amount` int NOT NULL COMMENT 'resources including spent, available, locked',
                                  `available_amount` int NOT NULL COMMENT 'currently available for transactions',
                                  `locked_amount` int NOT NULL COMMENT 'locked (taken out of base?, currently in some weird state)',
                                  `spent_amount` int NOT NULL COMMENT 'Spent. Not included anything that is tradet to other teams.'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `resource_types`
--

CREATE TABLE `resource_types` (
                                  `id` int NOT NULL,
                                  `name` varchar(20) NOT NULL,
                                  `subtype` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
                                  `qr_prefix` varchar(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `scores`
--

CREATE TABLE `scores` (
                          `id` int NOT NULL,
                          `type_id` int NOT NULL,
                          `reason_id` int NOT NULL,
                          `team_id` int NOT NULL,
                          `period_id` int NOT NULL,
                          `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                          `details` varchar(40) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `score_reasons`
--

CREATE TABLE `score_reasons` (
                                 `id` int NOT NULL,
                                 `title` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
                                 `amount` int NOT NULL,
                                 `type_id` int NOT NULL,
                                 `assign_period` int NOT NULL DEFAULT '1' COMMENT 'time amount during what points will be assigned. divide with points, min increment is 1 point'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `score_totals`
--

CREATE TABLE `score_totals` (
                                `id` int NOT NULL,
                                `type_id` int NOT NULL,
                                `period_id` int NOT NULL,
                                `score` int NOT NULL,
                                `team_id` int NOT NULL,
                                `last_update` timestamp NOT NULL,
                                `rank` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `score_type2period`
--

CREATE TABLE `score_type2period` (
                                     `id` int NOT NULL,
                                     `score_type_id` int NOT NULL,
                                     `period_id` int NOT NULL,
                                     `currentTotal` int NOT NULL DEFAULT '0',
                                     `target` int DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `score_types`
--

CREATE TABLE `score_types` (
                               `id` int NOT NULL,
                               `title` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
                            `id` int NOT NULL,
                            `description` TEXT NOT NULL,
                            `title` varchar(30) NOT NULL,
                            `value` varchar(50) NOT NULL,
                            `code` varchar(20) NOT NULL,
                            `is_system` tinyint NOT NULL DEFAULT '0' COMMENT 'disable admin panel editing'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `teams`
--

CREATE TABLE `teams` (
                         `id` int NOT NULL,
                         `title` varchar(20) DEFAULT NULL,
                         `color` varchar(6) DEFAULT NULL,
                         `flag_status_id` int NOT NULL,
                         `available_eq` int NOT NULL DEFAULT '0',
                         `flag_registration_id` int DEFAULT NULL,
                         `coinbox` INT NOT NULL DEFAULT '50'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
                         `id` int NOT NULL,
                         `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
                         `display_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
                         `team_id` int DEFAULT NULL,
                         `qr_id` int DEFAULT NULL,
                         `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                         `email` varchar(255) DEFAULT NULL,
                         `is_admin` tinyint NOT NULL DEFAULT '0',
                         `admin_password` varchar(70) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
                         `qr_code` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
                         `equipment_points` int DEFAULT '0',
                         `equipment_has_phoenix` tinyint DEFAULT 0,
                         `auth_token` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `workshop_activations`
--

CREATE TABLE `workshop_activations` (
                                        `id` int NOT NULL PRIMARY KEY AUTO_INCREMENT,
                                        `size_id` int NOT NULL,
                                        `team_id` int NOT NULL,
                                        `variant` int NOT NULL,
                                        `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                                        `cancelled` timestamp NULL DEFAULT NULL COMMENT 'if cancelled - add set to 1',
                                        `scored_points` int NOT NULL DEFAULT '0',
                                        `last_updated` timestamp NULL DEFAULT NULL,
                                        `finished` timestamp NULL DEFAULT NULL COMMENT 'if null - in progress. if data - is finished',
                                        `started` timestamp NULL DEFAULT NULL,
                                        `price_details` TEXT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- --------------------------------------------------------

--
-- Table structure for table `workshop_type`
--

CREATE TABLE `workshop_size` (
                                 `id` int NOT NULL PRIMARY KEY AUTO_INCREMENT,
                                 `ip_amount` int NOT NULL,
                                 `score_reason_id` int NOT NULL,
                                 `cooldown` int NOT NULL DEFAULT '40',
                                 `wood_cost` int NOT NULL DEFAULT '0',
                                 `resource_cost` int NOT NULL DEFAULT '0',
                                 `coin_cost` int NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `camps`
--
ALTER TABLE `camps`
    ADD PRIMARY KEY (`id`),
  ADD KEY `resource_id` (`resource_id`);

--
-- Indexes for table `castles`
--
ALTER TABLE `castles`
    ADD PRIMARY KEY (`id`),
  ADD KEY `team_id` (`team_id`);

--
-- Indexes for table `characters`
--
ALTER TABLE `characters`
    ADD PRIMARY KEY (`id`),
  ADD KEY `owner_team_id` (`owner_team_id`),
  ADD KEY `owner_user_id` (`owner_user_id`),
  ADD KEY `tribute_team_id` (`tribute_team_id`),
  ADD KEY `tribute_user_id` (`tribute_user_id`),
  ADD KEY `period_id` (`tribute_period_id`);

--
-- Indexes for table `character_kills`
--
ALTER TABLE `character_kills`
    ADD PRIMARY KEY (`id`),
  ADD KEY `team_id` (`team_id`),
  ADD KEY `scored_team_id` (`scored_team_id`);

--
-- Indexes for table `devices`
--
ALTER TABLE `devices`
    ADD PRIMARY KEY (`id`),
  ADD KEY `team_id` (`team_id`);

--
-- Indexes for table `equipment_allocated`
--
ALTER TABLE `equipment_allocated`
    ADD PRIMARY KEY (`id`),
  ADD KEY `team_id` (`team_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `equipment_bought`
--
ALTER TABLE `equipment_bought`
    ADD PRIMARY KEY (`id`),
  ADD KEY `team_id` (`team_id`);

--
-- Indexes for table `events`
--
ALTER TABLE `events`
    ADD PRIMARY KEY (`id`),
  ADD KEY `period_id` (`period_id`);

--
-- Indexes for table `events_votes`
--
ALTER TABLE `events_votes`
    ADD PRIMARY KEY (`id`),
  ADD KEY `event_id` (`event_id`),
  ADD KEY `team_id` (`team_id`),
  ADD KEY `weight_id` (`weight_id`),
  ADD KEY `period_id` (`period_id`);

--
-- Indexes for table `events_weights`
--
ALTER TABLE `events_weights`
    ADD PRIMARY KEY (`id`),
  ADD KEY `weight_id` (`weight`);

--
-- Indexes for table `flag_status`
--
ALTER TABLE `flag_status`
    ADD PRIMARY KEY (`id`),
  ADD KEY `score_reason_id` (`score_reason_id`),
  ADD KEY `self_score_reason_id` (`self_score_reason_id`),
  ADD KEY `next_status` (`next_status`);

--
-- Indexes for table `flag_status_log`
--
ALTER TABLE `flag_status_log`
    ADD PRIMARY KEY (`id`),
  ADD KEY `team_id` (`team_id`),
  ADD KEY `change_team_id` (`changer_team_id`),
  ADD KEY `period_id` (`period_id`),
  ADD KEY `current_status_id` (`current_status_id`);

--
-- Indexes for table `periods`
--
ALTER TABLE `periods`
    ADD PRIMARY KEY (`id`),
  ADD KEY `next_periods_id` (`next_periods_id`);

--
-- Indexes for table `qr_categories`
--
ALTER TABLE `qr_categories`
    ADD PRIMARY KEY (`id`);

--
-- Indexes for table `qr_codes`
--
ALTER TABLE `qr_codes`
    ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uuid` (`uuid`),
  ADD KEY `qrid` (`qrid`),
  ADD KEY `category_id` (`category_id`),
  ADD KEY `last_scan_id` (`last_scan_id`),
  ADD KEY `owner_team_id` (`owner_team_id`);

--
-- Indexes for table `qr_scans`
--
ALTER TABLE `qr_scans`
    ADD PRIMARY KEY (`id`),
  ADD KEY `team_id` (`scanner_team_id`),
  ADD KEY `qr_id` (`qr_id`),
  ADD KEY `user_id` (`scanner_user_id`),
  ADD KEY `qr_scans_ibfk_1` (`device_id`),
  ADD KEY `type_id` (`type_id`);

--
-- Indexes for table `qr_scan_types`
--
ALTER TABLE `qr_scan_types`
    ADD PRIMARY KEY (`id`),
  ADD KEY `category_id` (`category_id`);

--
-- Indexes for table `recipe`
--
ALTER TABLE `recipe`
    ADD PRIMARY KEY (`id`),
  ADD KEY `score_reason_id` (`score_reason_id`),
  ADD KEY `created_first_id` (`created_first_id`);

--
-- Indexes for table `recipe2resource`
--
ALTER TABLE `recipe2resource`
    ADD UNIQUE KEY `recipe_id` (`recipe_id`,`resource_id`),
    ADD KEY `recipe2resource_ibfk_1` (`resource_id`);

--
-- Indexes for table `recipe_log`
--
ALTER TABLE `recipe_log`
    ADD PRIMARY KEY (`id`),
  ADD KEY `recipe_id` (`recipe_id`),
  ADD KEY `team_id` (`team_id`);

--
-- Indexes for table `expansions`
--
ALTER TABLE `expansions`
    ADD PRIMARY KEY (`id`),
  ADD KEY `rewarded_team_id` (`rewarded_team_id`),
  ADD KEY `owner_team_id` (`owner_team_id`);

--
-- Indexes for table `resources`
--
ALTER TABLE `resources`
    ADD PRIMARY KEY (`id`),
  ADD KEY `camp_id` (`camp_id`),
  ADD KEY `last_scan_id` (`last_scan_id`),
  ADD KEY `team_id` (`team_id`),
  ADD KEY `type_id` (`type_id`),
  ADD KEY `qr_id` (`qr_id`);

--
-- Indexes for table `resource_action`
--
ALTER TABLE `resource_action`
    ADD PRIMARY KEY (`id`);

--
-- Indexes for table `resource_log`
--
ALTER TABLE `resource_log`
    ADD PRIMARY KEY (`id`),
  ADD KEY `team_id` (`team_id`,`resource_type_id`),
  ADD KEY `resource_type_id` (`resource_type_id`),
  ADD KEY `action_id` (`action_id`);

--
-- Indexes for table `resource_total`
--
ALTER TABLE `resource_total`
    ADD PRIMARY KEY (`id`),
  ADD KEY `resource_total_ibfk_1` (`team_id`),
  ADD KEY `resource_type_id` (`resource_type_id`);

--
-- Indexes for table `resource_types`
--
ALTER TABLE `resource_types`
    ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scores`
--
ALTER TABLE `scores`
    ADD PRIMARY KEY (`id`),
  ADD KEY `team_id` (`team_id`),
  ADD KEY `reason_id` (`reason_id`),
  ADD KEY `points_ibfk_3` (`type_id`),
  ADD KEY `period_id` (`period_id`);

--
-- Indexes for table `score_reasons`
--
ALTER TABLE `score_reasons`
    ADD PRIMARY KEY (`id`),
  ADD KEY `point_type` (`type_id`);

--
-- Indexes for table `score_totals`
--
ALTER TABLE `score_totals`
    ADD PRIMARY KEY (`id`),
  ADD KEY `team_id` (`team_id`),
  ADD KEY `type_id` (`type_id`),
  ADD KEY `era_id` (`period_id`);

--
-- Indexes for table `score_type2period`
--
ALTER TABLE `score_type2period`
    ADD PRIMARY KEY (`id`),
  ADD KEY `score_type_id` (`score_type_id`),
  ADD KEY `season_id` (`period_id`);

--
-- Indexes for table `score_types`
--
ALTER TABLE `score_types`
    ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
    ADD PRIMARY KEY (`id`),
  ADD KEY `title` (`code`) USING BTREE;

--
-- Indexes for table `teams`
--
ALTER TABLE `teams`
    ADD PRIMARY KEY (`id`),
  ADD KEY `flag_status_id` (`flag_status_id`),
  ADD KEY `teams_ibfk_2` (`flag_registration_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
    ADD PRIMARY KEY (`id`),
  ADD KEY `users_ibfk_1` (`team_id`),
  ADD KEY `qr_id` (`qr_id`);

CREATE TABLE `feature_switchboard` ( `id` INT NOT NULL AUTO_INCREMENT , `title` VARCHAR(255) NOT NULL , `status` TINYINT NOT NULL DEFAULT '0' , `details` VARCHAR(255) NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;
ALTER TABLE `feature_switchboard` ADD `category` VARCHAR(10) NOT NULL DEFAULT 'cron' AFTER `details`;
--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `camps`
--
ALTER TABLE `camps`
    MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `castles`
--
ALTER TABLE `castles`
    MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `characters`
--
ALTER TABLE `characters`
    MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `character_kills`
--
ALTER TABLE `character_kills`
    MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `devices`
--
ALTER TABLE `devices`
    MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `equipment_allocated`
--
ALTER TABLE `equipment_allocated`
    MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `equipment_bought`
--
ALTER TABLE `equipment_bought`
    MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `events`
--
ALTER TABLE `events`
    MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `events_votes`
--
ALTER TABLE `events_votes`
    MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `events_weights`
--
ALTER TABLE `events_weights`
    MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `flag_status`
--
ALTER TABLE `flag_status`
    MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `flag_status_log`
--
ALTER TABLE `flag_status_log`
    MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `periods`
--
ALTER TABLE `periods`
    MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `qr_categories`
--
ALTER TABLE `qr_categories`
    MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `qr_codes`
--
ALTER TABLE `qr_codes`
    MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `qr_scans`
--
ALTER TABLE `qr_scans`
    MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `qr_scan_types`
--
ALTER TABLE `qr_scan_types`
    MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `recipe`
--
ALTER TABLE `recipe`
    MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `recipe_log`
--
ALTER TABLE `recipe_log`
    MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `expansions`
--
ALTER TABLE `expansions`
    MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `resources`
--
ALTER TABLE `resources`
    MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `resource_action`
--
ALTER TABLE `resource_action`
    MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `resource_log`
--
ALTER TABLE `resource_log`
    MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `resource_total`
--
ALTER TABLE `resource_total`
    MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `resource_types`
--
ALTER TABLE `resource_types`
    MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `scores`
--
ALTER TABLE `scores`
    MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `score_reasons`
--
ALTER TABLE `score_reasons`
    MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `score_totals`
--
ALTER TABLE `score_totals`
    MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `score_type2period`
--
ALTER TABLE `score_type2period`
    MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `score_types`
--
ALTER TABLE `score_types`
    MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
    MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `teams`
--
ALTER TABLE `teams`
    MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
    MODIFY `id` int NOT NULL AUTO_INCREMENT;


-- new / migrations 2021.07.30-1
ALTER TABLE `users` ADD `sex` ENUM('male','female') NOT NULL DEFAULT 'male' AFTER `auth_token`;

-- new / migrations 2021.07.30-2
CREATE TABLE `feast_token` ( `id` INT NOT NULL AUTO_INCREMENT , `created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP , `team_id` INT NOT NULL , `is_used` TINYINT NULL DEFAULT '0' , `used_at` TIMESTAMP NULL DEFAULT NULL, PRIMARY KEY (`id`)) ENGINE = InnoDB;

ALTER TABLE `feast_token` ADD FOREIGN KEY (`team_id`) REFERENCES `teams`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `characters` CHANGE `qr_id` `charcode` INT NOT NULL;

ALTER TABLE `characters` ADD `charkey` INT NOT NULL AFTER `tribute_period_id`;

CREATE TABLE `character_faileds` ( `id` INT NOT NULL AUTO_INCREMENT , `charcode` INT NOT NULL , `user_id` INT NOT NULL , `created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP , PRIMARY KEY (`id`)) ENGINE = InnoDB;

-- new / migrations 2021.08.03

ALTER TABLE `characters` ADD `death_registered_at` TIMESTAMP NULL AFTER `charkey`;

ALTER TABLE `characters` ADD `death_season_id` INT NULL AFTER `death_registered_at`;

ALTER TABLE `feast_token` ADD `size` INT NOT NULL DEFAULT '1' AFTER `used_at`;

ALTER TABLE `expansions` ADD `assignment_period_id` INT NULL AFTER `camp_id`;

ALTER TABLE `feast_token` ADD `period_id` INT NOT NULL AFTER `size`;

ALTER TABLE `scores` ADD `score_time` TIMESTAMP NULL AFTER `details`;

ALTER TABLE `score_reasons` CHANGE `assign_period` `assign_period` INT NULL DEFAULT NULL COMMENT 'time amount during what points will be assigned. divide with points, min increment is 1 point';

ALTER TABLE `score_totals` ADD `event_points` INT NOT NULL DEFAULT '0' AFTER `rank`;

ALTER TABLE `score_type2period` ADD `overflow` INT NOT NULL DEFAULT '0' AFTER `target`, ADD `event` INT NOT NULL DEFAULT '0' AFTER `overflow`;

ALTER TABLE `recipe` CHANGE `title` `title` VARCHAR(40) NULL DEFAULT NULL;

alter table feast_token add recipe_id int null DEFAULT null;

alter table teams add castle_id int null DEFAULT null;

alter table castles add color varchar(16) null;

ALTER TABLE `score_totals` ADD UNIQUE `no_duplicate_scores` (`type_id`, `period_id`, `team_id`);

--
-- Constraints for dumped tables
--

COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
