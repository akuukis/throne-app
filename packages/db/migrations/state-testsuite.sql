SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

-- game max length was reduced from 100 to 95 but testsuite takes 97min, and I'm too lazy to squeeze testsuite shorter.
INSERT INTO `settings` (`id`, `description`, `title`, `value`, `code`, `is_system`) VALUES
    (29, 'number\r\n\r\nMinutes of minimum season length (scoring cant start earlier!)'                                                                                                , 'Season min length'            , '65'                 , 'season-min'          , '0');

INSERT INTO `periods` (`id`, `title`, `started`, `ended`, `next_periods_id`) VALUES
    (1, '1. Season', NULL, NULL, 2),
    (2, '2. Season', NULL, NULL, 3),
    (3, '3. Season', NULL, NULL, 4),
    (4, '4. Season', NULL, NULL, NULL),
    (99, '0. Season', NULL, NULL, NULL);

-- First season score target values
INSERT INTO `score_type2period` (`id`, `score_type_id`, `period_id`, `currentTotal`, `target`, `overflow`, `event`) VALUES
    (NULL, '2', '1', '0', '60', '0', '0'),
    (NULL, '3', '1', '0', '50', '0', '0'),
    (NULL, '4', '1', '0', '70', '0', '0');

INSERT INTO `camps` (`id`, `latitude`, `longitude`, `title`, `display_id`, `resource_id`) VALUES
    -- food
    ( 1, NULL, NULL, 'Rough Berry'        ,  1, 10),
    ( 4, NULL, NULL, 'Savory Cherries'    ,  4, 7),
    (10, NULL, NULL, 'Yeasty Fungi'       , 10, 9),
    (13, NULL, NULL, 'The Green Water'    , 13, 8),
    (19, NULL, NULL, 'Hexagon Candy'      , 19, 5),
    -- iron
    ( 2, NULL, NULL, 'Adamantium Well'    ,  2, 3),
    ( 5, NULL, NULL, 'Lost Ingot'         ,  5, 3),
    -- wood
    ( 3, NULL, NULL, 'Celluloose'         ,  3, 1),
    ( 6, NULL, NULL, 'Bamboozal'          ,  6, 1);

INSERT INTO `expansions` (`id`, `show_code`, `reward_code`, `owner_team_id`, `rewarded_team_id`, `showed`, `rewarded`, `camp_id`, `assignment_period_id`) VALUES
    ( 1, 'MASHES'   ,'PERCENT', NULL, NULL, NULL, NULL, '1', NULL),
    ( 2, 'TESTATE'  ,'FASHION', NULL, NULL, NULL, NULL, '1', NULL),
    ( 3, 'HOTSHOT'  ,'THEATER', NULL, NULL, NULL, NULL, '1', NULL),
    ( 5, 'HOOHAS'   ,'DILEMMA', NULL, NULL, NULL, NULL, '2', NULL),
    ( 6, 'OSTEOMATA','CONTROL', NULL, NULL, NULL, NULL, '2', NULL),
    ( 7, 'STEMMATA' ,'EYEBROW', NULL, NULL, NULL, NULL, '2', NULL),
    ( 9, 'STEMMA'   ,'SURFACE', NULL, NULL, NULL, NULL, '3', NULL),
    (10, 'SMOOTHE'  ,'TRAGEDY', NULL, NULL, NULL, NULL, '3', NULL),
    (11, 'HEATHS'   ,'ADVISER', NULL, NULL, NULL, NULL, '3', NULL),
    (13, 'TATTOO'   ,'REVENGE', NULL, NULL, NULL, NULL, '4', NULL),
    (14, 'MESHES'   ,'COLLEGE', NULL, NULL, NULL, NULL, '4', NULL),
    (15, 'HEMOSTATS','PUDDING', NULL, NULL, NULL, NULL, '4', NULL),
    (17, 'TESTATES' ,'SUPPORT', NULL, NULL, NULL, NULL, '5', NULL),
    (18, 'TOMATOE'  ,'CERTAIN', NULL, NULL, NULL, NULL, '5', NULL),
    (19, 'SHAMES'   ,'SECTION', NULL, NULL, NULL, NULL, '5', NULL),
    (21, 'HOTSEAT'  ,'GLACIER', NULL, NULL, NULL, NULL, '6', NULL),
    (22, 'HOTSHOE'  ,'RELEASE', NULL, NULL, NULL, NULL, '6', NULL),
    (23, 'SHEATH'   ,'FEATHER', NULL, NULL, NULL, NULL, '6', NULL),
    (36, 'MOTMOTS'  ,'SOCIETY', NULL, NULL, NULL, NULL, '10', NULL),
    (37, 'STOMATA'  ,'EXPLOIT', NULL, NULL, NULL, NULL, '10', NULL),
    (38, 'HOMMOS'   ,'SPEAKER', NULL, NULL, NULL, NULL, '10', NULL),
    (48, 'TATTOOS'  ,'PERSIST', NULL, NULL, NULL, NULL, '13', NULL),
    (49, 'TEASETS'  ,'OPINION', NULL, NULL, NULL, NULL, '13', NULL),
    (50, 'EMOTES'   ,'REALITY', NULL, NULL, NULL, NULL, '13', NULL),
    (78, 'OSTEOMA'  ,'EXPLOIT', NULL, NULL, NULL, NULL, '19', NULL),
    (79, 'SHAMMASH' ,'SAUSAGE', NULL, NULL, NULL, NULL, '19', NULL),
    (81, 'SOMATA'   ,'PEASANT', NULL, NULL, NULL, NULL, '19', NULL);

-- for testsuite only (on top of 2 random expansions)
UPDATE `expansions` SET `owner_team_id` = '1', `showed` = '1970-01-01 00:00:02', `assignment_period_id` = '1' WHERE `id` = '1';
UPDATE `expansions` SET `owner_team_id` = '2', `showed` = '1970-01-01 00:00:02', `assignment_period_id` = '1' WHERE `id` = '5';
UPDATE `expansions` SET `owner_team_id` = '3', `showed` = '1970-01-01 00:00:02', `assignment_period_id` = '1' WHERE `id` = '9';


INSERT INTO `castles` (`id`, `title`, `latitude`, `longitude`, `team_id`, `color`) VALUES
    ('1', 'D akmeņi', NULL, NULL, NULL, NULL),
    ('2', 'R kalns', NULL, NULL, NULL, NULL),
    ('3', 'A izcirtuma stūris', NULL, NULL, NULL, NULL);

INSERT INTO `teams` (`id`, `castle_id`, `title`, `color`, `flag_status_id`, `available_eq`, `flag_registration_id`) VALUES
    (1, '1', 'Snakes', 'green', 3, NULL, NULL),
    (2, '2', 'Bears', 'purple', 3, NULL, NULL),
    (3, '3', 'Lynx', 'gold', 3, NULL, NULL);

INSERT INTO `resource_total` (`id`, `team_id`, `resource_type_id`, `total_amount`, `available_amount`, `locked_amount`, `spent_amount`) VALUES
    (NULL, 1,  1, 66, 66, 0, 0),
    (NULL, 1,  2, 66, 66, 0, 0),
    (NULL, 1,  3, 66, 66, 0, 0),
    (NULL, 1,  4, 66, 66, 0, 0),
    (NULL, 1,  5, 60, 60, 0, 0),
    (NULL, 1,  6, 66, 66, 0, 0),
    (NULL, 1,  7, 60, 60, 0, 0),
    (NULL, 1,  8, 60, 60, 0, 0),
    (NULL, 1,  9, 60, 60, 0, 0),
    (NULL, 1, 10, 60, 60, 0, 0),
    (NULL, 2,  1, 66, 66, 0, 0),
    (NULL, 2,  2, 66, 66, 0, 0),
    (NULL, 2,  3, 66, 66, 0, 0),
    (NULL, 2,  4, 66, 66, 0, 0),
    (NULL, 2,  5, 60, 60, 0, 0),
    (NULL, 2,  6, 66, 66, 0, 0),
    (NULL, 2,  7, 60, 60, 0, 0),
    (NULL, 2,  8, 60, 60, 0, 0),
    (NULL, 2,  9, 60, 60, 0, 0),
    (NULL, 2, 10, 60, 60, 0, 0),
    (NULL, 3,  1, 66, 66, 0, 0),
    (NULL, 3,  2, 66, 66, 0, 0),
    (NULL, 3,  3, 66, 66, 0, 0),
    (NULL, 3,  4, 66, 66, 0, 0),
    (NULL, 3,  5, 60, 60, 0, 0),
    (NULL, 3,  6, 66, 66, 0, 0),
    (NULL, 3,  7, 60, 60, 0, 0),
    (NULL, 3,  8, 60, 60, 0, 0),
    (NULL, 3,  9, 60, 60, 0, 0),
    (NULL, 3, 10, 60, 60, 0, 0);


INSERT INTO `feast_token` (`id`, `created`, `team_id`, `is_used`, `used_at`, `size`, `period_id`, `recipe_id`) VALUES
    (1, 1, '1', '0', NULL, '3', 1, NULL),
    (2, 1, '2', '0', NULL, '3', 1, NULL),
    (3, 1, '3', '0', NULL, '3', 1, NULL);

INSERT INTO `recipe` (`id`, `title`, `created_first_id`, `score_reason_id`) VALUES
    (21, NULL, NULL, 32),
    (22, NULL, NULL, 32),
    (23, NULL, NULL, 32),
    (24, NULL, NULL, 32),
    (25, NULL, NULL, 32),
    (26, NULL, NULL, 32),
    (27, NULL, NULL, 32),
    (28, NULL, NULL, 32),
    (29, NULL, NULL, 32),
    (30, NULL, NULL, 32),
    (31, NULL, NULL, 32),
    (32, NULL, NULL, 32),
    (33, NULL, NULL, 32),
    (34, NULL, NULL, 32),
    (35, NULL, NULL, 32),
    (36, NULL, NULL, 33),
    (37, NULL, NULL, 33),
    (38, NULL, NULL, 33),
    (39, NULL, NULL, 33),
    (40, NULL, NULL, 33),
    (41, NULL, NULL, 33),
    (42, NULL, NULL, 34);

-- 15 + 6 + 1
INSERT INTO `recipe2resource` (`recipe_id`, `resource_id`) VALUES
    (21, 5), (21, 6), (21, 7), (21, 8),
    (22, 5), (22, 6), (22, 7),          (22, 9),
    (23, 5), (23, 6), (23, 7),                   (23, 10),
    (24, 5), (24, 6),          (24, 8), (24, 9),
    (25, 5), (25, 6),          (25, 8),          (25, 10),
    (26, 5), (26, 6),                   (26, 9), (26, 10),
    (27, 5),          (27, 7), (27, 8), (27, 9),
    (28, 5),          (28, 7), (28, 8),          (28, 10),
    (29, 5),          (29, 7),          (29, 9), (29, 10),
    (30, 5),                   (30, 8), (30, 9), (30, 10),
            (31, 6), (31, 7), (31, 8), (31, 9),
            (32, 6), (32, 7), (32, 8),          (32, 10),
            (33, 6), (33, 7),          (33, 9), (33, 10),
            (34, 6),          (34, 8), (34, 9), (34, 10),
                    (35, 7), (35, 8), (35, 9), (35, 10),
    (36, 5), (36, 6), (36, 7), (36, 8), (36, 9),
    (37, 5), (37, 6), (37, 7), (37, 8),          (37, 10),
    (38, 5), (38, 6), (38, 7),          (38, 9), (38, 10),
    (39, 5), (39, 6),          (39, 8), (39, 9), (39, 10),
    (40, 5),          (40, 7), (40, 8), (40, 9), (40, 10),
            (41, 6), (41, 7), (41, 8), (41, 9), (41, 10),
    (42, 5), (42, 6), (42, 7), (42, 8), (42, 9), (42, 10);


-- Admins --
INSERT INTO `users` (`id`, `name`, `display_name`, `team_id`, `qr_id`, `created`, `email`, `is_admin`, `admin_password`, `qr_code`, `equipment_points`, `auth_token`, `sex`) VALUES
    (NULL, 'Kalvis Kalniņš'           , 'Kalvis K'   , NULL, '4376', CURRENT_TIMESTAMP, 'kalvis@kalvis.lv', '1', NULL, 'PL-29-wi', '0', '', 'male'),
    (NULL, 'Testa Lietotājs'          , 'Test'       , NULL, '4377', CURRENT_TIMESTAMP, 'test', '1', '$2y$10$4n8DDmMtmBxnSR3sTqkIm.omY6iehSl0qxDtysLOkWg91Ca9Sm5Ti', 'PL-2A-da', '0', '', 'male');
-- Lynx --
INSERT INTO `users` (`id`, `name`, `display_name`, `team_id`, `qr_id`, `created`, `email`, `is_admin`, `admin_password`, `qr_code`, `equipment_points`, `auth_token`, `sex`) VALUES
    (NULL, 'Paula Lilienfelde'        , 'Paula'      , '1' , '4369', CURRENT_TIMESTAMP, NULL, '0', NULL, 'PL-22-93', '0', '', 'female'),
    (NULL, 'Artis Galvanovskis'       , 'Artis'      , '1' , '4370', CURRENT_TIMESTAMP, NULL, '0', NULL, 'PL-23-jJ', '0', '', 'male'),
    (NULL, 'Ernests Tomass Auziņš'    , 'Ernests'    , '1' , '4371', CURRENT_TIMESTAMP, NULL, '0', NULL, 'PL-24-fs', '0', '', 'male'),
    (NULL, 'Sabīne Rasa Vīra'         , 'Rasa'       , '1' , '4381', CURRENT_TIMESTAMP, NULL, '0', NULL, 'PL-2E-iB', '0', '', 'female');
-- Bears --
INSERT INTO `users` (`id`, `name`, `display_name`, `team_id`, `qr_id`, `created`, `email`, `is_admin`, `admin_password`, `qr_code`, `equipment_points`, `auth_token`, `sex`) VALUES
    (NULL, 'Ance Zveja'               , 'Ance'       , '2' , '4372', CURRENT_TIMESTAMP, NULL, '0', NULL, 'PL-25-3K', '0', '', 'female'),
    (NULL, 'Ēriks Zālmanis'           , 'Ēriks'      , '2' , '4375', CURRENT_TIMESTAMP, NULL, '0', NULL, 'PL-28-Ft', '0', '', 'male'),
    (NULL, 'Jānis Muižnieks'          , 'Muižnieks'  , '2' , '4374', CURRENT_TIMESTAMP, NULL, '0', NULL, 'PL-27-AB', '0', '', 'male'),
    (NULL, 'Evija Fokrote'            , 'Evija'      , '2' , '4382', CURRENT_TIMESTAMP, NULL, '0', NULL, 'PL-2F-LS', '0', '', 'female');
-- Salamanders --
INSERT INTO `users` (`id`, `name`, `display_name`, `team_id`, `qr_id`, `created`, `email`, `is_admin`, `admin_password`, `qr_code`, `equipment_points`, `auth_token`, `sex`) VALUES
    (NULL, 'Maija Brasava'            , 'Maija'      , '3' , '4379', CURRENT_TIMESTAMP, NULL, '0', NULL, 'PL-2B-uT', '0', '', 'female'),
    (NULL, 'Чингиз Акматов'           , 'Чингиз'     , '3' , '4378', CURRENT_TIMESTAMP, NULL, '0', NULL, 'PL-2C-r3', '0', '', 'male'),
    (NULL, 'Sandra Boguša'            , 'Sandra'     , '3' , '4380', CURRENT_TIMESTAMP, NULL, '0', NULL, 'PL-2D-Xs', '0', '', 'female'),
    (NULL, 'Dārta Smaine'             , 'Dārta'      , '4' , '4383', CURRENT_TIMESTAMP, NULL, '0', NULL, 'PL-2G-SA', '0', '', 'female'),
    (NULL, 'Kārlis Starks'            , 'Kārlis'     , '4' , '4384', CURRENT_TIMESTAMP, NULL, '0', NULL, 'PL-2H-mb', '0', '', 'male'),
    (NULL, 'Ričards Vīdners'          , 'Ričards'    , '4' , '4385', CURRENT_TIMESTAMP, NULL, '0', NULL, 'PL-2J-ps', '0', '', 'male'),
    (NULL, 'Māris Ozolājs'            , 'Māris'      , '4' , '4386', CURRENT_TIMESTAMP, NULL, '0', NULL, 'PL-2K-6j', '0', '', 'male'),
    (NULL, 'Reinis Jurģis Sevelis'    , 'Reinis'     , '4' , '4387', CURRENT_TIMESTAMP, NULL, '0', NULL, 'PL-2L-3K', '0', '', 'male'),
    (NULL, 'Anrijs Dovbņa'            , 'Anrijs'     , '4' , '4388', CURRENT_TIMESTAMP, NULL, '0', NULL, 'PL-2M-iA', '0', '', 'male'),
    (NULL, 'Dārta Smaine'             , 'Dārta'      , '4' , '4389', CURRENT_TIMESTAMP, NULL, '0', NULL, 'PL-2N-Ej', '0', '', 'female'),
    (NULL, 'Rolands Lukstraups'       , 'Rolands'    , '4' , '4390', CURRENT_TIMESTAMP, NULL, '0', NULL, 'PL-2P-r2', '0', '', 'male');


-- assign a flag to each team
UPDATE `qr_codes` SET `owner_team_id` = '1' WHERE `qr_codes`.`qrid` = 'FL-22-Xn';
UPDATE `qr_codes` SET `owner_team_id` = '2' WHERE `qr_codes`.`qrid` = 'FL-23-qx';
UPDATE `qr_codes` SET `owner_team_id` = '3' WHERE `qr_codes`.`qrid` = 'FL-24-uP';


SET FOREIGN_KEY_CHECKS=1;
COMMIT;
