import {
    GraphQLInt,
    GraphQLNonNull,
    GraphQLObjectType, GraphQLString,
} from 'graphql'

import { CampDb } from '../db/models/CampDb'
import { ExpansionDb } from '../db/models/ExpansionDb'
import { TeamDb } from '../db/models/TeamDb'

import { camp } from './Camp'
import { team } from './Team'


export const expansion = new GraphQLObjectType({
    name: 'Expansion',
    description: 'Camp expansion',
    fields: () => ({
        code: {
            type: new GraphQLNonNull(GraphQLString),
            resolve: (source: ExpansionDb) => source.show_code,
        },
        owners: {
            type: new GraphQLNonNull(team),
            description: 'Team who received original expansion',
            resolve: async (source: ExpansionDb) => await TeamDb.findByPk(source.owner_team_id),
        },
        rewarded: {
            type: team,
            description: 'Team who got rewarded for expansion, null if not scanned',
            resolve: async (source: ExpansionDb) => source.rewarded_team_id ? await TeamDb.findByPk(source.rewarded_team_id) : null,
        },
        camp: {
            type: new GraphQLNonNull(camp),
            resolve: async (source: ExpansionDb) => await CampDb.findByPk(source.camp_id),
        },
        showedAt: {
            type: new GraphQLNonNull(GraphQLInt),
            resolve: (source: ExpansionDb) => new Date(source.showed).getTime() / 1000,
        },
        rewardedAt: {
            type: new GraphQLNonNull(GraphQLInt),
            resolve: (source: ExpansionDb) => new Date(source.rewarded).getTime() / 1000,
        },
    }),
})
