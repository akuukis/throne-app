import {
    GraphQLFloat,
    GraphQLInt,
    GraphQLNonNull,
    GraphQLObjectType,
    GraphQLString,
} from 'graphql'

import { CastleDb } from '../db/models/CastleDb'
import { TeamDb } from '../db/models/TeamDb'

import { team } from './Team'


export const castle = new GraphQLObjectType({
    name: 'Castle',
    description: 'Castle',
    fields: () => ({
        id: {
            type: new GraphQLNonNull(GraphQLInt),
        },
        title: {
            type: new GraphQLNonNull(GraphQLString),
        },
        team: {
            type: team,
            resolve: (source: CastleDb) => TeamDb.findByPk(source.team_id),
        },
        longitude: {
            type: GraphQLFloat,
        },
        latitude: {
            type: GraphQLFloat,
        },
    }),
})
