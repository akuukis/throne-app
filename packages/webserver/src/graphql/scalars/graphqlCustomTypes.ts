import { GraphQLEmail, GraphQLURL, GraphQLDateTime } from 'graphql-custom-types';

export const customTypesDef = `
 scalar GraphQLEmail
 scalar GraphQLURL
 scalar GraphQLDateTime
`;

export const customTypes = {
  GraphQLEmail,
  GraphQLURL,
  GraphQLDateTime,
};
