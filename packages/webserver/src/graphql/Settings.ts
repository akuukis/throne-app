import {
    GraphQLNonNull,
    GraphQLObjectType,
    GraphQLString,
} from 'graphql'


export const settings = new GraphQLObjectType({
    name: 'Settings',
    description: 'Settings file',
    fields: () => ({
        code: {
            type: new GraphQLNonNull(GraphQLString),
        },
        title: {
            type: new GraphQLNonNull(GraphQLString),
        },
        value: {
            type: new GraphQLNonNull(GraphQLString),
        },
    }),
})
