import {
    GraphQLInt,
    GraphQLNonNull,
    GraphQLObjectType,
} from 'graphql'


import { ResourceDb } from '../db/models/ResourceDb'
import { ResourceTypeDb } from '../db/models/ResourceTypeDb'
import { TeamDb } from '../db/models/TeamDb'

import { resourceType } from './ResourceTypes'
import { team } from './Team'


export const resource = new GraphQLObjectType({
    name: 'Resource',
    description: 'One resource totals for team',
    fields: () => ({
        id: {
            type: new GraphQLNonNull(GraphQLInt),
        },
        total: {
            type: new GraphQLNonNull(GraphQLInt),
            resolve: (data: ResourceDb):number => data.total_amount,
        },
        spent: {
            type: new GraphQLNonNull(GraphQLInt),
            resolve: (data: ResourceDb):number => data.spent_amount,
        },
        available: {
            type: new GraphQLNonNull(GraphQLInt),
            resolve: (data: ResourceDb):number => data.available_amount,
        },
        locked: {
            type: new GraphQLNonNull(GraphQLInt),
            resolve: (data: ResourceDb):number => data.locked_amount,
        },
        totalAmount: {
            type: new GraphQLNonNull(GraphQLInt),
            resolve: (data: ResourceDb):number => data.total_amount,
        },
        team: {
            type: team,
            resolve: (data: ResourceDb): Promise<TeamDb> => TeamDb.findByPk(data.team_id),
        },
        type: {
            type: new GraphQLNonNull(resourceType),
            description: 'Resource type',
            resolve: (data: ResourceDb): Promise<ResourceTypeDb> => ResourceTypeDb.getType(data.resource_type_id),
        },
    }),
})
