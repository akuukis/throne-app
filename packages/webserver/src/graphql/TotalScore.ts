import {
    GraphQLInt, GraphQLList,
    GraphQLNonNull,
    GraphQLObjectType,
} from 'graphql'

import { PillarDb } from '../db/models/PillarDb'
import { SeasonDb } from '../db/models/SeasonDb'
import { TeamDb } from '../db/models/TeamDb'
import { TotalScoreDb } from '../db/models/TotalScoreDb'
import { Context } from '../types/Context'

import { pillar } from './Pillar'
import { season } from './Season'
import { team } from './Team'
 

export const totalScore = new GraphQLObjectType({
    name: 'TotalScore',
    description: 'Total score row',
    fields: () => ({
        id: {
            type: new GraphQLNonNull(GraphQLInt),
        },
        score: {
            type:  new GraphQLNonNull(GraphQLInt),
        },
        rank: {
            type: new GraphQLNonNull(GraphQLInt),
        },
        lastUpdate: {
            type: GraphQLInt,
            resolve: (data: TotalScoreDb) => data.last_update ? new Date(data.last_update).getTime() / 1000 : null,
        },
        pillar: {
            type:  new GraphQLNonNull(pillar),
            resolve: (data: TotalScoreDb) => PillarDb.findByPk(data.type_id),
        },
        season: {
            type:  new GraphQLNonNull(season),
            resolve: (data: TotalScoreDb) => SeasonDb.findByPk(data.period_id),
        },
        team: {
            type:  new GraphQLNonNull(team),
            resolve: (data: TotalScoreDb) => TeamDb.findByPk(data.team_id),
        },
    }),
})

export const periodTotalScoreQuery = {
    type: new GraphQLNonNull(new GraphQLList(new GraphQLNonNull(totalScore))),
    args: {
        season: {
            description: 'Season id',
            type: GraphQLInt,
        },
    },
    resolve: async (data: TeamDb, { season }: { season?: number }, context: Context): Promise<TotalScoreDb[] | null> => {
        const seasonData = context.settings.currentSeason

        const currentSeason = season || seasonData || 1

        return await TotalScoreDb.findAll({
            where: {
                team_id: data.id,
                period_id: currentSeason,
            },
        })
    },
}
