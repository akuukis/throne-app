import {
    GraphQLInt, GraphQLNonNull,
    GraphQLObjectType,
} from 'graphql'
import { Op } from 'sequelize'

import { ScoreDb } from '../db/models/ScoreDb'
import { ScoreReasonsDb } from '../db/models/ScoreReasonsDb'
import { TeamDb } from '../db/models/TeamDb'
import { Pillar } from '../helpers/enums'
import { Context } from '../types/Context'

import { team } from './Team'


export const victoryTotals = new GraphQLObjectType({
    name: 'VictoryTotals',
    description: 'Victory point totals',
    fields: () => ({
        points: {
            type:  new GraphQLNonNull(GraphQLInt),
            resolve: async (team: TeamDb, _, context: Context) => {
                const score = await ScoreDb.findAll({
                    where: {
                        team_id: team.id,
                        type_id: Pillar.Victory,
                        score_time: {
                            [Op.lte]: context.settings.currentTimeString,
                        },
                    },
                })

                return score.length > 0 ? await score.reduce(async (total: Promise<number> | number, score: ScoreDb) => {
                    const reason = await ScoreReasonsDb.findByPk(score.reason_id)
                    return await total + reason.amount
                }, 0) : 0
            },
        },
        team: {
            type: new GraphQLNonNull(team),
            resolve: (team: TeamDb) => team,
        },
    }),
})
