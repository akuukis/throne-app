import { ApolloError } from 'apollo-server-errors';


export const genericError = (e:Error | ApolloError, error: string, code: string) => {
    if (!(e instanceof ApolloError)) {
        throw new ApolloError(`${error}; ${e}`, code)
    } else {
        throw new ApolloError(e.message, e.extensions.code);
    }
}
