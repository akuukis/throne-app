export enum ScanTypes {
    PlayerRegistration = 10,
    CharacterRegistration = 11,
    ScoreTribute = 12,
    ScoreLocalFlag = 13,
    ScoreEnemyFlag = 14,
    ResourceRegistration = 15,
    FaithRegistration = 16,
    Login = 17,
    FlagRegistration = 19,
    NightFeastReward = 20,
}

/**
 * @deprecated - use `RESOURCE_TYPE` from `@larts/api` instead.
 */
export enum ResourceType {
    Wood= 1,
    Hides =2,
    Iron = 3,
    Coins = 4,
    FoodHoney =5,
    FoodMix = 6,
    FoodTomatoes = 7,
    FoodCucumbers = 8,
    FoodMushrooms = 9,
    FoodRaspberries = 10,
}

export const ResourceKeys = {
    WD: 1,
    HD: 2,
    RN: 3,
    CN: 4,
    Fh: 5,
    Fv: 6,
    Ft: 7,
    Fc: 8,
    Fm: 9,
    Fr: 10,
}

export enum ResourceActionColumn {
    Outside= 'outside',
    Available = 'available',
    Locked = 'locked',
    Spent = 'spent',
}

export enum ResourceActionId {
    Scanned= 1,
    WorkshopReserved = 2,
    WorkshopSpent = 3,
    WoerkshopCancelled = 4,
    RecipeSpent = 5,
    FeastSpent = 6,
    BanditsReserved = 7,
    BanditsSpent = 8,
    BanditsCancelled = 9,
    EquipmentSpent = 10,
    Deposited = 11,
    Withdrawn = 12,
    MarketResourcesSpent = 13,
    MarketCoinsGained = 14,
}

export enum ScoreReason {
    Tribute = 10,
    FlagHome = 6,
    FlagShame= 9,
    FlagNormal = 8,
    FlagGlory = 7,
    TournamentFirst = 11,
    TournamentSecond = 12,
    FeastHost = 35,
    FeastGuest = 36,
    ExpansionOwner = 83,
    ExpansionScorer = 84,
}

/**
 * @deprecated use `CONFIG` instead.
 */
export enum Settings {
    GameHasStarted= 1,
    CurrentSeason = 2,
    StolenFlagDefaultReset= 4,
    RecipePrice = 6,
    FlagHomeScoreInterval = 8,
    GameStartTime = 9,
    MaxTeamKills = 10,
    RespawnPeriod = 11,
    RecipeIngredientsPerType = 12,
    EmergencyShutdown = 14,
    RecipeScoreOverTime = 15,
    TributeScoreTimeout = 16,
    IsFridayNight= 19,
    FridayNightSeason = 21,
    CurrentTick = 22,
    TickEmergencyStop = 23,
    TickUpdateTime = 24,
    WorkshopBonusTimeout = 26,
    RecipePointsPerIngredient = 27,
    ForwardTimeTo = 28,
}

export enum FlagStatus {
    Stolen = 1,
    Shamed = 2,
    Glory = 3,
}

export enum FlagAction {
    Score = 'score',
    Register = 'register',
}

export enum Sex {
    Male = 'male',
    Female = 'female',
}

export enum Pillar {
    Victory= 1,
    Culture = 2,
    Military = 3,
    Industry = 4 ,
    Friday = 5,
}
