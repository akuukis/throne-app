import { Column, Model, Table } from 'sequelize-typescript'


@Table({
    modelName: 'recipelog',
    freezeTableName: true,
    tableName: 'recipe_log',
    timestamps: false,
})

export class RecipeLogDb extends Model {
    @Column({
        primaryKey: true,
        autoIncrement: true,
    })
    id: number;

    @Column
    team_id: number;

    @Column
    recipe_id: number;

    @Column
    created: string;

    @Column
    starts: string;
}
