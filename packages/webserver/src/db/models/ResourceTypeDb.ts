import { ApolloError } from 'apollo-server-errors'
import { Column, Model, Table } from 'sequelize-typescript'


@Table({
    modelName: 'resourcetypes',
    freezeTableName: true,
    tableName: 'resource_types',
    timestamps: false,
})

export class ResourceTypeDb extends Model {
    @Column({
        primaryKey: true,
        autoIncrement: true,
    })
    id!: number;

    @Column
    name: string;

    @Column
    subtype: string;

    @Column
    qr_prefix: string;

    public static types: ResourceTypeDb[] = []

    public static async getType (id: number): Promise<ResourceTypeDb> {
        try {
            if (ResourceTypeDb.types.length === 0) {
                const types = await ResourceTypeDb.findAll()
                types.map((type: ResourceTypeDb) => {
                    ResourceTypeDb.types[type.id] = type
                })
            }

            return ResourceTypeDb.types[id]
        } catch (e) {
            throw new ApolloError('Failed to get resource type', 'GET_RES_TYPE_FAIL')
        }
    }
}
