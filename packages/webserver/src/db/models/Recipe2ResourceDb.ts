import { Column, Model, Table } from 'sequelize-typescript'

import { ResourceTypeDb } from './ResourceTypeDb'


@Table({
    modelName: 'recipe2resource',
    freezeTableName: true,
    tableName: 'recipe2resource',
    timestamps: false,
})

export class Recipe2ResourceDb extends Model {

    @Column({
        primaryKey: true,
    })
    recipe_id: number;

    @Column({
        primaryKey: true,
    })
    resource_id: number;

    private resource: ResourceTypeDb

    public async getResource(reset = false): Promise<ResourceTypeDb> {
        if (!this.resource || reset) {
            this.resource = await ResourceTypeDb.getType(this.resource_id)
        }

        return this.resource
    }
}
