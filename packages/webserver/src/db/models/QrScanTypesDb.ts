import { Column, Model, Table } from 'sequelize-typescript';


@Table({
    modelName: 'qrscantypes',
    freezeTableName: true,
    tableName: 'qr_scan_types',
    timestamps: false,
})

export class QrScanTypesDb extends Model {
    @Column({
        primaryKey: true,
        autoIncrement: true,
    })
    id!: number;

    @Column
    title: string;

    @Column
    is_enabled: boolean;

    @Column
    category_id: number;
}
