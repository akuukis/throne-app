import { Column, Model, Table } from 'sequelize-typescript'


@Table({
    modelName: 'castles',
    freezeTableName: true,
    tableName: 'castles',
    timestamps: false,
})

export class CastleDb extends Model {
    @Column({
        primaryKey: true,
        autoIncrement: true,
    })
    id: number;

    @Column
    latitude: number;

    @Column
    longitude: string;

    @Column
    title: string;

    @Column
    team_id: number;
}
