import { Column, Model, Table } from 'sequelize-typescript'

import { Context } from '../../types/Context'

import { Pillar2SeasonDb } from './Pillar2SeasonDb'
import { ScoreReasonsDb } from './ScoreReasonsDb'
import { SeasonDb } from './SeasonDb'


@Table({
    modelName: 'scoreTotals',
    freezeTableName: true,
    tableName: 'score_totals',
    timestamps: false,
})

export class TotalScoreDb extends Model {
    @Column({
        primaryKey: true,
        autoIncrement: true,
    })
    id!: number;

    @Column
    period_id: number;

    @Column
    type_id: number;

    @Column
    team_id: number;

    @Column
    last_update: string;

    @Column
    score: number;

    @Column
    rank: number;

    @Column
    event_points: number;

    public static async recalculate (reason: ScoreReasonsDb, season: SeasonDb) {
        const scoreTotals: TotalScoreDb[] = (await TotalScoreDb.findAll({
            where: {
                type_id: reason.type_id,
                period_id: season.id,
            },
        }))

        const newScoreOrder = scoreTotals.sort((total1: TotalScoreDb, total2: TotalScoreDb) => {
            if (total1.score > total2.score) {
                return -1
            }

            if (total1.score < total2.score) {
                return 1
            }

            if (total1.rank < total2.rank) {
                return -1
            }

            if (total1.rank > total2.rank) {
                return 1
            }

            if (total1.team_id < total2.team_id) {
                return 1
            }

            if (total1.team_id > total2.team_id) {
                return -1
            }

            // should not happen ever
            return 0
        })

        await Promise.all(newScoreOrder.map(async (scoreTotal: TotalScoreDb, index) => {
            scoreTotal.rank = index + 1
            await scoreTotal.save()
        }))
    }

    public static async updateTotal (reason: ScoreReasonsDb, season: SeasonDb, teamId: number, context: Context) {
        const scoreTotals: TotalScoreDb = (await TotalScoreDb.findOne({
            where: {
                team_id: teamId,
                type_id: reason.type_id,
                period_id: season.id,
            },
        })) || new TotalScoreDb()

        if (scoreTotals.isNewRecord) {
            scoreTotals.team_id = teamId
            scoreTotals.type_id = reason.type_id
            scoreTotals.period_id = season.id
            scoreTotals.score = 0
            scoreTotals.last_update = context.settings.currentTimeString
            scoreTotals.rank = 10
            await scoreTotals.save()
        }

        scoreTotals.last_update = context.settings.currentTimeString
        scoreTotals.score = scoreTotals.score + reason.amount
        await scoreTotals.save()

        await TotalScoreDb.recalculate(reason, season)

        await Pillar2SeasonDb.recalculate(season, reason.type_id)
    }
}
