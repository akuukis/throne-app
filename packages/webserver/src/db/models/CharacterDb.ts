import { ApolloError } from 'apollo-server-errors'
import { Column, Model, Table } from 'sequelize-typescript'

import { genericError } from '../../helpers/error'
import { Context } from '../../types/Context'
 
import { CharacterFailedDb } from './CharacterFailedDb'
import { UserDb } from './UserDb'


@Table({
    modelName: 'characters',
    freezeTableName: true,
    tableName: 'characters',
    timestamps: false,
})

export class CharacterDb extends Model {
    @Column({
        primaryKey: true,
        autoIncrement: true,
    })
    id: number;

    @Column
    charkey: number;

    @Column
    charcode: number;

    @Column
    owner_user_id: number;

    @Column
    owner_team_id: number;

    @Column
    tribute_user_id: number;

    @Column
    tribute_team_id: number;

    @Column
    created: string;

    @Column
    tribute_time: string;

    @Column
    tribute_period_id: number;

    @Column
    death_registered_at: string;

    @Column
    death_season_id: number;

    public static async validateCode(code: number, user: UserDb, context: Context) {
        try {
            const key = Math.floor(code/10)

            const existingCharacter = await CharacterDb.findOne({
                where: {
                    charkey: key,
                },
            })

            const possibleFail = new CharacterFailedDb()

            possibleFail.charcode = code
            possibleFail.user_id = user.id
            possibleFail.created = context.settings.currentTimeString

            if (code < 9999 || code > 100000) {
                await possibleFail.save()

                throw new ApolloError('Code is incorrect', 'CHAR_VALIDATION_INCORRECT')
            }

            if (existingCharacter) {
                await possibleFail.save()

                throw new ApolloError('Code already exists', 'CHAR_VALIDATION_EXISTS')
            }

        } catch (e) {
            genericError(e,'Failed to validate code', 'CHAR_VALIDATION_FAIL')
        }

        return true
    }
}
