import { ApolloError } from 'apollo-server-errors'
import { Column, Model, Table } from 'sequelize-typescript'

import { genericError } from '../../helpers/error'
import { Context } from '../../types/Context'

import { DeviceDb } from './DeviceDb'
import { QrCodeDb } from './QrCodeDb'
import { QrScanTypesDb } from './QrScanTypesDb'
import { UserDb } from './UserDb'


@Table({
    modelName: 'qrscans',
    freezeTableName: true,
    tableName: 'qr_scans',
    timestamps: false,
})

export class QrScanDb extends Model {
    @Column({
        primaryKey: true,
        autoIncrement: true,
    })
    id!: number;

    @Column
    qr_id: number;

    @Column
    created: string;

    @Column
    device_id: number;

    @Column
    scanner_team_id: number;

    @Column
    scanner_user_id: number;

    @Column
    type_id: number;

    @Column
    remove: boolean;

    @Column
    is_fail: boolean;

    @Column
    qr_raw: string;

    @Column
    error: string;

    private qrcode: QrCodeDb;
    private scanType: QrScanTypesDb;

    public async getQrCode(reset = false): Promise<QrCodeDb> {
        if (!this.qrcode || reset) {
            this.qrcode = await QrCodeDb.findByPk(this.qr_id)
        }

        return this.qrcode
    }

    public async getScanType(reset = false): Promise<QrScanTypesDb> {
        if (!this.scanType || reset) {
            this.scanType = await QrScanTypesDb.findByPk(this.qr_id)
        }

        return this.scanType
    }

    public async saveWithError (error: string) {
        this.is_fail = true
        this.error = error
        await this.save()

        return error
    }

    public static async registerScan (qrCode: string, type: number, device: DeviceDb, user: UserDb | null, context: Context, use = false) {
        try {
            const scan = new QrScanDb()
            
            scan.qr_raw = qrCode
            if (device) {scan.device_id = device.id}
            scan.type_id = type
            scan.created = context.settings.currentTimeString

            if (user) {
                scan.scanner_user_id = user.id
                scan.scanner_team_id = (await user.getTeam()).id
            }

            if (!device || !device.is_enabled) {
                throw new ApolloError('This device is not registered for any team', await scan.saveWithError('QR_REGISTRATION_UNREGISTERED_DEVICE'))
            }

            /* const isValid = qrValidate(qrCode, scan)

            if (!isValid) {
                throw new ApolloError('Failed to validate QR', await scan.saveWithError('QR_REGISTRATION_INVALID_QR'))
            }
            */

            const qr = await QrCodeDb.findOne({ where: { qrid: qrCode } })

            if (!qr) {
                throw new ApolloError(`QR not found in db ${qrCode}`, await scan.saveWithError('QR_REGISTRATION_NON_EXISTENT_QR'))
            }

            scan.qr_id = qr.id

            const scanType = await QrScanTypesDb.findByPk(type)

            if (!scanType) {
                throw new ApolloError('No such action exists', await scan.saveWithError('QR_REGISTRATION_NON_EXISTENT_ACTION'))
            }

            if (qr.is_used) {
                throw new ApolloError(`QR code already used ${qrCode}`, await scan.saveWithError('QR_REGISTRATION_CODE_ALREADY_USED'))
            }

            if (scanType.category_id !== qr.category_id) {
                throw new ApolloError('Incorrect QR for chosen action', await scan.saveWithError('QR_REGISTRATION_INCORRECT_ACTION'))
            }

            await scan.save()

            qr.last_scan_id = scan.id

            if (use) {
                qr.is_used = true
            }
            await qr.save()

            return scan
        } catch (e) {
            genericError(e,'Failed to register scan', 'QR_REGISTRATION')
        }

    }
}
