import { Column, Model, Table } from 'sequelize-typescript'


@Table({
    modelName: 'feasttoken',
    freezeTableName: true,
    tableName: 'feast_token',
    timestamps: false,
})

export class FeastTokenDb extends Model {
    @Column({
        primaryKey: true,
        autoIncrement: true,
    })
    id: number;

    @Column
    created: string;

    @Column
    team_id: number;

    @Column
    is_used: boolean;

    @Column
    used_at: string;

    @Column
    period_id: number;

    @Column
    recipe_id: number;
}
