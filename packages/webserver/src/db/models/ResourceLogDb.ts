import { Column, Model, Table } from 'sequelize-typescript'


@Table({
    modelName: 'resourcelog',
    freezeTableName: true,
    tableName: 'resource_log',
    timestamps: false,
})

export class ResourceLogDb extends Model {
    @Column({
        primaryKey: true,
        autoIncrement: true,
    })
    id!: number;

    @Column
    resource_type_id: number;

    @Column
    team_id: number;

    @Column
    action_id: number;

    @Column
    created: string;

    @Column
    details: string;

    @Column
    amount: number;
}
