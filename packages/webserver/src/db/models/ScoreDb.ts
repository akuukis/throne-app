import { ApolloError } from 'apollo-server-errors'
import { Column, Model, Table } from 'sequelize-typescript'

import { genericError } from '../../helpers/error'
import { Context } from '../../types/Context'

import { ScoreReasonsDb } from './ScoreReasonsDb'
import { SeasonDb } from './SeasonDb'
import { TotalScoreDb } from './TotalScoreDb'


@Table({
    modelName: 'score',
    freezeTableName: true,
    tableName: 'scores',
    timestamps: false,
})

export class ScoreDb extends Model {
    @Column({
        primaryKey: true,
        autoIncrement: true,
    })
    id!: number;

    @Column
    period_id: number;

    @Column
    type_id: number;

    @Column
    reason_id: number;

    @Column
    team_id: number;

    @Column
    created: string;

    @Column
    details: string;

    @Column
    score_time: string;

    public static async cancel (reasonId: number, details: string, context: Context) {
        try {
            const { currentTime } = context.settings

            const scores = await ScoreDb.findAll({
                where: {
                    reason_id: reasonId,
                    details: details,
                },
            })

            const stopCancel = scores.reduce((stop: boolean, score: ScoreDb) => stop || new Date(score.score_time) <= currentTime, false)

            if (stopCancel) {
                throw new ApolloError('Points already added', 'SCORE_CANCEL_TOO_LATE')
            }

            scores.forEach((score: ScoreDb) => score.destroy())

        } catch (e) {
            genericError(e, 'Failed to cancel score', 'SCORE_CANCEL_FAIL')
        }
    }

    public static async do (reasonId: number, details: string, teamId: number, context: Context, seasonIsFixed: SeasonDb | null = null, futureTime: Date | null = null) {
        try {
            const { currentSeason, currentTimeString, currentTime } = context.settings

            const scoreTimeString = futureTime
                ? futureTime.toISOString().slice(0, 19).replace('T', ' ')
                : currentTimeString

            const reason = await ScoreReasonsDb.findByPk(reasonId)

            if (!reason) {
                throw new ApolloError('Score reason not found!', 'SCORE_INVALID_REASON')
            }

            if (currentSeason === 0 && !seasonIsFixed) {
                throw new ApolloError('Season not set!', 'SCORE_NO_SEASON_SET')
            }

            const season = seasonIsFixed || await SeasonDb.findByPk(currentSeason)

            if (!season) {
                throw new ApolloError('Season not found!', 'SCORE_NO_SEASON')
            }

            const score = new ScoreDb()
            score.reason_id = reason.id
            score.type_id = reason.type_id
            score.team_id = teamId
            score.period_id = season.id
            score.details = details
            score.created = currentTimeString
            score.score_time = scoreTimeString
            await score.save()

            if (!futureTime || futureTime <= currentTime ) {
                await TotalScoreDb.updateTotal(reason, season, teamId, context)
            }

        } catch (e) {
            genericError(e, 'Failed to add score', 'SCORE_FAIL')
        }
    }
}
