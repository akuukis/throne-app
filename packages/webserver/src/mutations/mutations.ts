import { GraphQLObjectType } from 'graphql'

import { authMutations } from './auth'
import { characterMutations } from './character'
import { debugMutations } from './debugging'
import { equipmentMutations } from './equipment'
import { expansionMutations } from './expansions'
import { feastMutations } from './feast'
import { flagMutations } from './flag'
import { nightMutations } from './night'
import { recipeMutations } from './recipe'
import { resourceMutations } from './resources'
import { workshopMutations } from './workshop'


export const mutation = new GraphQLObjectType({
    name: 'Mutation',
    fields: () => ({
        auth: {
            type: authMutations,
            resolve: () => ({}),
        },
        workshop: {
            type: workshopMutations,
            resolve: () => ({}),
        },
        resources: {
            type: resourceMutations,
            resolve: () => ({}),
        },
        equipment: {
            type: equipmentMutations,
            resolve: () => ({}),
        },
        character: {
            type: characterMutations,
            resolve: () => ({}),
        },
        flag: {
            type: flagMutations,
            resolve: () => ({}),
        },
        recipe: {
            type: recipeMutations,
            resolve: () => ({}),
        },
        feast: {
            type: feastMutations,
            resolve: () => ({}),
        },
        expansions: {
            type: expansionMutations,
            resolve: () => ({}),
        },
        night: {
            type: nightMutations,
            resolve: () => ({}),
        },
        debug: {
            type: debugMutations,
            resolve: () => ({}),
        },
    }),
})
