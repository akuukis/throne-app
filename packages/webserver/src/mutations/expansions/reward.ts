import { ApolloError } from 'apollo-server-errors'
import { GraphQLNonNull, GraphQLString } from 'graphql'

import { ExpansionDb } from '../../db/models/ExpansionDb'
import { ScoreDb } from '../../db/models/ScoreDb'
import { SeasonDb } from '../../db/models/SeasonDb'
import { checkAuth } from '../../helpers/auth'
import { ScoreReason } from '../../helpers/enums'
import { genericError } from '../../helpers/error'
import { Context } from '../../types/Context'
import { MUTATION_SUCCESS } from '../constants'


export const reward = {
    type: new GraphQLNonNull(GraphQLString),
    args: {
        code: {
            description: 'Reward Code',
            type: new GraphQLNonNull(GraphQLString),
        },
    },
    resolve: async (_, { code }: { code: string }, context: Context): Promise<string> => {
        checkAuth(context)

        try {
            const expansion = await ExpansionDb.findOne({
                where: {
                    reward_code: code,
                },
            })

            if (!expansion) {
                throw new ApolloError('Expansion code invalid!', 'EXP_REWARD_INVALID_CODE')
            }

            if (expansion.rewarded) {
                throw new ApolloError('Expansion code already scored!', 'EXP_REWARD_SCORED')
            }

            if (!expansion.showed) {
                throw new ApolloError('Expansion code invalid!', 'EXP_REWARD_NOT_AWARDED')
            }

            const seasonId = context.settings.currentSeason

            if (seasonId === 0) {
                throw new ApolloError('Season not set!', 'EXP_REWARD_NO_SEASON_SET')
            }

            const season = await SeasonDb.findByPk(seasonId)
            const prevSeason = await SeasonDb.findOne({ where: { next_periods_id: season.id } })

            if (expansion.assignment_period_id !== season.id && (prevSeason && expansion.assignment_period_id !== prevSeason.id)) {
                throw new ApolloError('Expansion token is expired', 'EXP_REWARD_EXPIRED')
            }

            expansion.rewarded_team_id = context.authDevice.data.team_id
            expansion.rewarded = context.settings.currentTimeString
            await expansion.save()

            await ScoreDb.do(ScoreReason.ExpansionScorer, String(expansion.id), context.authUser.data.team_id, context)
            await ScoreDb.do(ScoreReason.ExpansionOwner, String(expansion.id), expansion.owner_team_id, context)

        } catch (e) {
            genericError(e,'Reward for expansion failed', 'EXP_REWARD_FAIL')
        }

        return MUTATION_SUCCESS
    },
}
