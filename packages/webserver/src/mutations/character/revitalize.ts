import { ApolloError } from 'apollo-server-errors'
import { GraphQLNonNull, GraphQLString } from 'graphql'
import { Op } from 'sequelize'

import { CharacterDb } from '../../db/models/CharacterDb'
import { SettingsDb } from '../../db/models/SettingsDb'
import { checkAuth } from '../../helpers/auth'
import { Settings } from '../../helpers/enums'
import { genericError } from '../../helpers/error'
import { Context } from '../../types/Context'
import { MUTATION_SUCCESS } from '../constants'


export const revitalize = {
    type: new GraphQLNonNull(GraphQLString),
    resolve: async (_, __, context: Context): Promise<string> => {
        checkAuth(context)

        try {
            const seasonId = context.settings.currentSeason

            if (seasonId === 0) {
                throw new ApolloError('Season not set!', 'REVITALIZE_NO_SEASON_SET')
            }

            const previousChar = await CharacterDb.findOne({ where: {
                owner_team_id: context.authUser.data.team_id,
            }, order: [['created', 'DESC']] })

            if (previousChar) {
                const respawnPeriod = await SettingsDb.findByPk(Settings.RespawnPeriod)
                const date = new Date(previousChar.created)

                const distance = new Date(date.setSeconds(date.getSeconds() + Number(respawnPeriod.value)))

                if (distance > context.settings.currentTime) {
                    throw new ApolloError('Please wait cooldown for respawn!', 'REVITALIZE_TOO_SOON')
                }
            }

            const existingCharacter = await CharacterDb.findOne({
                where: {
                    owner_user_id: context.authUser.data.id,
                    death_registered_at: { [Op.eq]: null },
                },
            })

            if (!existingCharacter) {
                throw new ApolloError('Code not found', 'REVITALIZE_NO_CODE')
            }

            if (existingCharacter.tribute_time) {
                throw new ApolloError('Tribute already scored', 'REVITALIZE_ALREADY_SCORED')
            }

            existingCharacter.death_registered_at = context.settings.currentTimeString
            existingCharacter.death_season_id = seasonId
            await existingCharacter.save()

            const newCharacter = new CharacterDb()

            newCharacter.owner_team_id = context.authUser.data.team_id
            newCharacter.owner_user_id = context.authUser.data.id
            newCharacter.charcode = existingCharacter.charcode
            newCharacter.charkey = existingCharacter.charkey
            newCharacter.created = context.settings.currentTimeString

            await newCharacter.save()
        } catch (e) {
            genericError(e,'Revitalize failed', 'REVITALIZE_FAIL')
        }

        return MUTATION_SUCCESS
    },
}
