import { GraphQLObjectType } from 'graphql'

import { respawn } from './respawn'
import { revitalize } from './revitalize'
import { tribute } from './tribute'


export const characterMutations = new GraphQLObjectType({
    name: 'CharacterActions',
    fields: {
        respawn,
        tribute,
        revitalize,
    },
})
