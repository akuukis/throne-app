import { GraphQLObjectType } from 'graphql'

import { score } from './score'


export const flagMutations = new GraphQLObjectType({
    name: 'FlagActions',
    fields: {
        score,
    },
})
