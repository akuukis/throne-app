import { RESOURCE_TYPE } from '@larts/api'
import { ApolloError } from 'apollo-server-errors'
import { GraphQLInputObjectType, GraphQLInt, GraphQLList, GraphQLNonNull, GraphQLString } from 'graphql'
import { Op } from 'sequelize'

import { ResourceActionDb } from '../../db/models/ResourceActionDb'
import { Price, ResourceDb } from '../../db/models/ResourceDb'
import { ResourceTypeDb } from '../../db/models/ResourceTypeDb'
import { ScoreDb } from '../../db/models/ScoreDb'
import { WorkshopActivationDb } from '../../db/models/WorkshopActivationDb'
import { WorkshopSizeDb } from '../../db/models/WorkshopSizeDb'
import { checkAuth } from '../../helpers/auth'
import { ResourceActionId } from '../../helpers/enums'
import { genericError } from '../../helpers/error'
import { Context } from '../../types/Context'
import { MUTATION_SUCCESS } from '../constants'


const WorkshopActivateFoodInput = new GraphQLInputObjectType({
    name: 'WorkshopActivateFoodInput',
    description: 'Workshop Activation Input',
    fields: () => ({
        variant: {
            type: new GraphQLNonNull(GraphQLInt),
        },
        amount: {
            type: new GraphQLNonNull(GraphQLInt),
        },
    }),
})

const WorkshopActivateInput = new GraphQLInputObjectType({
    name: 'WorkshopActivateInput',
    description: 'Workshop Activation Input',
    fields: () => ({
        variant: {
            type: new GraphQLNonNull(GraphQLInt),
        },
        sizeId: {
            type: new GraphQLNonNull(GraphQLInt),
        },
        wood: {
            type: new GraphQLNonNull(GraphQLInt),
        },
        food: {
            type: new GraphQLList(new GraphQLNonNull(WorkshopActivateFoodInput)),
            description: 'ID of food subtype IF selected workshop runs on food!',
        },
    }),
})

interface WorkshopActivationFoodInputProps {
    variant: number;
    amount: number;
}

interface WorkshopActivationInputProps {
    variant: number;
    sizeId: number;
    wood: number;
    food?: WorkshopActivationFoodInputProps[],
}

export const activate = {
    type: new GraphQLNonNull(GraphQLString),
    args: {
        input: {
            type: new GraphQLNonNull(WorkshopActivateInput),
        },
    },
    resolve: async (_, { input }: { input: WorkshopActivationInputProps } , context: Context): Promise<string> => {
        checkAuth(context)

        try {
            const workshopSize = await WorkshopSizeDb.findByPk(input.sizeId)

            if (!workshopSize) {
                throw new ApolloError('Workshop size not found', 'WORKSHOP_ACTIVATE_NO_WORKSHOP_SIZE')
            }
            if (input.wood < workshopSize.wood_cost) {
                throw new ApolloError('Not enough wood', 'WORKSHOP_ACTIVATE_NO_ENOUGH_WOOD')
            }

            let price: Price[] = []

            if (input.wood < (workshopSize.wood_cost + workshopSize.resource_cost)) {
                if (input.variant === RESOURCE_TYPE.Food) {
                    if (!input.food) {
                        throw new ApolloError('Food not specified!', 'WORKSHOP_ACTIVATE_SPECIFY_FOOD')
                    }

                    price = await Promise.all(input.food.map(async (food: WorkshopActivationFoodInputProps): Promise<Price> => {
                        const selectedFood = await ResourceTypeDb.getType(food.variant)

                        if (!selectedFood) {
                            throw new ApolloError('Weird food value received!', 'WORKSHOP_ACTIVATE_NON_EXISTENT_FOOD')
                        }


                        return {
                            type: selectedFood,
                            amount: food.amount,
                        }
                    }))
                } else {
                    price.push({
                        type: await ResourceTypeDb.getType(input.variant),
                        amount: (workshopSize.wood_cost + workshopSize.resource_cost) - input.wood,
                    })
                }
            }

            price.push({
                type: await ResourceTypeDb.getType(RESOURCE_TYPE.Wood),
                amount: input.wood,
            })

            price.push({
                type: await ResourceTypeDb.getType(RESOURCE_TYPE.Coins),
                amount: workshopSize.coin_cost,
            })

            await ResourceDb.spendResources(price, await ResourceActionDb.findByPk(ResourceActionId.WorkshopReserved), context.authDevice.data, context.settings.currentTimeString)

            const activation = new WorkshopActivationDb()
            activation.team_id = context.authUser.data.team_id
            activation.size_id = workshopSize.id
            activation.created = context.settings.currentTimeString
            activation.variant = input.variant

            const previousActivation = await WorkshopActivationDb.findOne({
                where: {
                    team_id: activation.team_id,
                    finished: { [Op.gte]: context.settings.currentTimeString },
                    cancelled: { [Op.eq]: null },
                },
                order: [
                    ['started', 'DESC'],
                ],
            })

            const lastFinishTime = previousActivation !== null ? new Date(previousActivation.finished) : context.settings.currentTime

            const newStartDate = new Date(lastFinishTime.getTime() + 1000)
            const newFinishDate = new Date(newStartDate.getTime() + (Number(workshopSize.cooldown) * 1000))

            activation.finished = newFinishDate.toISOString().slice(0, 19).replace('T', ' ')
            activation.started = newStartDate.toISOString().slice(0, 19).replace('T', ' ')

            activation.price_details = JSON.stringify(price.map((priceItem) => {
                return {
                    type: priceItem.type.id,
                    amount: priceItem.amount,
                }
            }))

            await activation.save()

            // Lets create score values for future
            const IPperiod = Number(workshopSize.cooldown) / workshopSize.ip_amount

            // assign predicted score point values

            for (let i = 0; i < workshopSize.ip_amount; i++) {
                ScoreDb.do(
                    workshopSize.score_reason_id,
                    String(activation.id),
                    context.authDevice.data.team_id,
                    context,
                    null,
                    new Date(newStartDate.getTime() + (IPperiod * (i + 1) * 1000)),
                )
            }

        } catch (e) {
            genericError(e, 'Failed to activate workshop!', 'WORKSHOP_ACTIVATE_FAILED')
        }

        return MUTATION_SUCCESS
    },
}
