import { ApolloError } from 'apollo-server-errors'
import { GraphQLNonNull, GraphQLString } from 'graphql'

import { checkAuth } from '../../helpers/auth'
import { genericError } from '../../helpers/error'
import { Context } from '../../types/Context'
import { MUTATION_SUCCESS } from '../constants'

// Hardcoded for Amulet of Phoenix. TODO: Generalize for all kinds of virtual equipment.
export const equip = {
    type: new GraphQLNonNull(GraphQLString),
    resolve: async (_, __, context: Context): Promise<string> => {
        checkAuth(context)

        try {
            if (context.authUser.data.equipment_points === 0) {
                throw new ApolloError('User has no available EP', 'EQ_NOT_AVAILABLE')
            }
            if (context.authUser.data.equipment_has_phoenix) {
                throw new ApolloError('User already has Amulet of Phoenix', 'EQ_ALREADY_EQUIPPED')
            }

            context.authUser.data.equipment_has_phoenix = 1
            await context.authUser.data.save()
        } catch (e) {
            genericError(e,'Failed to equip Amulet of Phoenix', 'EQ_EQUIP_FAIL')
        }

        return MUTATION_SUCCESS
    },
}
