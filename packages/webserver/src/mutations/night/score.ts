import { GraphQLNonNull, GraphQLString } from 'graphql'

import { QrScanDb } from '../../db/models/QrScanDb'
import { ScoreDb } from '../../db/models/ScoreDb'
import { SeasonDb } from '../../db/models/SeasonDb'
import { SettingsDb } from '../../db/models/SettingsDb'
import { checkAuth } from '../../helpers/auth'
import { ScanTypes, Settings } from '../../helpers/enums'
import { genericError } from '../../helpers/error'
import { Context } from '../../types/Context'
import { MUTATION_SUCCESS } from '../constants'


export const score = {
    type: new GraphQLNonNull(GraphQLString),
    args: {
        qrCode: {
            description: 'QR Code',
            type: new GraphQLNonNull(GraphQLString),
        },
    },
    resolve: async (_, { qrCode }: { qrCode: string }, context: Context): Promise<string> => {
        checkAuth(context)

        try {
            const scan = await QrScanDb.registerScan(qrCode, ScanTypes.NightFeastReward, context.authDevice.data, context.authUser.data, context, true)

            const qr = await scan.getQrCode()

            const seasonId = await SettingsDb.findByPk(Settings.FridayNightSeason)
            const season = await SeasonDb.findByPk(Number(seasonId['value']))

            await ScoreDb.do(Number(qr.details), String(qr.id), context.authUser.data.team_id, context, season)
        } catch (e) {
            genericError(e,'Friday night failed to scan', 'NIGHT_SCORE_FAIL')
        }

        return MUTATION_SUCCESS
    },
}
