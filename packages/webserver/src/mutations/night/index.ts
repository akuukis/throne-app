import { GraphQLObjectType } from 'graphql'

import { score } from './score'


export const nightMutations = new GraphQLObjectType({
    name: 'NightActions',
    fields: {
        score,
    },
})
