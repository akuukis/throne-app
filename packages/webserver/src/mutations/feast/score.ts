import { ApolloError } from 'apollo-server-errors'
import { GraphQLInt, GraphQLList, GraphQLNonNull, GraphQLString } from 'graphql'

import { FeastTokenDb } from '../../db/models/FeastTokenDb'
import { ScoreDb } from '../../db/models/ScoreDb'
import { SeasonDb } from '../../db/models/SeasonDb'
import { UserDb } from '../../db/models/UserDb'
import { checkAuth } from '../../helpers/auth'
import { ScoreReason, Sex } from '../../helpers/enums'
import { genericError } from '../../helpers/error'
import { Context } from '../../types/Context'
import { MUTATION_SUCCESS } from '../constants'


export const score = {
    type: new GraphQLNonNull(GraphQLString),
    args: {
        id: {
            description: 'Feast token',
            type: new GraphQLNonNull(GraphQLInt),
        },
        qrList: {
            description: 'List of QR\'s for users who are feasting',
            type: new GraphQLNonNull(new GraphQLList(new GraphQLNonNull(GraphQLString))),
        },
    },
    resolve: async (_, { id, qrList }: { id: number, qrList: string[] }, context: Context): Promise<string> => {
        checkAuth(context)

        try {
            const token = await FeastTokenDb.findByPk(id)

            if (qrList.length % 2 > 0) {
                throw new ApolloError(`Odd participant amount (${qrList.length})`, 'FEAST_SCORE_ODD_PARTICIPANT_AMOUNT')
            }

            if (!token) {
                throw new ApolloError('Feast token does not exist', 'FEAST_SCORE_INVALID_TOKEN')
            }

            if (token.team_id !== context.authDevice.data.team_id) {
                throw new ApolloError('Feast token belongs to other team', 'FEAST_SCORE_WRONG_TOKEN')
            }

            if (token.used_at) {
                throw new ApolloError('Feast token already used', 'FEAST_SCORE_TOKEN_USED')
            }

            if (qrList.length / 2 > 6) {
                throw new ApolloError('Too much participants', 'FEAST_SCORE_AMOUNT_TOO_MUCH')
            }

            const seasonId = context.settings.currentSeason

            if (seasonId === 0) {
                throw new ApolloError('Season not set!', 'FEAST_SCORE_NO_SEASON_SET')
            }


            const season = await SeasonDb.findByPk(seasonId)
            const prevSeason = await SeasonDb.findOne({ where: { next_periods_id: season.id } })

            if (token.period_id !== season.id && (prevSeason && token.period_id !== prevSeason.id)) {
                throw new ApolloError('Grand dish token is expired', 'FEAST_SCORE_EXPIRED')
            }

            const participants: UserDb[] = await Promise.all(qrList.map(async (qr): Promise<UserDb> => {
                const participant = UserDb.findOne({ where: {
                    qr_code: qr,
                } })

                if (!participant) {
                    throw new ApolloError(`Participant not found: ${qr}`, 'FEAST_SCORE_NON_EXISTENT_PARTICIPANT')
                }

                return participant
            }))

            const hosts = participants.filter((participant) => participant.team_id === context.authDevice.data.team_id)
            const guests = participants.filter((participant) => participant.team_id !== context.authDevice.data.team_id)

            if (hosts.length !== guests.length) {
                throw new ApolloError('Not equal amount of hosts and guests', 'FEAST_SCORE_GUEST_VS_HOST_AMOUNT')
            }

            const hostMale = hosts.filter((participant) => participant.sex === Sex.Male)
            const hostFemale = hosts.filter((participant) => participant.sex === Sex.Female)

            const guestMale = guests.filter((participant) => participant.sex === Sex.Male)
            const guestFemale = guests.filter((participant) => participant.sex === Sex.Female)

            if (hostMale.length !== guestFemale.length || hostFemale.length !== guestMale.length) {
                throw new ApolloError('Incorrect proportions of male-female', 'FEAST_SCORE_AMOUNT_INCORRECT_MALE_FEMALE')
            }

            for (const host of hosts) {
                await ScoreDb.do(ScoreReason.FeastHost, String(token.id), host.team_id, context)
            }

            for (const guest of guests) {
                await ScoreDb.do(ScoreReason.FeastGuest, String(token.id), guest.team_id, context)
            }

            token.is_used = true
            token.used_at = context.settings.currentTimeString
            await token.save()

        } catch (e) {
            genericError(e,'Feast scoring failed', 'FEAST_SCORE_FAIL')
        }

        return MUTATION_SUCCESS
    },
}
