import { ApolloError } from 'apollo-server-errors'
import { GraphQLInt, GraphQLNonNull, GraphQLString } from 'graphql'

import { ResourceActionDb } from '../../db/models/ResourceActionDb'
import { Price, ResourceDb } from '../../db/models/ResourceDb'
import { ResourceTypeDb } from '../../db/models/ResourceTypeDb'
import { checkAuth } from '../../helpers/auth'
import { ResourceActionId, ResourceType } from '../../helpers/enums'
import { genericError } from '../../helpers/error'
import { Context } from '../../types/Context'
import { MUTATION_SUCCESS } from '../constants'


export const sell = {
    type: new GraphQLNonNull(GraphQLString),
    args: {
        id: {
            description: 'Resource type id',
            type: new GraphQLNonNull(GraphQLInt),
        },
        amount: {
            type: new GraphQLNonNull(GraphQLInt),
        },
    },
    resolve: async (_, { id, amount }: { id: number, amount: number }, context: Context): Promise<string> => {
        checkAuth(context)

        try {
            if (amount <= 0) {
                throw new ApolloError('Please specify positive amount', 'RESOURCE_SELL_ZERO_OR_LESS')
            }

            if (amount % 2 !== 0) {
                throw new ApolloError('Please specify even amount', 'RESOURCE_SELL_EVEN')
            }

            const resource: Price = {
                type: await ResourceTypeDb.getType(id),
                amount: amount,
            }

            const coins: Price ={
                type: await ResourceTypeDb.getType(ResourceType.Coins),
                amount: amount/2 * (id === ResourceType.Hides ? 4 : 3),
            }

            await ResourceDb.spendResources([resource], await ResourceActionDb.findByPk(ResourceActionId.MarketResourcesSpent), context.authUser.data, context.settings.currentTimeString)
            await ResourceDb.spendResources([coins], await ResourceActionDb.findByPk(ResourceActionId.MarketCoinsGained), context.authUser.data, context.settings.currentTimeString)
        } catch (e) {
            genericError(e,'Resource failed to register resource', 'RESOURCE_SELL')
        }

        return MUTATION_SUCCESS
    },
}
