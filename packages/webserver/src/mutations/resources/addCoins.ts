import { ApolloError } from 'apollo-server-errors'
import { GraphQLInt, GraphQLNonNull, GraphQLString } from 'graphql'

import { ResourceActionDb } from '../../db/models/ResourceActionDb'
import { Price, ResourceDb } from '../../db/models/ResourceDb'
import { ResourceTypeDb } from '../../db/models/ResourceTypeDb'
import { checkAuth } from '../../helpers/auth'
import { ResourceActionId, ResourceType } from '../../helpers/enums'
import { genericError } from '../../helpers/error'
import { Context } from '../../types/Context'
import { MUTATION_SUCCESS } from '../constants'


export const addCoins = {
    type: new GraphQLNonNull(GraphQLString),
    args: {
        amount: {
            description: 'Amount of coins being added',
            type: new GraphQLNonNull(GraphQLInt),
        },
    },
    resolve: async (_, { amount }: { amount: number }, context: Context): Promise<string> => {
        checkAuth(context)

        try {
            if (amount <= 0) {
                throw new ApolloError('Please specify positive amount', 'ADD_COINS_ZERO_OR_LESS')
            }

            const resource: Price = {
                type: await ResourceTypeDb.getType(ResourceType.Coins),
                amount: amount,
            }

            await ResourceDb.spendResources([resource], await ResourceActionDb.findByPk(ResourceActionId.Deposited), context.authUser.data, context.settings.currentTimeString)

            const team = await context.authUser.data.getTeam()
            team.coinbox = team.coinbox + amount
            await team.save()
        } catch (e) {
            genericError(e,'Failed to add coins', 'ADD_COINS_ERROR')
        }

        return MUTATION_SUCCESS
    },
}
