import { DeviceDb } from '../db/models/DeviceDb'
import { UserDb } from '../db/models/UserDb'


export interface Context {
    settings: {
        currentTime: Date,
        currentTimeString: string,
        currentSeason: number,
        bonusTimeout: number,
        gameStarted: boolean,
    },
    authDevice: {
        token?: string,
        data: null | DeviceDb,
        register: (token: string) => void,
        remove: () => void,
    },
    authUser: {
        token?: string,
        data: null | UserDb,
        login: (token: string) => void,
        reset: () => void,
        logout: () => void,
        getSession: () => void,
    }
}
