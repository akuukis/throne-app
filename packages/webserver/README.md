See docker-compose-dev.yaml

Always build before `docker-compose -f ./docker-compose-dev.yaml up`, because `lib/` folder is what matter, not `src/`. See `yarn watch`.

### Running lapi.tronis.lv

To do anything in lapi.tronis.lv please do following:

In top right corner click settings gear icon
in json change

    "request.credentials": "omit", 
to

    "request.credentials": "include",

and save.

### Running Queries in lapi.tronis.lv

* Then to be able to run simple queries run first this mutation:
    ```
    mutation RegisterDevice {
        auth {
            deviceRegister,
        }
    }
    ```
* Go to lpma.tronis.lv
* log in (default password: root/qwerty)
* open database (default `tronis`)
* open table `devices`
* locate last entry (`auth_token` should match returned code of mutation)
* change team_id to any existing team id from table `teams`
* change `is_enabled` to 1

Device should be registered now. Registration cookie expires after 3 days and then need to register again

### Running Mutations in lapi.tronis.lv

To run mutations You need to register as user.

For that run:

    ```
    mutation Registeruser($qrCode: String!) {
        auth {
            userLogin(qrCode: $qrCode)
        }
    }
    ```

WITH query variables:

    ```
    {
        "qrCode": "PL-22-93"
    }
    ```

User has 3 minute session after last mutation. Each mutation resets it back to 3 minute mark.

QR code can be changed as needed, but that QR must have `category_id=3` and must be linked to user `users` table `qr_id` and `gr_code` rows).

Also see team restrictions from game rules (actions user can do only on his own device vs actions that can be done on other devices).

### Pseudo-code for pillar target calculation

```ts
const AVG_SEASON_LENGTH = 80

const pillarXHistory = [
    {season: 1, totalPoints: 60, broughtOverPoints: 0, eventPoints: 0},
    {season: 2, totalPoints: 86, broughtOverPoints: 16, eventPoints: 10},
]

const getPillarNextTarget = (history = pillarXHistory, minutesElapsed: number, broughtOverPoints = 0, eventPoints = 0) => {
    const pillarWithRawScore = history.map((season) => ({
        ...season,
        rawPoints: season.totalPoints - season.broughtOverPoints - season.eventPoints,
    }))

    const cumulativePoints = pillarWithRawScore.reduce((sum, {rawPoints}) => sum + rawPoints, 0)
    const n = history.length + 1

    const targetNaive = AVG_SEASON_LENGTH * n * cumulativePoints / minutesElapsed - cumulativePoints
    const target = targetNaive + broughtOverPoints + eventPoints

    return Math.round(target)
}

console.log(getPillarNextTarget(pillarXHistory, 160, 0, 0))
console.log(getPillarNextTarget(pillarXHistory, 200, 10, 10))
console.log(getPillarNextTarget(pillarXHistory, 120, 10, 10))
```
