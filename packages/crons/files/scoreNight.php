<?php

function import($database)
{
    $NIGHT_PILLAR = 5;
    $VICTORY_POINT_TYPE = 1;

    $night_was_scored = $database->get('settings', ['value'], ['id' => SETTING_NIGHT_SCORING]);
    $night_season_id = $database->get('settings', ['value'], ['id' => SETTING_NIGHT_SEASON]);
    $now = $database->get('settings', ['value'], ['id' => SETTING_TICKER]);
    $nowDate = date('Y-m-d H:i:s', $now);

    if ($night_was_scored['value' == "1"]) {
        throw new Exception('Night already scored');
    }

    $score_reasons = new ScoreReason();
    $score_reasons->genereatelist($database);

    $teamScores = $database->select('score_totals', ['rank', 'team_id'], [
            'type_id' => $NIGHT_PILLAR,
    ]);

    foreach ($teamScores as $score) {
        $vp_reason = $score_reasons->reasonMap[5][$score['rank'] - 1];

        $database->insert('scores', [
            'period_id' => $night_season_id,
            'reason_id' => $vp_reason,
            'team_id' => $score['team_id'],
            'type_id' => $VICTORY_POINT_TYPE,
            'score_time' => $nowDate,
            'created' => $nowDate,
        ]);
    }

    $database->update('settings', ['value' => 1], ['id' => SETTING_NIGHT_SCORING]);
}
