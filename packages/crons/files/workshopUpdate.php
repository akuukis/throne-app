<?php

function calculateRequiredPoints($start, $now, $points, $workTime) {
    $period = floor($workTime / $points);

    $ellapsed = $now - $start;

    return floor($ellapsed / $period);
}

function insertNewPoints($teamId, $jobId, $scoreReason, $seasonId, $scoreReasonList, $database, $now) {
    $score = [
        'type_id' => $scoreReasonList->types['industry'],
        'reason_id' => $scoreReason,
        'team_id' => $teamId,
        'period_id' => $seasonId,
        'details' => $jobId,
        'score' => date('Y-m-d H:i:s', $now)
    ];

    $database->insert('scores', $score);

    $where = [
        'team_id' => $teamId,
        'period_id' => $seasonId,
        'type_id' => $scoreReasonList->types['industry'],
    ];

    $score_totals = $database->get('score_totals', ['id', 'score'], $where);

    if (!$score_totals) {
        $score_totals = $where;
        $score_totals['score'] = 1;
        $score_totals['last_update'] = date('Y-m-d H:i:s', $now);
        $database->insert('score_totals', $score_totals);
    } else {
        $score_totals['score'] = $score_totals['score'] + 1;
        $score_totals['last_update'] = date('Y-m-d H:i:s', $now);
        $database->update('score_totals', $score_totals, ['id'=>$score_totals['id']]);
    }
}

function payTransaction($database, $payment, $team, $actionId, $nowDate) {
    $RESOURCE_ACTION = 3;

    $resourceTotal = $database->get('resource_total', ['id', 'locked_amount', 'spent_amount', 'team_id'], ['team_id' => $team, 'resource_type_id' => $payment['type']]);

    if (!$resourceTotal || $resourceTotal['locked_amount'] < $payment['amount']) {
        $ty = $payment['type'];
        $am = $payment['amount'];
        throw new Exception("Team --$team-- has not enought --$ty--, amount: --$am--! Workshop activation stopped! Please check!");
    }

    $database->update('resource_total', [
        'locked_amount' => $resourceTotal['locked_amount'] - $payment['amount'],
        'spent_amount' => $resourceTotal['spent_amount'] + $payment['amount']
    ], ['id' => $resourceTotal['id']]);

    $database->insert('resource_log', [
        'action_id'=>$RESOURCE_ACTION,
        'amount'=>$payment['amount'],
        'resource_type_id'=> $payment['type'],
        'team_id' => $team,
        'details'=>$actionId,
        'created'=>$nowDate
    ]);
}

function payResources($data, $database, $nowDate) {
    $prices = json_decode($data['price_details'], true);

    foreach ($prices as $price) {
        payTransaction($database, $price, $data['team_id'], $data['id'], $nowDate);
    }
}

function import($database)
{
        // start transaction (?)

        $WORKSHOP_WORK_TIME = 20 * 60;
        $WORKSHOP_KIT_COUNT = 3;
        $BONUS_TIMEOUT = 60 * 5;

        $hasAddedPoints = false;

        $scoreReasonList = new ScoreReason();
        $scoreReasonList->genereatelist($database);

        $current_season_id = $database->get('settings', ['value'], ['id' => SETTING_SEASON]);
        $now = $database->get('settings', ['value'], ['id' => SETTING_TICKER]);

        $nowDate = date('Y-m-d H:i:s', $now);

        if ($current_season_id['value'] === "0") {
            throw new Exception('Game has not started!');
        }

        // select active jobs
        $activeJobs = $database->select('workshop_activations', [
            "[><]workshops" => ["workshop_id" => "id"],
            "[><]workshop_type" => ["workshops.type_id" => "id"]
        ], [
            'workshop_activations.id',
            'bonuses_awarded',
            'workshop_id',
            'scored_points',
            'last_updated',
            'kit_type_id',
            'workshops.team_id',
            'started',
            'workshop_type.score_reason_id',
            'workshop_type.bonus_reason_id',
            'workshop_type.ip_amount',
            'workshop_type.bonus_ip',
            'workshop_type.wood_cost',
            'workshop_type.coin_cost',
            'workshop_type.resource_cost',
            'price_details'
        ],
            ['cancelled' => null, "finished" => null, "ORDER" => ["workshop_activations.created" => "ASC"]]
        );

        // organize activations by workhsops
        $perWorkshop = [];
        foreach ($activeJobs as $activeJob) {
            if (!isset($perWorkshop[$activeJob['workshop_id']])) {
                $perWorkshop[$activeJob['workshop_id']] = [];
            }

            $perWorkshop[$activeJob['workshop_id']][] = $activeJob;
        }

        $currentFreeKits = $database->select('workshop_kit', ['id', 'type_id'], ['team_id' => null]);

        $similarKits = false;
        $similarKitType = null;
        $kit_types = [];

        foreach ($currentFreeKits as $kit) {
            if (!$kit_types[$kit['type_id']]) {
                $kit_types[$kit['type_id']] = 0;
            }

            $kit_types[$kit['type_id']] = $kit_types[$kit['type_id']] + 1;

            if ($kit_types[$kit['type_id']] >= $WORKSHOP_KIT_COUNT) {
                $similarKits = true;
                $similarKitType = $kit['type_id'];
            }
        }

        foreach ($perWorkshop as $workshop) {
            $currentJob = $workshop[0];
            $newJob = null;

            $firstPoint = false;
            $secondPoint = false;

            if ($currentJob['started']) {
                $workshopStarted = strtotime($currentJob['started']);

                $reachedPoints = calculateRequiredPoints($workshopStarted, $now, $currentJob['ip_amount'], $WORKSHOP_WORK_TIME);

                if ($reachedPoints > $currentJob['scored_points']) {
                    insertNewPoints($currentJob['team_id'], $currentJob['id'], $currentJob['score_reason_id'], $current_season_id['value'], $scoreReasonList, $database, $now);
                    $currentJob['scored_points'] = $currentJob['scored_points'] + 1;
                    if ($currentJob['scored_points'] == 1 && $currentJob['bonuses_awarded'] == 0) {
                        $firstPoint = true;
                    } else {
                        $secondPoint = true;
                    }

                    $hasAddedPoints = true;
                }

                // check for bonuses!
                if ($similarKits) {
                    if ($similarKitType == $currentJob['kit_type_id'] && $currentJob['bonus_ip'] > $currentJob['bonuses_awarded']) {
                        $lastAwarded = $database->get('scores', ['created'], [
                           'details' => $currentJob['id'],
                           'score_time[<=]' => $nowDate,
                           'reason_id' => $currentJob['bonus_reason_id'],
                           'ORDER' => ['created' => 'DESC'],
                        ]);

                        if (!$lastAwarded) {
                            $currentJob['bonuses_awarded'] = $currentJob['bonuses_awarded'] + 1;
                            insertNewPoints($currentJob['team_id'], $currentJob['id'], $currentJob['bonus_reason_id'], $current_season_id['value'], $scoreReasonList, $database, $now);
                            $hasAddedPoints = true;
                            if (!$secondPoint &&  $currentJob['bonuses_awarded'] == 1) {
                                $firstPoint = true;
                            }
                        } else {
                            $lastAwardedTime = strtotime($lastAwarded['created']);
                            if ($now - $lastAwardedTime > $BONUS_TIMEOUT) {
                                $currentJob['scored_points'] = $currentJob['scored_points'] + 1;
                                insertNewPoints($currentJob['team_id'], $currentJob['id'], $currentJob['bonus_reason_id'], $current_season_id['value'], $scoreReasonList, $database, $now);
                                $hasAddedPoints = true;
                                if (!$secondPoint &&  $currentJob['bonuses_awarded'] == 1) {
                                    $firstPoint = true;
                                }
                            }
                        }
                    }
                }

                if ($hasAddedPoints) {
                    if ($reachedPoints >= $currentJob['ip_amount']) {
                        // check if all points awarded and if yes - finish activation
                        $currentJob['finished'] = date("Y-m-d H:i:s", $now);

                        // if more jobs - set next to start
                        if (isset($workshop[1])) {
                            $newJob = $workshop[1];
                        } else {
                            $database->update('workshop', ['is_active' => 0], ['id' => $currentJob['workshop_id']]);
                        }
                    }

                    // locked resources to spent
                    if ($firstPoint) {
                        payResources($currentJob, $database,$nowDate);
                    }

                    $database->update('workshop_activations', [
                        'finished'=>$currentJob['finished'],
                        'scored_points'=>$currentJob['scored_points'],
                        'bonuses_awarded'=>$currentJob['bonuses_awarded'],
                    ], ['id' => $currentJob['id']]);
                }
            } else {
                // if first job in queue is not started
                $newJob = $currentJob;
            }

            // start new job
            if ($newJob) {
                $database->update('workshop_activations', [
                    'started' => date('Y-m-d H:i:s', $now),
                ], ['id' => $newJob['id']]);
            }
        }

        if ($hasAddedPoints) {
            // if any points were added update scoring points

            $teamTotals = $database->select('score_totals', ['id', 'score', 'team_id', 'rank'], [
                'period_id' => $current_season_id['value'],
                'type_id' => $scoreReasonList->types['industry']
            ]);

            $newTotals = [];

            // prepare for sort function
            foreach ($teamTotals as $teamTotal) {
                $teamTotal['lastTotal'] = ['rank' => $teamTotal['rank']];

                $newTotals[] = $teamTotal;
            }

            usort($newTotals, 'scoreSort');

            $sum = 0;

            // update team rankings based on score
            foreach ($newTotals as $rank => $newTotal) {
                $database->update('score_totals', [
                    'rank' => $rank + 1
                ], ['id' => $newTotal['id']]);

                $sum = $sum + $newTotal['score'];
            }

            // update global industry pillar score
            $database->update('score_type2period', ['currentTotal' => $sum], ['score_type_id' => $scoreReasonList->types['industry'], 'period_id' => $current_season_id['value']]);
        }
}
