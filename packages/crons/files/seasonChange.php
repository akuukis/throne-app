<?php

// TODO: &log=1 makes too much output and bugs out this php script during fast forward.

function recalculateTotals($database, $period, $nowDate) {
    // select reasons
    $scoreReasons = $database->select('score_reasons', ['id', 'amount', 'type_id']);

    foreach ($scoreReasons as $scoreReason) {
        $reason[$scoreReason['id']] = $scoreReason;
    }

    // select all scores in season
    $scores = $database->select('scores', ['type_id', 'reason_id', 'team_id'], [
        'period_id' => $period,
        'score_time[<=]' => $nowDate,
    ]);

    $allTotals = [];

    // add scores together
    foreach ($scores as $score) {
        if (!$allTotals[$score['type_id']]) {
            $allTotals[$score['type_id']] = [];
        }

        $allTotals[$score['type_id']][$score['team_id']]['score'] += intval($reason[$score['reason_id']]['amount']);
        $allTotals[$score['type_id']][$score['team_id']]['team_id'] = $score['team_id'];
    }

    $allTeams =  $database->select('teams', ['id']);

    foreach ($allTeams as $team) {
        if (!isset($allTotals[1][$team['id']])) {
            $allTotals[1][$team['id']] = ['score' => 0, 'team_id' => $team['id']] ;
        }

        if (!isset($allTotals[2][$team['id']])) {
            $allTotals[2][$team['id']] = ['score' => 0, 'team_id' => $team['id']];
        }

        if (!isset($allTotals[3][$team['id']])) {
            $allTotals[3][$team['id']] = ['score' => 0, 'team_id' => $team['id']];
        }

        if (!isset($allTotals[4][$team['id']])) {
            $allTotals[4][$team['id']] = ['score' => 0, 'team_id' => $team['id']];
        }
    }

    foreach ($allTotals as $type => $typeTotal) {
        foreach ($typeTotal as $team => $total) {
            // select existing total record if exists
            $teamTotal = $database->get('score_totals', ['id', 'rank'], [
                'period_id' => $period,
                'team_id' => $team,
                'type_id' => $type
            ]);

            // add id and last rank to data
            if ($teamTotal) {
                $typeTotal[$team]['id'] = $teamTotal['id'];
                $typeTotal[$team]['rank'] = $teamTotal['rank'];
                $typeTotal[$team]['team'] = $team;
            }
        }

        // sort for ranking
        usort($typeTotal, 'scoreSort2');
        $newTypeTotal = array_reverse($typeTotal);

        $sum = 0;

        // update team rankings based on score
        foreach ($newTypeTotal as $rank => $total) {
            if ($total['id']) {
                if ($total['score'] > 0) {
                    $database->update('score_totals', [
                        'rank' => $rank + 1,
                        'score' => $total['score'],
                        'last_update' => $nowDate,
                    ], ['id' => $total['id']]);
                }
            } else {
                $database->insert('score_totals', [
                    'rank' => $rank + 1,
                    'score' => $total['score'],
                    'team_id' => $total['team_id'],
                    'type_id' => $type,
                    'period_id' => $period,
                    'last_update' => $nowDate,
                ]);
            }

            $sum = $sum + $total['score'];
        }

        // update global industry pillar score
        $database->update('score_type2period', ['currentTotal' => $sum], ['score_type_id' => $type, 'period_id' => $period]);
    }
}

function overflowReducer($list, $team)
{
    $OVERFLOW_FOR_LAST_TEAM = 6;

    if (!empty($list)) {
        $previous = count($list) - 1;

        if ($list[$previous]['score'] > $OVERFLOW_FOR_LAST_TEAM && $team['score'] > $OVERFLOW_FOR_LAST_TEAM) {
            $list[$previous]['amount'] = $list[$previous]['score'] - $team['score'];
        }

        if ($list[$previous]['score'] <= $OVERFLOW_FOR_LAST_TEAM && $team['score'] <= $OVERFLOW_FOR_LAST_TEAM) {
            $list[$previous]['amount'] = $list[$previous]['score'] - $team['score'];
        }
    }

    // calculate temp overflow

    if ($team['score'] > $OVERFLOW_FOR_LAST_TEAM) {
        $over = $team['score'] - $OVERFLOW_FOR_LAST_TEAM;
    } else {
        $over = $team['score'];
    }

    $list[] = ['id' => $team['team'], 'score' => $team['score'], 'amount' => $over, 'event_points' => $team['lastTotal']['event_points']];

    return $list;
}


function getPillarNextTarget($history, $secondsElapsed, $broughtOverPoints, $eventPoints, $AVG_SEASON_LENGTH)
{
    $minutesElapsed = Round($secondsElapsed / 60);

    // TODO: Dev feature, remove after test(?)
    /*
    if ($minutesElapsed > 100) {
        $minutesElapsed = 100;
    }
    */

    $sum = 0;

    foreach ($history as $hist) {
        $sum = $sum + ($hist['currentTotal'] - $hist['overflow'] - $hist['event']);
    }

    $n = count($history) + 1;

    $targetNaive = $AVG_SEASON_LENGTH * $n * $sum / $minutesElapsed - $sum;

    $target = $targetNaive + $broughtOverPoints + $eventPoints;

    return round($target, 0);
}

function gameStart($database) {
    $game_started_time = $database->get('settings', ['value'], ['id' => SETTING_GAME_STARTED_TIME]);

    if (strtotime($game_started_time['value']) > time()) {
        return false;
    }

    // stop feast night
    $database->update('settings', ['value' => "0"], ['id' => SETTING_IS_FEAST]);

    // Step: START THE GAME
    $database->update('settings', ['value' => "1"], ['id' => SETTING_GAME_STARTED]);

    return true;
}

function gameStop($database) {
    // Step: START THE GAME
    $database->update('settings', ['value' => "0"], ['id' => SETTING_GAME_STARTED]);
    $database->update('settings', ['value' => "1"], ['id' => SETING_GAME_FINISHED]);
}
function import($database) {
    $isFinished = $database->get('settings', ['value'], ['id' => SETING_GAME_FINISHED]);

    if ($isFinished['value'] === "1") {
        throw new Exception('Game has ended!');
    }

    $now = $database->get('settings', ['value'], ['id' => SETTING_TICKER]);
    $fastForward = $database->get('settings', ['value'], ['id' => SETTING_FAST_FORWARD]);

    $game_started_setting = $database->get('settings', ['value'], ['id' => SETTING_GAME_STARTED]);

    if ($game_started_setting['value'] === "0" && !gameStart($database)) {
        throw new Exception('Game has not started!');
    }

    try {
        $timeNow = intval($now['value']);
        $timeFast = intval($fastForward['value']);

        if ($timeNow < $timeFast) {
            for ($timeNow; $timeNow <= $timeFast; $timeNow++) {
                $nowDate = date('Y-m-d H:i:s', $timeNow);
                increment($database, $nowDate, $timeNow);
            }

            $database->update('settings', ['value' => $timeFast], ['id' => SETTING_TICKER]);
        } else {
            $nowDate = date('Y-m-d H:i:s', $timeNow);
            increment($database, $nowDate, $timeNow);
        }
    } catch (Exception $e) {
        if (isset($_GET['log'])) {
            var_dump($database->log());
        }
        echo "Failed with: ", $e->getMessage(), "\n";
    }
}

function increment($database, $nowDate, $now)
{
    $FIRST_SEASON_ID = 1;
    $SCORING_PILLAR_THRESHHOLD = 2;
    $VICTORY_POINT_TYPE = 1;
    $OVERFLOW_FOR_LAST_TEAM = 6;

    $PILLARS = [2, 3, 4];

    $doSeasonChange = false;

    $season = null;

    $current_season_id = $database->get('settings', ['value'], ['id' => SETTING_SEASON]);
    $season_min = $database->get('settings', ['value'], ['id' => SETTING_MIN_SEASON]);
    $season_max = $database->get('settings', ['value'], ['id' => SETTING_MAX_SEASON]);
    $season_avg = $database->get('settings', ['value'], ['id' => SETTING_AVG_SEASON]);

    $SEASON_MIN_LENGHT = $season_min['value'] * 60;
    $SEASON_MAX_LENGHT = $season_max['value'] * 60;
    $SEASON_AVG_LENGHT_MIN = $season_avg['value'];

    $teams = $database->select('teams', ['id', 'flag_status_id']);

    show($now);

    if ($current_season_id['value'] === "0") {
        // Only first season so don't calculate result;
        logStatus('Season change due to game start', 'process');
        $doSeasonChange = true;
    } else {
        $season = $database->get('periods', ['id', 'title', 'started', 'ended', 'next_periods_id'], ['id' => $current_season_id['value']]);
        $season_started_time = strtotime($season['started']);

        show($season_started_time);

        recalculateTotals($database, $current_season_id['value'], $nowDate);

        if ($now - $season_started_time < $SEASON_MIN_LENGHT) {
            return false;
        }

        if ($now - $season_started_time >= $SEASON_MAX_LENGHT) {
            // season running longer than max time
            logStatus('Season change due to max time reached', 'process');
            $doSeasonChange = true;
        }
    }

    if ($season) {
        $maxed_pillar_count = $database
            ->query("SELECT COUNT(`id`) FROM `score_type2period` WHERE `period_id`={$current_season_id['value']} AND `currentTotal` > `target`")
            ->fetchALl();

        if ($maxed_pillar_count[0][0] >= $SCORING_PILLAR_THRESHHOLD) {
            // two pillars have reached target score
            $doSeasonChange = true;
            logStatus('Season change due to 2 pillars reaching target', 'process');
        }
    }

    if (!$doSeasonChange) {
        echo 'Season change not needed!';
        if (isset($_GET['log'])) {
            var_dump($database->log());
        }

        return true;
    }

    $next_season_id = $FIRST_SEASON_ID;

    if ($season) {
        $season['ended'] = date('Y-m-d H:i:s', $now);
        $database->update('periods', $season, ['id' => $season['id']]);
        $next_season_id = $season['next_periods_id'];

        if (!$next_season_id) {
            gameStop($database);
        }
    }

    if ($next_season_id === $FIRST_SEASON_ID) {
        recalculateTotals($database, $next_season_id, $nowDate);
    }

    if ($next_season_id) {
        $database->update('periods', ['started' => date('Y-m-d H:i:s', $now)], ['id' => $next_season_id]);
        $database->update('settings', ['value' => $next_season_id], ['id' => SETTING_SEASON]);

        // Auto-raise flags for the next season.
        foreach ($teams as $team) {
            $database->insert('flag_status_log', [
                'team_id' => $team['id'],
                'current_status_id' => $team['flag_status_id'],
                'changer_team_id' => $team['id'],
                'action' => 'register',
                'period_id' => $next_season_id,
                'created' => $nowDate,
            ]);
            // TODO: perhaps there's a better way to capture id of the newly created entry?
            $flagStatusLog = $database->select('flag_status_log', ['id'], [
                'team_id' => $team['id'],
                'created' => $nowDate,
            ]);
            $database->update('teams', ['flag_registration_id' => $flagStatusLog[0]['id']], ['id' => $team['id']]);

        }
    }

    $unusedExpansions = $database->select('expansions', ['id'], [
        'owner_team_id' => null,
        'rewarded_team_id' => null,
    ]);

    shuffle($unusedExpansions);

    foreach($teams as $key => $team) {
        $use = $unusedExpansions[$key];
        $use2 = $unusedExpansions[$key + count($teams)];

        $database->update('expansions', ['owner_team_id'=>$team['id'], 'showed'=>date('Y-m-d H:i:s', $now), 'assignment_period_id' => $next_season_id], ['id' => $use['id']]);
        $database->update('expansions', ['owner_team_id'=>$team['id'], 'showed'=>date('Y-m-d H:i:s', $now), 'assignment_period_id' => $next_season_id], ['id' => $use2['id']]);
    }

    if ($season) {
        $all_scores = $database->select('scores', ['type_id', 'team_id', 'created', 'score_time', 'reason_id'], ['period_id' => $season['id'], 'type_id' => $PILLARS, 'score_time[<=]' => $nowDate]);
        $score_reasons = new ScoreReason();
        $score_reasons->genereatelist($database);

        // let's recalculate full season scores

        $pillar_totals = [];

        foreach ($all_scores as $score) {
            if (!$pillar_totals[$score['type_id']]) {
                $pillar_totals[$score['type_id']] = [];
            }

            $amount = $score_reasons->list[$score['reason_id']];

            $pillar_totals[$score['type_id']][$score['team_id']] = isset($pillar_totals[$score['type_id']][$score['team_id']]) ? $pillar_totals[$score['type_id']][$score['team_id']] + $amount : $amount;
        }

        foreach ($teams as $team) {
            foreach($PILLARS as $pillar) {
                if (!isset($pillar_totals[$pillar][$team['id']])) {
                    $pillar_totals[$pillar][$team['id']] = 0;
                }
            }
        }

        // update, score pillars

        foreach ($pillar_totals as $pillar_id => $pillar) {
            $pillar_team_totals = [];
            // lets sort teams by scores

            foreach ($pillar as $teamId => $amount) {
                $lastTotal = $database->get('score_totals', ['score', 'last_update', 'id', 'event_points'], [
                    'team_id' => $teamId,
                    'type_id' => $pillar_id,
                    'period_id' => $season['id'],
                ]);

                $pillar_team_totals[] = ['team' => $teamId, 'score' => $amount, 'lastTotal' => $lastTotal];
            }

            usort($pillar_team_totals, "scoreSort");

            $pillar_team_totals = array_reverse($pillar_team_totals);

            $sorted_pillar_team_totals[$pillar_id] = $pillar_team_totals;

            // lets update scores and rankings and insert any missing teams scoring info

            foreach ($pillar_team_totals as $rank => $team) {
                // let's give VP to the team

                if ($team['score'] < $OVERFLOW_FOR_LAST_TEAM) {
                    $vp_reason = $score_reasons->reasonMap[$pillar_id][$rank + 1];
                } else {
                    $vp_reason = $score_reasons->reasonMap[$pillar_id][$rank];
                }

                $database->insert('scores', [
                    'period_id' => $season['id'],
                    'reason_id' => $vp_reason,
                    'team_id' => $team['team'],
                    'type_id' => $VICTORY_POINT_TYPE,
                    'score_time' => $nowDate,
                    'created' => $nowDate,
                ]);
            }

            recalculateTotals($database, $season['id'], $nowDate);

            // calculate overflow, but only if next season exists
            if ($season['next_periods_id']) {
                $overflow_reason = $score_reasons->overflowReasons[$pillar_id];
                $team_overflows = array_reduce($sorted_pillar_team_totals[$pillar_id], "overflowReducer", []);

                usort($team_overflows, "amountSort");

                $team_overflows = array_reverse($team_overflows);

                $seasonTotal = [
                    'score' => 0,
                    'event' => 0,
                ];

                $newSeasonTotalOverflow = 0;

                foreach ($team_overflows as $rank => $overflow) {
                    for ($i = 0; $i < $overflow['amount']; $i++) {
                        $database->insert('scores', [
                            'period_id' => $season['next_periods_id'],
                            'reason_id' => $overflow_reason,
                            'team_id' => $overflow['id'],
                            'type_id' => $pillar_id,
                            'score_time' => $nowDate,
                            'created' => $nowDate,
                        ]);
                    }

                    $database->update('score_totals', [
                        'score' => $overflow['amount'],
                        'rank' => $rank + 1,
                        'last_update' => $nowDate,
                    ], [
                        'team_id' => $overflow['id'],
                        'period_id' => $season['next_periods_id'],
                        'type_id' => $pillar_id
                    ]);

                    $newSeasonTotalOverflow = $newSeasonTotalOverflow + $overflow['amount'];
                    $seasonTotal['score'] = $seasonTotal['score'] + $overflow['score'];
                    $seasonTotal['event'] = $seasonTotal['event'] + $overflow['event_points'];
                }

                $database->update('score_type2period', $seasonTotal, ['score_type_id' => $pillar_id, 'period_id' => $season['id']]);
                $seasonTotals = $database->select('score_type2period', ['period_id', 'id', 'currentTotal', 'overflow', 'event', 'target'], ['score_type_id' => $pillar_id]);

                // calculate new threshold

                $firstSeason = $database->get('periods', ['started'], ['id' => $FIRST_SEASON_ID]);

                $seconds_in_game = strtotime($season['ended']) - strtotime($firstSeason);

                $event_total = 0;

                $nextSeasonThreshold = getPillarNextTarget($seasonTotals, $seconds_in_game, $newSeasonTotalOverflow, $event_total, $SEASON_AVG_LENGHT_MIN);

                $database->insert('score_type2period', ['overflow' => $newSeasonTotalOverflow, 'target' => strval($nextSeasonThreshold), 'score_type_id' => $pillar_id, 'period_id' => $next_season_id]);
                recalculateTotals($database, $next_season_id, $nowDate);
            }
        }

        // TODO: must add request for total VP's for the team in teams data and in global data (without specifying period)
    }
}

?>
