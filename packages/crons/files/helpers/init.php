<?php

set_time_limit ( 3600 );
ini_set('max_execution_time',3600);

define('SETTING_GAME_STARTED', 1);
define('SETTING_SEASON', 2);
define('SETTING_GAME_STARTED_TIME', 9);
define('SETTING_NIGHT_SCORING', 17);
define('SETTING_RESOURCES_ASSIGNED', 18);
define('SETTING_IS_FEAST', 19);
define('SETTING_NIGHT_SEASON', 21);
define('SETTING_TICKER', 22);
define('SETTING_GAME_EMERGENCY_TICKER_PAUSED', 23);
define('SETTING_GAME_TICKER_UPDATE_TIME', 24);
define('SETTING_FAST_FORWARD', 28);
define('SETING_GAME_FINISHED', 32);

define('SETTING_MIN_SEASON', 29);
define('SETTING_MAX_SEASON', 30);
define('SETTING_AVG_SEASON', 31);

require 'Medoo.php';

require 'log.php';
require 'Lock.php';

$pdo = new PDO('mysql:host=' . getenv('MYSQL_HOST') . ';dbname=' . getenv('MYSQL_DB'), getenv('MYSQL_USER'), getenv('MYSQL_PASS'));

$database = new \Medoo\Medoo([
    // Initialized and connected PDO object
    'pdo' => $pdo,
    // [optional] Medoo will have different handle method according to different database type
    'database_type' => 'mysql',
    "logging" => true,
]);

$database->query("SET NAMES 'utf8'")->fetchAll();

$locker = new Lock($database);
