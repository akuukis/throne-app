import { RESOURCE_TYPE } from './resources'


type Price = { type: RESOURCE_TYPE, amount: number }[]

export const equipmentPrice = (seasonId: number): Price => {

    const price = {
        1: [{ type: RESOURCE_TYPE.Iron, amount: 2 }, { type: RESOURCE_TYPE.Coins, amount: 2 }],
        2: [{ type: RESOURCE_TYPE.Iron, amount: 2 }, { type: RESOURCE_TYPE.Coins, amount: 1 }],
        3: [{ type: RESOURCE_TYPE.Iron, amount: 2 }, { type: RESOURCE_TYPE.Coins, amount: 0 }],
        4: [{ type: RESOURCE_TYPE.Iron, amount: 1 }, { type: RESOURCE_TYPE.Coins, amount: 2 }],
        5: [{ type: RESOURCE_TYPE.Iron, amount: 1 }, { type: RESOURCE_TYPE.Coins, amount: 1 }],
        6: [{ type: RESOURCE_TYPE.Iron, amount: 1 }, { type: RESOURCE_TYPE.Coins, amount: 0 }],
    }

    return price[seasonId]
}
