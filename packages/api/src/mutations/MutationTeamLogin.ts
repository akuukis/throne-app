import { gql } from '@apollo/client'

import { TEAM_ID } from '../constants'

import { ApiBaseMutationOptions } from './common'


export const MutationTeamLogin: ApiBaseMutationOptions<MutationTeamLogin.Response, MutationTeamLogin.Params> = {
    mutation: gql`mutation($team: Int, $name: String!) {
        auth {
            deviceRegister(team: $team, name: $name)
        }
    }`,
}

export namespace MutationTeamLogin {
    export type Params = {
        team: TEAM_ID | null
        name: string
    }
    export interface Response {
        auth: {
            deviceRegister: string
        }
    }
}
