import { gql } from '@apollo/client'

import { WorkshopActivateInput } from '../graphql.schema'
import { QUERY } from '../queries/common'

import { ApiBaseMutationOptions, MUTATION_RESULT } from './common'


export const MutationTaskQueue: ApiBaseMutationOptions<MutationTaskQueue.Response, MutationTaskQueue.Params> = {
    mutation: gql`mutation QueueTask($input: WorkshopActivateInput!) {
        workshop {
            activate(input: $input)
        }
    }`,
    refetchQueries: [
        QUERY.Session,
        QUERY.TeamResources,
        QUERY.TeamWorkshop,
    ],
}

export namespace MutationTaskQueue {
    export interface Params {
        input: WorkshopActivateInput
    }
    export interface Response {
        workshop: {
            cancel: MUTATION_RESULT
        }
    }
}
