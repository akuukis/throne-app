import { gql } from '@apollo/client'

import { QUERY } from '../queries/common'

import { ApiBaseMutationOptions, MUTATION_RESULT } from './common'


export const MutationFeastScore: ApiBaseMutationOptions<MutationFeastScore.Response, MutationFeastScore.Params> = {
    mutation: gql`mutation FeastScore($id: Int!, $qrList: [String!]!) {
        feast {
            score(id: $id, qrList: $qrList),
        }
    }`,
    refetchQueries: [
        QUERY.Session,
        QUERY.TeamFeasts,
    ],
}

export namespace MutationFeastScore {
    export interface Params {
        id: number,
        qrList: string[],
    }
    export interface Response {
        feast: {
            score: MUTATION_RESULT
        }
    }
}
