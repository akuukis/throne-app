import { gql } from '@apollo/client'

import { QUERY } from '../queries/common'

import { ApiBaseMutationOptions, MUTATION_RESULT } from './common'


export const MutationEquipmentAdd: ApiBaseMutationOptions<MutationEquipmentAdd.Response, MutationEquipmentAdd.Params> = {
    mutation: gql`mutation EquipmentAdd {
        equipment {
            add,
        }
    }`,
    refetchQueries: [
        QUERY.Session,
        QUERY.TeamEquipment,
        QUERY.UserEquipment,
    ],
}

export namespace MutationEquipmentAdd {
    export type Params = never
    export interface Response {
        equipment: {
            add: MUTATION_RESULT
        }
    }
}
