import { gql } from '@apollo/client'

import { QUERY } from '../queries/common'

import { ApiBaseMutationOptions, MUTATION_RESULT } from './common'


export const MutationResourcesSell: ApiBaseMutationOptions<MutationResourcesSell.Response, MutationResourcesSell.Params> = {
    mutation: gql`mutation ResourcesSell($id: Int!, $amount: Int!) {
        resources {
            sell(id: $id, amount: $amount)
        }
    }`,
    refetchQueries: [
        QUERY.Session,
        QUERY.TeamResources,
    ],
}

export namespace MutationResourcesSell {
    export interface Params {
        id: number
        amount: number
    }
    export interface Response {
        resources: {
            sell: MUTATION_RESULT
        }
    }
}
