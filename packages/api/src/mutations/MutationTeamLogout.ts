import { gql } from '@apollo/client'

import { QueryTeamBase } from '../queries'

import { ApiBaseMutationOptions, MUTATION_RESULT } from './common'


export const MutationTeamLogout: ApiBaseMutationOptions<MutationTeamLogout.Response, MutationTeamLogout.Params> = {
    mutation: gql`mutation TeamLogout {
        auth {
            deviceRemove,
        }
    }`,
    update: (cache) => {
        cache.writeQuery({
            ...QueryTeamBase,
            data: {
                team: null,
            },
        })
    },
}

export namespace MutationTeamLogout {
    export type Params = never
    export interface Response {
        auth: {
            deviceRemove: MUTATION_RESULT
        }
    }
}
