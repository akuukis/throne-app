import { gql } from '@apollo/client'

import { Query } from '../graphql.schema'

import { ApiBaseQueryOptions, QUERY } from './common'


export const QueryScorePillar: ApiBaseQueryOptions<QueryScorePillar.Response, QueryScorePillar.Params> = {
    query: gql`query ${QUERY.ScorePillar}($id: Int!) {
        pillar(id: $id) {
            id,
            title,
            allSeasons {
                season {
                    id,
                    started,
                    ended,
                },
                target,
                currentTotal,
                scores {
                    id,
                    created,
                    scored,
                    team { id },
                    reason {
                        id,
                        title,
                        amount,
                    }
                },
            }
        }
    }`,
}

export namespace QueryScorePillar {
    export interface Params {
        id: number
    }
    export interface Response {
        pillar: {
            id: NonNullable<Query['pillar']>['id']
            title: NonNullable<Query['pillar']>['title']
            allSeasons: Array<{
                season: Pick<NonNullable<NonNullable<Query['pillar']>['allSeasons'][0]>['season'], 'id' | 'started' | 'ended'>
                target: NonNullable<NonNullable<Query['pillar']>['season']>['target']
                currentTotal: NonNullable<NonNullable<Query['pillar']>['season']>['currentTotal']
                scores: Array<{
                    id: NonNullable<NonNullable<Query['pillar']>['season']>['scores'][0]['id']
                    created: NonNullable<NonNullable<Query['pillar']>['season']>['scores'][0]['created']
                    scored: NonNullable<NonNullable<Query['pillar']>['season']>['scores'][0]['scored']
                    team: Pick<NonNullable<NonNullable<Query['pillar']>['season']>['scores'][0]['team'], 'id'>
                    reason: {
                        id: NonNullable<NonNullable<Query['pillar']>['season']>['scores'][0]['reason']['id']
                        title: NonNullable<NonNullable<Query['pillar']>['season']>['scores'][0]['reason']['title']
                        amount: NonNullable<NonNullable<Query['pillar']>['season']>['scores'][0]['reason']['amount']
                    }
                }>
            }>
        },
    }
}
