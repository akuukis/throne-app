import { gql } from '@apollo/client'

import { Maybe, Query } from '..'

import { ApiBaseQueryOptions, QUERY } from './common'


export const QueryTeamResources: ApiBaseQueryOptions<QueryTeamResources.Response, QueryTeamResources.Params> = {
    query: gql`query ${QUERY.TeamResources} {
        team {
            id,
            coinbox,
            resources {
                id,
                type { id },
                available,
                locked,
            },
        }
    }`,
}

export namespace QueryTeamResources {
    export type Params = never
    export interface Response {
        team: Maybe<{
            id: NonNullable<Query['team']>['id']
            coinbox: NonNullable<Query['team']>['coinbox']
            resources: Array<{
                id: NonNullable<Query['team']>['resources'][0]['id']
                type: Pick<NonNullable<Query['team']>['resources'][0]['type'], 'id'>
                available: NonNullable<Query['team']>['resources'][0]['available']
                locked: NonNullable<Query['team']>['resources'][0]['locked']
            }>
        }>
    }
}
