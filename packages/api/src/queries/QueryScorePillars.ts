import { gql } from '@apollo/client'

import { Maybe, Query } from '../graphql.schema'

import { ApiBaseQueryOptions, QUERY } from './common'


export const QueryScorePillars: ApiBaseQueryOptions<QueryScorePillars.Response, QueryScorePillars.Params> = {
    query: gql`query ${QUERY.ScorePillars} {
        season {
            id,
            title,
            started,
            ended,
            pillars {
                id,
                target,
                currentTotal,
                pillar {
                    id,
                    title,
                },
                totalScores {
                    id,
                    score,
                    rank,
                    lastUpdate,
                },
                scores {
                    id,
                    created,
                    reason {
                        id,
                        title,
                        amount,
                    }
                },
            }
        }
    }`,
}

export namespace QueryScorePillars {
    export type Params = never
    export interface Response {
        season: Maybe<{
            id: NonNullable<Query['season']>['id']
            title: NonNullable<Query['season']>['title']
            started: NonNullable<Query['season']>['started']
            ended: NonNullable<Query['season']>['ended']
            pillars: Array<{
                id: NonNullable<Query['season']>['pillars'][0]['id']
                target: NonNullable<Query['season']>['pillars'][0]['target']
                currentTotal: NonNullable<Query['season']>['pillars'][0]['currentTotal']
                pillar: Pick<NonNullable<Query['season']>['pillars'][0]['pillar'], 'id' | 'title'>
                totalScores: Array<Pick<NonNullable<Query['season']>['pillars'][0]['totalScores'][0], 'id' | 'score' | 'rank' | 'lastUpdate'>>
                scores: Array<{
                    id: NonNullable<Query['season']>['pillars'][0]['scores'][0]['id']
                    created: NonNullable<Query['season']>['pillars'][0]['scores'][0]['created']
                    reason: Pick<NonNullable<Query['season']>['pillars'][0]['scores'][0]['reason'], 'id' | 'title' | 'amount'>
                }>
            }>
        }>
    }
}
