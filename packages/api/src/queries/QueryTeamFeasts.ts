import { gql } from '@apollo/client'

import { Maybe, Query } from '../graphql.schema'

import { ApiBaseQueryOptions, QUERY } from './common'


export const QueryTeamFeasts: ApiBaseQueryOptions<QueryTeamFeasts.Response, QueryTeamFeasts.Params> = {
    query: gql`query ${QUERY.TeamFeasts} {
        team {
            id,
            feastTokens {
                id,
                usedAt,
                season {
                    id,
                },
                recipe {
                    id,
                    title,
                }
            },
        }
    }`,
}

export namespace QueryTeamFeasts {
    export type Params = never
    export interface Response {
        team: Maybe<{
            id: NonNullable<Query['team']>['id']
            feastTokens: Array<{
                id: NonNullable<Query['team']>['feastTokens'][0]['id']
                usedAt: NonNullable<Query['team']>['feastTokens'][0]['usedAt']
                season: Pick<NonNullable<Query['team']>['feastTokens'][0]['season'], 'id'>
                recipe: Maybe<{
                    id: NonNullable<NonNullable<Query['team']>['feastTokens'][0]['recipe']>['id']
                    title: NonNullable<NonNullable<Query['team']>['feastTokens'][0]['recipe']>['title']
                }>,
            }>
        }>
    }
}
