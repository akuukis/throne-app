import { gql } from '@apollo/client'

import { Maybe, Query } from '..'

import { ApiBaseQueryOptions, QUERY } from './common'


export const QueryTeams: ApiBaseQueryOptions<QueryTeams.Response, QueryTeams.Params> = {
    query: gql`query ${QUERY.Teams} {
        teams {
            id,
            title,
            color,
            castle {
                title,
                longitude,
                latitude
            }
        }
    }`,
}

export namespace QueryTeams {
    export type Params = never
    export interface Response {
        teams: Array<{
            id: NonNullable<Query['teams'][0]>['id'],
            title: NonNullable<Query['teams'][0]>['title'],
            color: NonNullable<Query['teams'][0]>['color'],
            castle: Maybe<Pick<NonNullable<NonNullable<Query['teams'][0]>['castle']>, 'title' | 'longitude' | 'latitude'>>,
        }>
    }
}
