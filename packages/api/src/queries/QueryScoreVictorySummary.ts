import { gql } from '@apollo/client'

import { Maybe, Query } from '../graphql.schema'

import { ApiBaseQueryOptions, QUERY } from './common'


export const QueryScoreVictorySummary: ApiBaseQueryOptions<QueryScoreVictorySummary.Response, QueryScoreVictorySummary.Params> = {
    query: gql`query ${QUERY.ScoreVictorySummary} {
        victoryPoints {
            points {
                reason { amount }
                season { id }
                team { id }
            },
            totals {
                points,
                team {
                    id,
                    title,
                    color
                }
            }
        }
    }`,
}

export namespace QueryScoreVictorySummary {
    export type Params = never
    export interface Response {
        victoryPoints: Maybe<{
            points: Array<{
                reason: Pick<NonNullable<Query['victoryPoints']>['points'][0]['reason'], 'amount'>,
                season: Pick<NonNullable<Query['victoryPoints']>['points'][0]['season'], 'id'>,
                team: Pick<NonNullable<Query['victoryPoints']>['points'][0]['team'], 'id'>,
            }>
            totals: Array<{
                points: NonNullable<Query['victoryPoints']>['totals'][0]['points']
                team: Pick<NonNullable<Query['victoryPoints']>['totals'][0]['team'], 'id' | 'title' | 'color'>
            }>
        }>
    }
}
