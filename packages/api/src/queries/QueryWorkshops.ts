import { gql } from '@apollo/client'

import { Query } from '../graphql.schema'

import { ApiBaseQueryOptions, QUERY } from './common'


export const QueryWorkshops: ApiBaseQueryOptions<QueryWorkshops.Response, QueryWorkshops.Params> = {
    query: gql`query ${QUERY.Workshops} {
        workshops {
            id,
            coin,
            cooldown,
            ip,
            resourceCost,
            score {
                id,
                title,
            },
            wood,
        },
    }`,
}

export namespace QueryWorkshops {
    export type Params = never
    export interface Response {
        workshops: Array<{
            id: Query['workshops'][0]['id']
            coin: Query['workshops'][0]['coin']
            cooldown: Query['workshops'][0]['cooldown']
            ip: Query['workshops'][0]['ip']
            resourceCost: Query['workshops'][0]['resourceCost']
            score: Pick<Query['workshops'][0]['score'], 'id' | 'title'>
            wood: Query['workshops'][0]['wood']
        }>
    }
}
