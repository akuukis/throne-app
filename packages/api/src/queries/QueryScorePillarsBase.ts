import { gql } from '@apollo/client'

import { Query } from '../graphql.schema'

import { ApiBaseQueryOptions, QUERY } from './common'


export const QueryScorePillarsBase: ApiBaseQueryOptions<QueryScorePillarsBase.Response, QueryScorePillarsBase.Params> = {
    query: gql`query ${QUERY.ScorePillarsBase} {
        pillars {
            id,
            title,
        }
    }`,
}

export namespace QueryScorePillarsBase {
    export type Params = never
    export interface Response {
        pillars: Array<{
            id: Query['pillars'][0]['id']
            title: Query['pillars'][0]['title']
        }>
    }
}
