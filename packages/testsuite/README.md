## Testing

Open from the test folder!

Runs all testsuites concurently, automatically spawning and cleaning up a docker instance per testsuite.

```sh
yarn test  # run all testsuite
yarn test main # run the big main test file
yarn test connection  # run the small connection check file
yarn resume 1  # resume instance No1. See instances at http://lpma.tronis.lv:8x based on No.
bash run/files/run-manually.sh to move time forward.
```

### Time forwarding

1. run this query
```
mutation debug($target: Int!) {
    debug {
        setTick(target: $target)
    }
}
```
2. call http://lcron.tronis.lv/?do=season&run=1&log=1
3. check current_tick to see if it succeeded
4. if not, see errors at output of step 2

