/**
 * @teststate state-testsuite
 */
/* eslint-disable @typescript-eslint/no-non-null-assertion */
import {
    FLAG_STATUS,
    PILLAR,
    QueryRecipes,
    QueryScorePillar,
    QueryScorePillarSummary,
    QueryScorePillars,
    QueryScorePillarsBase,
    QueryScoreVictorySummary,
    QuerySeason,
    QuerySession,
    QuerySettings,
    QueryTeamBase,
    QueryTeamEquipment,
    QueryTeamFeasts,
    QueryTeamFlag,
    QueryTeamNextRespawn,
    QueryTeamResources,
    QueryTeamUpgrades,
    QueryTeamWorkshop,
    QueryTeams,
    QueryTime,
    QueryUserCharacter,
    QueryUserEquipment,
    QueryUserName,
    QueryWorkshops,
    RESOURCE_TYPE,
    TEAM_ID,
} from '@larts/api'

import { QR_BY_TYPE } from '../src/constants'
import { beforeAllReturn, expectGraphqlQuery, testGraphqlError, testTeamScore, testTeamScores } from '../src/utils'


beforeAll(async () => {
    await global.admin.setupGame()
})


const TEAM_A = TEAM_ID.Snakes // Note: must be 1-3 and not 4+, because test db has only 3 teams.
const TEAM_B = TEAM_ID.Bears // Note: must be 1-3 and not 4+, because test db has only 3 teams.
const TEAM_C = TEAM_ID.Lynx // Note: must be 1-3 and not 4+, because test db has only 3 teams.
const MEMBER_1 = 'PL-22-93' // Must exist in db file.
const MEMBER_2 = 'PL-23-jJ' // Must exist in db file.
const MEMBER_3 = 'PL-24-fs' // Must exist in db file.
const MEMBER_4 = 'PL-2E-iB' // Must exist in db file.
const OTHER_1 = 'PL-25-3K' // Must exist in db file.
const OTHER_2 = 'PL-28-Ft' // Must exist in db file.
const OTHER_3 = 'PL-27-AB' // Must exist in db file.
const OTHER_4 = 'PL-2F-LS' // Must exist in db file.
const THIRD_1 = 'PL-2B-uT' // Must exist in db file.
const USER_INVALID = 'PL-00-00' // Must exist in db file.
const USER_UNASSIGED = 'PL-4T-pt' // Must exist in db file.
const WRISTBAND_UNUSED = 10000
const WRISTBAND_A1_1 = 11111
const WRISTBAND_A1_1_SIMILAR = 11112
const WRISTBAND_A1_2 = 22222
const WRISTBAND_A2_1 = 33333
const WRISTBAND_B1_1 = 44444
const WRISTBAND_C1_1 = 55555
const WRISTBAND_SHORT = 1234
const WRISTBAND_LONG = 123456

const FLAG_A = 'FL-22-Xn'
const FLAG_B = 'FL-23-qx'
const FLAG_C = 'FL-24-uP'

const teamA = beforeAllReturn(() => global.clientAnonymous.loginTeam(TEAM_A))
const teamB = beforeAllReturn(() => global.clientAnonymous.loginTeam(TEAM_B))
const teamC = beforeAllReturn(() => global.clientAnonymous.loginTeam(TEAM_C))
const teamMarketplace = beforeAllReturn(() => global.clientAnonymous.loginTeam(null))

const userA1 = beforeAllReturn(() => teamA.current!.loginUser(MEMBER_1))
const userA2 = beforeAllReturn(() => teamA.current!.loginUser(MEMBER_2))
const userA3Marketplace = beforeAllReturn(() => teamMarketplace.current!.loginUser(MEMBER_3))
const userB1 = beforeAllReturn(() => teamB.current!.loginUser(OTHER_1))
const userC1 = beforeAllReturn(() => teamC.current!.loginUser(THIRD_1))


describe('LOGIN', () => {

    describe ('login a team', () => {
        test('team is logged in', () => expectGraphqlQuery(teamA.current!.queryTeamBase(), { team: { id: TEAM_A } } as QueryTeamBase.Response))
    })

    describe('login a user from the correct team', () => {
        test('user is logged in', () => expectGraphqlQuery(userA1.current!.queryUserName(), { user: { id: expect.any(Number) } } as QueryUserName.Response))
    })

    describe ('Login a user with invalid QR code', () => {
        testGraphqlError(`QR not found in db ${USER_INVALID}`, () => teamA.current!.loginUser(USER_INVALID))
    })

    describe ('Login a non-existent user', () => {
        testGraphqlError('Please enter correct QR code!', () => teamA.current!.loginUser(USER_UNASSIGED))
    })

    describe ('login a user from wrong team', () => {
        testGraphqlError('Cant login in opposing teams device!', () => teamA.current!.loginUser(OTHER_1))
    })

})

describe('INIT', () => {
    const INITIAL_TICK = 1

    describe('initial state before the game', () => {
        test(`current tick is ${INITIAL_TICK}`, () => expect(global.clientAnonymous.currentTick()).resolves.toEqual(INITIAL_TICK))
        test('game has not started ("switch-game_started" is "0")', () => expectGraphqlQuery(global.clientAnonymous.querySettings(), { baseData: { settings: expect.arrayContaining([expect.objectContaining({ code: 'switch-game_started', value: '0' })]) } } as QuerySettings.Response))
        test('current season is 0 ("var-current_period" is "0")', () => expectGraphqlQuery(global.clientAnonymous.querySettings(), { baseData: { settings: expect.arrayContaining([expect.objectContaining({ code: 'var-current_period', value: '0' })]) } } as QuerySettings.Response))
        test('current season is 1 (the id)', () => expectGraphqlQuery(userA1.current!.querySeason({}), { season: { id: 1, started: null } } as QuerySeason.Response))
        test('team A has only the hardcoded testsuite-only upgrade', () => expectGraphqlQuery(userA1.current!.queryTeamUpgrades(), { team: { expansions: expect.arrayContaining([
            expect.objectContaining({ rewarded: null, showedAt: 2, owners: expect.objectContaining({ id: TEAM_A }) }),
        ]) } } as QueryTeamUpgrades.Response))
        test('team B has only the hardcoded testsuite-only upgrade', () => expectGraphqlQuery(userB1.current!.queryTeamUpgrades(), { team: { expansions: expect.arrayContaining([
            expect.objectContaining({ rewarded: null, showedAt: 2, owners: expect.objectContaining({ id: TEAM_B }) }),
        ]) } } as QueryTeamUpgrades.Response))
        test('team C has only the hardcoded testsuite-only upgrade', () => expectGraphqlQuery(userC1.current!.queryTeamUpgrades(), { team: { expansions: expect.arrayContaining([
            expect.objectContaining({ rewarded: null, showedAt: 2, owners: expect.objectContaining({ id: TEAM_C }) }),
        ]) } } as QueryTeamUpgrades.Response))
    })

    describe ('initial state right after "start-time"', () => {
        beforeAllReturn(() => global.clientAnonymous.mutationDebugSetTick({ tick: INITIAL_TICK }))
        test(`current tick is ${INITIAL_TICK}`, () => expect(global.clientAnonymous.currentTick()).resolves.toEqual(INITIAL_TICK))
        test('game has started ("switch-game_started" is "1")', () => expectGraphqlQuery(global.clientAnonymous.querySettings(), { baseData: { settings: expect.arrayContaining([expect.objectContaining({ code: 'switch-game_started', value: '1' })]) } } as QuerySettings.Response))
        test('current season is 1 ("var-current_period" is "1")', () => expectGraphqlQuery(global.clientAnonymous.querySettings(), { baseData: { settings: expect.arrayContaining([expect.objectContaining({ code: 'var-current_period', value: '1' })]) } } as QuerySettings.Response))
        test('current season is 1 (the id)', () => expectGraphqlQuery(userA1.current!.querySeason({}), { season: { id: 1, started: INITIAL_TICK } } as QuerySeason.Response))
        test('team A has 2 new random upgrades', () => expectGraphqlQuery(userA1.current!.queryTeamUpgrades(), { team: { expansions: expect.arrayContaining([
            expect.objectContaining({ rewarded: null, showedAt: 1, owners: expect.objectContaining({ id: TEAM_A }) }),
            expect.objectContaining({ rewarded: null, showedAt: 1, owners: expect.objectContaining({ id: TEAM_A }) }),
        ]) } } as QueryTeamUpgrades.Response))
        test('team B has 2 new random upgrades', () => expectGraphqlQuery(userB1.current!.queryTeamUpgrades(), { team: { expansions: expect.arrayContaining([
            expect.objectContaining({ rewarded: null, showedAt: 1, owners: expect.objectContaining({ id: TEAM_B }) }),
            expect.objectContaining({ rewarded: null, showedAt: 1, owners: expect.objectContaining({ id: TEAM_B }) }),
        ]) } } as QueryTeamUpgrades.Response))
        test('team C has 2 new random upgrades', () => expectGraphqlQuery(userC1.current!.queryTeamUpgrades(), { team: { expansions: expect.arrayContaining([
            expect.objectContaining({ rewarded: null, showedAt: 1, owners: expect.objectContaining({ id: TEAM_C }) }),
            expect.objectContaining({ rewarded: null, showedAt: 1, owners: expect.objectContaining({ id: TEAM_C }) }),
        ]) } } as QueryTeamUpgrades.Response))
        testTeamScore('team A no points in the start', userA1, PILLAR.Military, 0, 1)
        testTeamScore('team B no points in the start', userB1, PILLAR.Military, 0, 2)
        testTeamScore('team C no points in the start', userC1, PILLAR.Military, 0, 3)
        testTeamScore('team A no points in the start', userA1, PILLAR.Agriculture, 0, 1)
        testTeamScore('team B no points in the start', userB1, PILLAR.Agriculture, 0, 2)
        testTeamScore('team C no points in the start', userC1, PILLAR.Agriculture, 0, 3)
        testTeamScore('team A no points in the start', userA1, PILLAR.Industry, 0, 1)
        testTeamScore('team B no points in the start', userB1, PILLAR.Industry, 0, 2)
        testTeamScore('team C no points in the start', userC1, PILLAR.Industry, 0, 3)
    })
})

describe('CONSTANTS (unused elsewhere in testsuite)', () => {
    describe('queryScorePillarsBase', () => {
        test('', () => expectGraphqlQuery(global.clientAnonymous.queryScorePillarsBase(), {
            pillars: [
                expect.objectContaining({ id: PILLAR.Victory           , title: expect.any(String) } as QueryScorePillarsBase.Response['pillars'][0]),
                expect.objectContaining({ id: PILLAR.Military          , title: expect.any(String) } as QueryScorePillarsBase.Response['pillars'][0]),
                expect.objectContaining({ id: PILLAR.Agriculture       , title: expect.any(String) } as QueryScorePillarsBase.Response['pillars'][0]),
                expect.objectContaining({ id: PILLAR.Industry          , title: expect.any(String) } as QueryScorePillarsBase.Response['pillars'][0]),
                expect.objectContaining({ id: PILLAR.FridayFeastSpecial, title: expect.any(String) } as QueryScorePillarsBase.Response['pillars'][0]),
            ],
        } as QueryScorePillarsBase.Response))
    })
    describe('queryScorePillars', () => {
        test('', () => expectGraphqlQuery(global.clientAnonymous.queryScorePillars(), {
            season: {
                ended: null,
                id: expect.any(Number),
                started: expect.any(Number),
                title: expect.any(String),
                pillars: expect.arrayContaining([
                    expect.objectContaining({
                        id: expect.any(Number),
                        currentTotal: expect.any(Number),
                        target: expect.any(Number),
                        pillar: expect.objectContaining({
                            id: PILLAR.Military,
                            title: expect.any(String),
                        }),
                        scores: [],
                        totalScores: [
                            expect.objectContaining({ id: expect.any(Number), rank: expect.any(Number), score: expect.any(Number), lastUpdate: expect.any(Number) } as NonNullable<QueryScorePillars.Response['season']>['pillars'][0]['totalScores'][0]),
                            expect.objectContaining({ id: expect.any(Number), rank: expect.any(Number), score: expect.any(Number), lastUpdate: expect.any(Number) } as NonNullable<QueryScorePillars.Response['season']>['pillars'][0]['totalScores'][0]),
                            expect.objectContaining({ id: expect.any(Number), rank: expect.any(Number), score: expect.any(Number), lastUpdate: expect.any(Number) } as NonNullable<QueryScorePillars.Response['season']>['pillars'][0]['totalScores'][0]),
                        ],
                    } as NonNullable<QueryScorePillars.Response['season']>['pillars'][0]),
                ]),
            },
        } as QueryScorePillars.Response))
    })
    describe('queryScorePillarSummary', () => {
        test('', () => expectGraphqlQuery(global.clientAnonymous.queryScorePillarSummary({ pillarId: PILLAR.Military }), {
            pillar: {
                id: PILLAR.Military,
                title: expect.any(String),
                allSeasons: expect.arrayContaining([
                    expect.objectContaining({
                        target: expect.any(Number),
                        currentTotal: expect.any(Number),
                        season: expect.objectContaining({
                            id: expect.any(Number),
                        }),
                        totalScores: expect.arrayContaining([
                            expect.objectContaining({ id: expect.any(Number), rank: expect.any(Number), score: expect.any(Number), lastUpdate: expect.any(Number) } as NonNullable<QueryScorePillarSummary.Response['pillar']>['allSeasons'][0]['totalScores'][0]),
                            expect.objectContaining({ id: expect.any(Number), rank: expect.any(Number), score: expect.any(Number), lastUpdate: expect.any(Number) } as NonNullable<QueryScorePillarSummary.Response['pillar']>['allSeasons'][0]['totalScores'][0]),
                            expect.objectContaining({ id: expect.any(Number), rank: expect.any(Number), score: expect.any(Number), lastUpdate: expect.any(Number) } as NonNullable<QueryScorePillarSummary.Response['pillar']>['allSeasons'][0]['totalScores'][0]),
                        ]),
                    } as QueryScorePillarSummary.Response['pillar']['allSeasons'][0]),
                ]),
            },
        } as QueryScorePillarSummary.Response))
    })
    describe('queryScorePillar', () => {
        test('', () => expectGraphqlQuery(global.clientAnonymous.queryScorePillar({ id: PILLAR.Military }), {
            pillar: {
                id: PILLAR.Military,
                title: expect.any(String),
                allSeasons: expect.arrayContaining([
                    expect.objectContaining({
                        target: expect.any(Number),
                        currentTotal: expect.any(Number),
                        season: expect.objectContaining({
                            id: expect.any(Number),
                        }),
                        scores: [],
                    } as QueryScorePillar.Response['pillar']['allSeasons'][0]),
                ]),
            },
        } as QueryScorePillar.Response))
    })
    describe('querySession', () => {
        test('', () => expectGraphqlQuery(global.clientAnonymous.querySession(), {
            user: null,
            baseData: { userSessionEnd: null },
        } as QuerySession.Response))
    })
    describe('queryTeams', () => {
        test('', () => expectGraphqlQuery(global.clientAnonymous.queryTeams(), {
            teams: expect.arrayContaining([
                expect.objectContaining({ id: TEAM_A, color: expect.any(String), castle: expect.any(Object) } as QueryTeams.Response['teams'][0]),
                expect.objectContaining({ id: TEAM_B, color: expect.any(String), castle: expect.any(Object) } as QueryTeams.Response['teams'][0]),
                expect.objectContaining({ id: TEAM_C, color: expect.any(String), castle: expect.any(Object) } as QueryTeams.Response['teams'][0]),
            ]),
        } as QueryTeams.Response))
    })
    describe('queryTime', () => {
        test('', () => expectGraphqlQuery(global.clientAnonymous.queryTime(), {
            baseData: {
                currentTime: 1,
                elapsedTime: expect.any(Number),
                elapsedSeasonTime: 0,
                // settings: null,  // TODO: remove from query
            },
        } as QueryTime.Response))
    })
    describe('queryWorkshops', () => {
        test('', () => expectGraphqlQuery(global.clientAnonymous.queryWorkshops(), {
            workshops: [
                {
                    coin: 1,
                    cooldown: 1500,
                    id: 4,
                    ip: 4,
                    resourceCost: 1,
                    score: {
                        id: 13,
                        title: expect.any(String),
                    },
                    wood: 1,
                },
                {
                    coin: 2,
                    cooldown: 1500,
                    id: 5,
                    ip: 5,
                    resourceCost: 3,
                    score: {
                        id: 14,
                        title: expect.any(String),
                    },
                    wood: 1,
                },
                {
                    coin: 3,
                    cooldown: 1500,
                    id: 6,
                    ip: 6,
                    resourceCost: 7,
                    score: {
                        id: 15,
                        title: expect.any(String),
                    },
                    wood: 1,
                },
                {
                    coin: 4,
                    cooldown: 1500,
                    id: 7,
                    ip: 7,
                    resourceCost: 13,
                    score: {
                        id: 16,
                        title: expect.any(String),
                    },
                    wood: 1,
                },
                {
                    coin: 5,
                    cooldown: 1500,
                    id: 8,
                    ip: 8,
                    resourceCost: 21,
                    score: {
                        id: 17,
                        title: expect.any(String),
                    },
                    wood: 1,
                },
            ],
        } as QueryWorkshops.Response))
    })
})

describe('RESPAWN', () => {
    const INITIAL_TICK = 1

    describe('initial state', () => {
        test(`current tick is ${INITIAL_TICK}`, () => expect(global.clientAnonymous.currentTick()).resolves.toEqual(INITIAL_TICK))
        test('user A1 does NOT have a character', () => expectGraphqlQuery(userA1.current!.queryUserCharacter(), { user: { character: null } } as QueryUserCharacter.Response))
        test('user A2 does NOT have a character', () => expectGraphqlQuery(userA2.current!.queryUserCharacter(), { user: { character: null } } as QueryUserCharacter.Response))
        test('user B1 does NOT have a character', () => expectGraphqlQuery(userB1.current!.queryUserCharacter(), { user: { character: null } } as QueryUserCharacter.Response))
        test('user C1 does NOT have a character', () => expectGraphqlQuery(userC1.current!.queryUserCharacter(), { user: { character: null } } as QueryUserCharacter.Response))
    })

    describe(`user A1 fails to respawn with too short character "${WRISTBAND_SHORT}"`, () => {
        testGraphqlError('Code is incorrect', () => userA2.current!.mutationCharacterRespawn({ code: WRISTBAND_SHORT }))
        test('user A1 has NOT a character', () => expectGraphqlQuery(userA2.current!.queryUserCharacter(), { user: { character: null } } as QueryUserCharacter.Response))
    })

    describe(`user A1 fails to respawn with too long character "${WRISTBAND_LONG}"`, () => {
        testGraphqlError('Code is incorrect', () => userA2.current!.mutationCharacterRespawn({ code: WRISTBAND_SHORT }))
        test('user A1 has NOT a character', () => expectGraphqlQuery(userA2.current!.queryUserCharacter(), { user: { character: null } } as QueryUserCharacter.Response))
    })

    describe(`user A1 succeeds to respawn with valid character "${WRISTBAND_A1_1}"`, () => {
        beforeAllReturn(() => userA1.current!.mutationCharacterRespawn({ code: WRISTBAND_A1_1 }))
        test('user A1 has respawn count of 1', () => expectGraphqlQuery(userA1.current!.queryUserCharacter(), { user: { respawnCount: 1 } } as QueryUserCharacter.Response))
        test('team A has a respawn cooldown 30sec', () => expectGraphqlQuery(userA1.current!.queryTeamNextRespawn(), { team: { nextRespawnTime: INITIAL_TICK + 30 } } as QueryTeamNextRespawn.Response))
        test(`user A1 has a character with id ${WRISTBAND_A1_1}`, () => expectGraphqlQuery(userA1.current!.queryUserCharacter(), { user: { character: { charcode: WRISTBAND_A1_1 } } } as QueryUserCharacter.Response))
    })

    describe(`user A2 fails to respawn with already used character "${WRISTBAND_A1_1}"`, () => {
        testGraphqlError('Code already exists', () => userA2.current!.mutationCharacterRespawn({ code: WRISTBAND_A1_1 }))
        test('user A2 has NOT a character', () => expectGraphqlQuery(userA2.current!.queryUserCharacter(), { user: { character: null } } as QueryUserCharacter.Response))
    })

    describe(`user A1 fails to respawn with character "${WRISTBAND_A1_2}" immediately after last respawn`, () => {
        testGraphqlError('Please wait cooldown for respawn!', () => userA1.current!.mutationCharacterRespawn({ code: WRISTBAND_A1_2 }))
        test(`user A1 still has a character with id ${WRISTBAND_A1_1}`, () => expectGraphqlQuery(userA1.current!.queryUserCharacter(), { user: { character: { charcode: WRISTBAND_A1_1 } } } as QueryUserCharacter.Response))
    })

    describe(`user A1 fails to respawn with character "${WRISTBAND_A1_2}" 29 ticks after last respawn`, () => {
        beforeAllReturn(() => global.clientAnonymous.mutationDebugSetTick({ tick: INITIAL_TICK + 29 }))
        testGraphqlError('Please wait cooldown for respawn!', () => userA1.current!.mutationCharacterRespawn({ code: WRISTBAND_A1_2 }))
        test(`user A1 still has a character with id ${WRISTBAND_A1_1}`, () => expectGraphqlQuery(userA1.current!.queryUserCharacter(), { user: { character: { charcode: WRISTBAND_A1_1 } } } as QueryUserCharacter.Response))
    })

    describe(`user A1 succeeds to respawn with character "${WRISTBAND_A1_2}" 30 ticks after last respawn`, () => {
        beforeAllReturn(() => global.clientAnonymous.mutationDebugSetTick({ tick: INITIAL_TICK + 30 }))
        beforeAllReturn(() => userA1.current!.mutationCharacterRespawn({ code: WRISTBAND_A1_2 }))
        test('user A1 has respawn count of 2', () => expectGraphqlQuery(userA1.current!.queryUserCharacter(), { user: { respawnCount: 2 } } as QueryUserCharacter.Response))
        test(`user A1 has a character with id ${WRISTBAND_A1_2}`, () => expectGraphqlQuery(userA1.current!.queryUserCharacter(), { user: { character: { charcode: WRISTBAND_A1_2 } } } as QueryUserCharacter.Response))
    })

    describe(`user A2 fails to respawn with character "${WRISTBAND_A1_1_SIMILAR}" that's too similar to already used character "${WRISTBAND_A1_1}"`, () => {
        beforeAllReturn(() => global.clientAnonymous.mutationDebugSetTick({ tick: INITIAL_TICK + 30 }))
        testGraphqlError('Code already exists', () => userA2.current!.mutationCharacterRespawn({ code: WRISTBAND_A1_1_SIMILAR }))
        test('user A2 still has no character', () => expectGraphqlQuery(userA2.current!.queryUserCharacter(), { user: { character: null } } as QueryUserCharacter.Response))
    })

    describe(`user A2 succeeds to respawn with valid character "${WRISTBAND_A2_1}" 30 ticks after last respawn`, () => {
        beforeAllReturn(() => global.clientAnonymous.mutationDebugSetTick({ tick: INITIAL_TICK + 30 + 30 }))
        beforeAllReturn(() => userA2.current!.mutationCharacterRespawn({ code: WRISTBAND_A2_1 }))
        test('user A2 has respawn count of 1', () => expectGraphqlQuery(userA2.current!.queryUserCharacter(), { user: { respawnCount: 1 } } as QueryUserCharacter.Response))
        test(`user A1 has a character with id ${WRISTBAND_A2_1}`, () => expectGraphqlQuery(userA2.current!.queryUserCharacter(), { user: { character: { charcode: WRISTBAND_A2_1 } } } as QueryUserCharacter.Response))
    })

    describe(`user A2 fails to revitalize the same character "${WRISTBAND_A2_1}" immediately after`, () => {
        testGraphqlError('Please wait cooldown for respawn!', () => userA2.current!.mutationCharacterRevitalize())
        test(`user A1 has a character with id ${WRISTBAND_A2_1}`, () => expectGraphqlQuery(userA2.current!.queryUserCharacter(), { user: { character: { charcode: WRISTBAND_A2_1 } } } as QueryUserCharacter.Response))
    })

    describe(`user A2 succeeds to revitalize the same character "${WRISTBAND_A2_1}" 30 ticks after last respawn`, () => {
        beforeAllReturn(() => global.clientAnonymous.mutationDebugSetTick({ tick: INITIAL_TICK + 30 + 30 + 30 }))
        beforeAllReturn(() => userA2.current!.mutationCharacterRevitalize())
        test('user A2 has respawn count of 2', () => expectGraphqlQuery(userA2.current!.queryUserCharacter(), { user: { respawnCount: 2 } } as QueryUserCharacter.Response))
        test(`user A2 has a character with id ${WRISTBAND_A2_1}`, () => expectGraphqlQuery(userA2.current!.queryUserCharacter(), { user: { character: { charcode: WRISTBAND_A2_1 } } } as QueryUserCharacter.Response))
    })

    describe(`user B1 succeeds to respawns with code "${WRISTBAND_B1_1}" regardless that other team just respawned`, () => {
        beforeAllReturn(() => userB1.current!.mutationCharacterRespawn({ code: WRISTBAND_B1_1 }))
        test('user B1 has respawn count of 1', () => expectGraphqlQuery(userB1.current!.queryUserCharacter(), { user: { respawnCount: 1 } } as QueryUserCharacter.Response))
        test(`user B1 has a character with id ${WRISTBAND_B1_1}`, () => expectGraphqlQuery(userB1.current!.queryUserCharacter(), { user: { character: { charcode: WRISTBAND_B1_1 } } } as QueryUserCharacter.Response))
    })

    describe(`user C1 succeeds to respawns with code "${WRISTBAND_C1_1}" regardless that other team just respawned`, () => {
        beforeAllReturn(() => userC1.current!.mutationCharacterRespawn({ code: WRISTBAND_C1_1 }))
        test('user C1 has respawn count of 1', () => expectGraphqlQuery(userB1.current!.queryUserCharacter(), { user: { respawnCount: 1 } } as QueryUserCharacter.Response))
        test(`user C1 has a character with id ${WRISTBAND_C1_1}`, () => expectGraphqlQuery(userC1.current!.queryUserCharacter(), { user: { character: { charcode: WRISTBAND_C1_1 } } } as QueryUserCharacter.Response))
    })

})

describe('TRIBUTE', () => {
    const INITIAL_TICK = 124

    describe('initial state', () => {
        beforeAllReturn(() => global.clientAnonymous.mutationDebugSetTick({ tick: INITIAL_TICK }))
        test(`current tick is ${INITIAL_TICK}`, () => expect(global.clientAnonymous.currentTick()).resolves.toEqual(INITIAL_TICK))
        test(`user A1 has a character with id ${WRISTBAND_A1_2}`, () => expectGraphqlQuery(userA1.current!.queryUserCharacter(), { user: { character: { charcode: WRISTBAND_A1_2 } } } as QueryUserCharacter.Response))
        test(`user A2 has a character with id ${WRISTBAND_A2_1}`, () => expectGraphqlQuery(userA2.current!.queryUserCharacter(), { user: { character: { charcode: WRISTBAND_A2_1 } } } as QueryUserCharacter.Response))
        test(`user B1 has a character with id ${WRISTBAND_B1_1}`, () => expectGraphqlQuery(userB1.current!.queryUserCharacter(), { user: { character: { charcode: WRISTBAND_B1_1 } } } as QueryUserCharacter.Response))
    })

    describe(`user A1 fails to tribute unused character "${WRISTBAND_UNUSED}"`, () => {
        testGraphqlError(`No character found with wristband "${WRISTBAND_UNUSED}"`, () => userA1.current!.mutationCharacterTribute({ code: WRISTBAND_UNUSED }))
    })

    describe(`user A1 fails to tribute his mate's character "${WRISTBAND_A2_1}"`, () => {
        testGraphqlError('This character is in Your team!', () => userA1.current!.mutationCharacterTribute({ code: WRISTBAND_A2_1 }))
    })

    describe(`user A1 fails to tribute his own character "${WRISTBAND_A1_2}"`, () => {
        testGraphqlError('This character is in Your team!', () => userA1.current!.mutationCharacterTribute({ code: WRISTBAND_A1_2 }))
    })

    describe(`user B1 succeeds to tribute A1's old character code "${WRISTBAND_A1_1}"`, () => {
        beforeAllReturn(() => userB1.current!.mutationCharacterTribute({ code: WRISTBAND_A1_1 }))
        testTeamScore('team B gains +1 MP', userB1, PILLAR.Military, 1)
        test('user A1 still has current character', () => expectGraphqlQuery(userA1.current!.queryUserCharacter(), { user: { character: { charcode: WRISTBAND_A1_2 } } } as QueryUserCharacter.Response))
    })

    describe(`user B1 succeeds to tribute A2's current character "${WRISTBAND_A2_1}"`, () => {
        beforeAllReturn(() => userB1.current!.mutationCharacterTribute({ code: WRISTBAND_A2_1 }))
        testTeamScore('team B gains +1 MP', userB1, PILLAR.Military, 2)
        test('user A2 has no character', () => expectGraphqlQuery(userA2.current!.queryUserCharacter(), { user: { character: null } } as QueryUserCharacter.Response))
    })

    describe(`user A2 fails to revitalize his character "${WRISTBAND_A2_1}" after it was tributed`, () => {
        testGraphqlError('Code not found', () => userA2.current!.mutationCharacterRevitalize())
        test('user A2 still has no character', () => expectGraphqlQuery(userA2.current!.queryUserCharacter(), { user: { character: null } } as QueryUserCharacter.Response))
    })

    describe(`user A2 fails to respawn his character "${WRISTBAND_A2_1}" after it was tributed`, () => {
        testGraphqlError('Code already exists', () => userA2.current!.mutationCharacterRespawn({ code: WRISTBAND_A2_1 }))
        test('user A2 still has no character', () => expectGraphqlQuery(userA2.current!.queryUserCharacter(), { user: { character: null } } as QueryUserCharacter.Response))
    })

})

describe('RESOURCES', () => {
    const INITIAL_TICK = 124
    const INITIAL_COINBOX = 50
    const INITIAL_RESOURCE = new Map<RESOURCE_TYPE, number>([
        [RESOURCE_TYPE.Wood            ,  66],
        [RESOURCE_TYPE.Hides           ,  66],
        [RESOURCE_TYPE.Iron            ,  66],
        [RESOURCE_TYPE.Coins           ,  66],
        [RESOURCE_TYPE.Food_Honey      ,  60],
        [RESOURCE_TYPE.Food_Lavender   ,  66],
        [RESOURCE_TYPE.Food_Tomatoes   ,  60],
        [RESOURCE_TYPE.Food_Cucumbers  ,  60],
        [RESOURCE_TYPE.Food_Mushrooms  ,  60],
        [RESOURCE_TYPE.Food_Raspberries,  60],
    ])

    describe('initial state', () => {
        beforeAllReturn(() => global.clientAnonymous.mutationDebugSetTick({ tick: INITIAL_TICK }))
        test(`current tick is ${INITIAL_TICK}`, () => expect(global.clientAnonymous.currentTick()).resolves.toEqual(INITIAL_TICK))
        test(`user A1 has a character with id ${WRISTBAND_A1_2}`, () => expectGraphqlQuery(userA1.current!.queryUserCharacter(), { user: { character: { charcode: WRISTBAND_A1_2 } } } as QueryUserCharacter.Response))
        test(`team A has ${INITIAL_COINBOX} coins in the coinbox`, () => expectGraphqlQuery(userA1.current!.queryTeamResources(), { team: { coinbox: INITIAL_COINBOX } } as QueryTeamResources.Response))
        test('team A has known amount of resources', () => expectGraphqlQuery(userA1.current!.queryTeamResources(), { team: { resources: expect.arrayContaining([
            ...[...INITIAL_RESOURCE.entries()].map(([id, available]) => expect.objectContaining({ id, available, locked: 0 })),
        ]) } } as QueryTeamResources.Response))
    })

    describe('user A1 succeeds to scan WOOD token', () => {
        beforeAllReturn(() => userA1.current!.mutationResourcesAdd({ qrCode: QR_BY_TYPE[RESOURCE_TYPE.Wood][0] }))
        test('team A has one more Wood', () => expectGraphqlQuery(userA1.current!.queryTeamResources(), { team: { resources: expect.arrayContaining([ expect.objectContaining({ id: RESOURCE_TYPE.Wood, available: INITIAL_RESOURCE.get(RESOURCE_TYPE.Wood)! + 1 }) ]) } } as QueryTeamResources.Response))
    })

    describe('user A1 succeeds to sell 2 WOOD tokens for 3 coins', () => {
        beforeAllReturn(() => userA1.current!.mutationResourcesSell({ id: RESOURCE_TYPE.Wood, amount: 2 }))
        test('team A has 2 less Wood', () => expectGraphqlQuery(userA1.current!.queryTeamResources(), { team: { resources: expect.arrayContaining([ expect.objectContaining({ id: RESOURCE_TYPE.Wood, available: INITIAL_RESOURCE.get(RESOURCE_TYPE.Wood)! + 1 - 2 }) ]) } } as QueryTeamResources.Response))
        test('team A has 3 more Coins', () => expectGraphqlQuery(userA1.current!.queryTeamResources(), { team: { resources: expect.arrayContaining([ expect.objectContaining({ id: RESOURCE_TYPE.Coins, available: INITIAL_RESOURCE.get(RESOURCE_TYPE.Coins)! + 3 }) ]) } } as QueryTeamResources.Response))
    })

    describe('user A1 succeeds to sell 2 HIDES token for 4 coins', () => {
        beforeAllReturn(() => userA1.current!.mutationResourcesSell({ id: RESOURCE_TYPE.Hides, amount: 2 }))
        test('team A has 2 less Hides', () => expectGraphqlQuery(userA1.current!.queryTeamResources(), { team: { resources: expect.arrayContaining([ expect.objectContaining({ id: RESOURCE_TYPE.Hides, available: INITIAL_RESOURCE.get(RESOURCE_TYPE.Hides)! - 2 }) ]) } } as QueryTeamResources.Response))
        test('team A has 4 more Coins', () => expectGraphqlQuery(userA1.current!.queryTeamResources(), { team: { resources: expect.arrayContaining([ expect.objectContaining({ id: RESOURCE_TYPE.Coins, available: INITIAL_RESOURCE.get(RESOURCE_TYPE.Coins)! + 3 + 4 }) ]) } } as QueryTeamResources.Response))
    })

    describe('user A1 succeeds to add 3 coins', () => {
        beforeAllReturn(() => userA1.current!.mutationResourcesAddCoins({ amount: 3 }))
        test('team A has 3 more Coins', () => expectGraphqlQuery(userA1.current!.queryTeamResources(), { team: { resources: expect.arrayContaining([ expect.objectContaining({ id: RESOURCE_TYPE.Coins, available: INITIAL_RESOURCE.get(RESOURCE_TYPE.Coins)! + 3 + 4 + 3 }) ]) } } as QueryTeamResources.Response))
        test('team A has 3 more Coins in the coinbox', () => expectGraphqlQuery(userA1.current!.queryTeamResources(), { team: { coinbox: INITIAL_COINBOX + 3 } } as QueryTeamResources.Response))
    })

    describe('user A1 succeeds to remove 10 coins', () => {
        beforeAllReturn(() => userA1.current!.mutationResourcesRemoveCoins({ amount: 10 }))
        test('team A has 10 less Coins', () => expectGraphqlQuery(userA1.current!.queryTeamResources(), { team: { resources: expect.arrayContaining([ expect.objectContaining({ id: RESOURCE_TYPE.Coins, available: INITIAL_RESOURCE.get(RESOURCE_TYPE.Coins)! + 3 + 4 + 3 - 10 }) ]) } } as QueryTeamResources.Response))
        test('team A has 10 less Coins in the coinbox', () => expectGraphqlQuery(userA1.current!.queryTeamResources(), { team: { coinbox: INITIAL_COINBOX + 3 - 10 } } as QueryTeamResources.Response))
    })

})

describe('EQUIPMENT POINTS', () => {
    const INITIAL_TICK = 124
    const INITIAL_SEASON = 1
    const INITIAL_A_EP = 0
    const INITIAL_A1_EP = 0
    const INITIAL_IRON = 66
    const INITIAL_COIN = 66

    describe('initial state', () => {
        beforeAllReturn(() => global.clientAnonymous.mutationDebugSetTick({ tick: INITIAL_TICK }))
        test(`current tick is ${INITIAL_TICK}`, () => expect(global.clientAnonymous.currentTick()).resolves.toEqual(INITIAL_TICK))
        test(`current season is ${INITIAL_SEASON}`, () => expectGraphqlQuery(userA1.current!.querySeason({}), { season: { id: INITIAL_SEASON } } as QuerySeason.Response))
        test(`user A1 has a character with id ${WRISTBAND_A1_2}`, () => expectGraphqlQuery(userA1.current!.queryUserCharacter(), { user: { character: { charcode: WRISTBAND_A1_2 } } } as QueryUserCharacter.Response))
        test(`user A1 has ${INITIAL_A1_EP} EP`, () => expectGraphqlQuery(userA1.current!.queryUserEquipment(), { user: { equipmentPoints: INITIAL_A1_EP } } as QueryUserEquipment.Response))
        test('user A1 has NO Amulet of Phoenix', () => expectGraphqlQuery(userA1.current!.queryUserEquipment(), { user: { equipmentHasPhoenix: false } } as QueryUserEquipment.Response))
        test(`team A has ${INITIAL_A_EP} EP`, () => expectGraphqlQuery(userA1.current!.queryTeamEquipment(), { team: { availableEq: INITIAL_A_EP } } as QueryTeamEquipment.Response))
        test(`team A has ${INITIAL_IRON} Iron and ${INITIAL_COIN} coins`, () => expectGraphqlQuery(userA1.current!.queryTeamResources(), { team: { resources: expect.arrayContaining([
            expect.objectContaining({ id: RESOURCE_TYPE.Iron, available: INITIAL_IRON, locked: 0 }),
            expect.objectContaining({ id: RESOURCE_TYPE.Coins, available: INITIAL_COIN, locked: 0 }),
        ]) } } as QueryTeamResources.Response))
    })

    describe('user A1 succeeds to buy EP for 2 iron and 2 coins', () => {
        beforeAllReturn(() => userA1.current!.mutationEquipmentBuy())
        test(`team A has ${INITIAL_A_EP + 1} EP`, () => expectGraphqlQuery(userA1.current!.queryTeamEquipment(), { team: { availableEq: INITIAL_A_EP + 1 } } as QueryTeamEquipment.Response))
        test(`team A has ${INITIAL_IRON - 2} Iron and ${INITIAL_COIN - 2} coins`, () => expectGraphqlQuery(userA1.current!.queryTeamResources(), { team: { resources: expect.arrayContaining([
            expect.objectContaining({ id: RESOURCE_TYPE.Iron, available: INITIAL_IRON - 2, locked: 0 }),
            expect.objectContaining({ id: RESOURCE_TYPE.Coins, available: INITIAL_COIN - 2, locked: 0 }),
        ]) } } as QueryTeamResources.Response))
    })

    describe('user A1 fails to add Amulet of Phoenix', () => {
        testGraphqlError('User has no available EP', () => userA1.current!.mutationEquipmentEquip())
    })

    describe('user A1 succeeds to add 1 EP', () => {
        beforeAllReturn(() => userA1.current!.mutationEquipmentAdd())
        test(`user A1 has ${INITIAL_A1_EP + 1} EP`, () => expectGraphqlQuery(userA1.current!.queryUserEquipment(), { user: { equipmentPoints: INITIAL_A1_EP + 1 } } as QueryUserEquipment.Response))
        test(`team A has ${INITIAL_A_EP} EP`, () => expectGraphqlQuery(userA1.current!.queryTeamEquipment(), { team: { availableEq: INITIAL_A_EP } } as QueryTeamEquipment.Response))
    })

    describe('user A1 succeeds to add Amulet of Phoenix', () => {
        beforeAllReturn(() => userA1.current!.mutationEquipmentEquip())
        test('user A1 has Amulet of Phoenix', () => expectGraphqlQuery(userA1.current!.queryUserEquipment(), { user: { equipmentHasPhoenix: true } } as QueryUserEquipment.Response))
    })

    describe('user A1 succeeds fails to remove 1 EP', () => {
        testGraphqlError('User has to unequip Amulet of Phoenix first', () => userA1.current!.mutationEquipmentRemove())
    })

    describe('user A1 succeeds to remove Amulet of Phoenix', () => {
        beforeAllReturn(() => userA1.current!.mutationEquipmentUnequip())
        test(`user A1 has ${INITIAL_A1_EP + 1} EP`, () => expectGraphqlQuery(userA1.current!.queryUserEquipment(), { user: { equipmentPoints: INITIAL_A1_EP + 1 } } as QueryUserEquipment.Response))
        test('user A1 has NO Amulet of Phoenix', () => expectGraphqlQuery(userA1.current!.queryUserEquipment(), { user: { equipmentHasPhoenix: false } } as QueryUserEquipment.Response))
    })

    describe('user A1 succeeds to remove 1 EP', () => {
        beforeAllReturn(() => userA1.current!.mutationEquipmentRemove())
        test(`user A1 has ${INITIAL_A1_EP} EP`, () => expectGraphqlQuery(userA1.current!.queryUserEquipment(), { user: { equipmentPoints: INITIAL_A1_EP } } as QueryUserEquipment.Response))
        test(`team A has ${INITIAL_A_EP + 1} EP`, () => expectGraphqlQuery(userA1.current!.queryTeamEquipment(), { team: { availableEq: INITIAL_A_EP + 1 } } as QueryTeamEquipment.Response))
    })

})

describe('RECIPES', () => {
    const INITIAL_TICK = 124
    const INITIAL_AP = 0
    const INITIAL_AP_B = 0
    const INITIAL_AP_C = 0
    const INITIAL_RESOURCE = new Map<RESOURCE_TYPE, number>([
        [RESOURCE_TYPE.Coins           ,  64],
        [RESOURCE_TYPE.Food_Honey      ,  60],
        [RESOURCE_TYPE.Food_Lavender   ,  66],
        [RESOURCE_TYPE.Food_Tomatoes   ,  60],
        [RESOURCE_TYPE.Food_Cucumbers  ,  60],
        [RESOURCE_TYPE.Food_Mushrooms  ,  60],
        [RESOURCE_TYPE.Food_Raspberries,  60],
    ])

    describe('initial state', () => {
        beforeAllReturn(() => global.clientAnonymous.mutationDebugSetTick({ tick: INITIAL_TICK }))
        test(`current tick is ${INITIAL_TICK}`, () => expect(global.clientAnonymous.currentTick()).resolves.toEqual(INITIAL_TICK))
        test(`user A1 has a character with id ${WRISTBAND_A1_2}`, () => expectGraphqlQuery(userA1.current!.queryUserCharacter(), { user: { character: { charcode: WRISTBAND_A1_2 } } } as QueryUserCharacter.Response))
        test(`user B1 has a character with id ${WRISTBAND_B1_1}`, () => expectGraphqlQuery(userB1.current!.queryUserCharacter(), { user: { character: { charcode: WRISTBAND_B1_1 } } } as QueryUserCharacter.Response))
        test(`user C1 has a character with id ${WRISTBAND_C1_1}`, () => expectGraphqlQuery(userC1.current!.queryUserCharacter(), { user: { character: { charcode: WRISTBAND_C1_1 } } } as QueryUserCharacter.Response))
        test('team A has known amount of resources', () => expectGraphqlQuery(userA1.current!.queryTeamResources(), { team: { resources: expect.arrayContaining([
            ...[...INITIAL_RESOURCE.entries()].map(([id, available]) => expect.objectContaining({ id, available, locked: 0 })),
        ]) } } as QueryTeamResources.Response))
        test('all 23 (15 + 6 + 1 + 1) recipes are available', async () => {
            const { data: { recipes } } = await userA1.current!.queryRecipes()
            expect(recipes).toHaveLength(22)
            expect(recipes.every((recipe) => recipe.firstCookedBy === null && recipe.cookLog.length === 0)).toBe(true)
        })
        test('team A has one unused initial dish', () => expectGraphqlQuery(userA1.current!.queryTeamFeasts(), { team: { feastTokens: expect.arrayContaining([
            expect.objectContaining({ usedAt: null }),
        ]) } } as QueryTeamFeasts.Response))
        testTeamScore(`team A has ${INITIAL_AP} AP`, userA1, PILLAR.Agriculture, INITIAL_AP, 1)
        testTeamScore(`team B has ${INITIAL_AP_B} AP`, userB1, PILLAR.Agriculture, INITIAL_AP_B, 2)
        testTeamScore(`team C has ${INITIAL_AP_C} AP`, userC1, PILLAR.Agriculture, INITIAL_AP_C, 3)
    })

    describe('user A1 succeeds to invent No.21 (honey+lavender+tomato+cucumber) recipe', () => {
        beforeAllReturn(() => userA1.current!.mutationRecipeScore({ id: 21, title: 'my first' }))
        test('recipe 21 is invented', () => expectGraphqlQuery(userA1.current!.queryRecipes(), { recipes: expect.arrayContaining([expect.objectContaining({ id: 21, title: 'my first' })]) } as QueryRecipes.Response))
        test('team A has two unused dishes', () => expectGraphqlQuery(userA1.current!.queryTeamFeasts(), { team: { feastTokens: expect.arrayContaining([
            expect.objectContaining({ usedAt: null }),
            expect.objectContaining({ usedAt: null }),
        ]) } } as QueryTeamFeasts.Response))
        testTeamScore(`team A still has the same ${INITIAL_AP} AP for now`, userA1, PILLAR.Agriculture, INITIAL_AP, 1)
        test('team A has 2 food less of each recipe ingredient', () => expectGraphqlQuery(userA1.current!.queryTeamResources(), { team: { resources: expect.arrayContaining([
            expect.objectContaining({ id: RESOURCE_TYPE.Food_Honey    , available: INITIAL_RESOURCE.get(RESOURCE_TYPE.Food_Honey    )! - 1 }),
            expect.objectContaining({ id: RESOURCE_TYPE.Food_Lavender , available: INITIAL_RESOURCE.get(RESOURCE_TYPE.Food_Lavender )! - 1 }),
            expect.objectContaining({ id: RESOURCE_TYPE.Food_Tomatoes , available: INITIAL_RESOURCE.get(RESOURCE_TYPE.Food_Tomatoes )! - 1 }),
            expect.objectContaining({ id: RESOURCE_TYPE.Food_Cucumbers, available: INITIAL_RESOURCE.get(RESOURCE_TYPE.Food_Cucumbers)! - 1 }),
        ]) } } as QueryTeamResources.Response))
    })

    describe('teams A receives 1st AP after 3min (4 AP over 12min)', () => {
        beforeAllReturn(() => global.clientAnonymous.mutationDebugSetTick({ tick: INITIAL_TICK + 180 }))
        testTeamScore('team A gains +1 AP', userA1, PILLAR.Agriculture, INITIAL_AP + 1)
    })

    describe('teams A receives 2nd AP after 6min (4 AP over 12min)', () => {
        beforeAllReturn(() => global.clientAnonymous.mutationDebugSetTick({ tick: INITIAL_TICK + 180 + 180 }))
        testTeamScore('team A gains +1 AP', userA1, PILLAR.Agriculture, INITIAL_AP + 2)
    })

    describe('teams A receives 3rd AP after 9min (4 AP over 12min)', () => {
        beforeAllReturn(() => global.clientAnonymous.mutationDebugSetTick({ tick: INITIAL_TICK + 180 + 180 + 180 }))
        testTeamScore('team A gains +1 AP', userA1, PILLAR.Agriculture, INITIAL_AP + 3)
    })

    describe('teams A receives 4th AP after 12min (4 AP over 12min)', () => {
        beforeAllReturn(() => global.clientAnonymous.mutationDebugSetTick({ tick: INITIAL_TICK + 180 + 180 + 180 + 180 }))
        testTeamScore('team A gains +1 AP', userA1, PILLAR.Agriculture, INITIAL_AP + 4)
    })

    describe('user A1 succeeds to score initial dish (2 pairs) together with Team B', () => {
        beforeAllReturn(() => userA1.current!.mutationFeastScore({ id: TEAM_A, qrList: [MEMBER_1, MEMBER_2, OTHER_1, OTHER_2] }))
        test('team A has one used dish', () => expectGraphqlQuery(userA1.current!.queryTeamFeasts(), { team: { feastTokens: expect.arrayContaining([
            expect.objectContaining({ usedAt: INITIAL_TICK + 180 + 180 + 180 + 180 }),
        ]) } } as QueryTeamFeasts.Response))
        testTeamScore('team A gains +3 AP', userA1, PILLAR.Agriculture, INITIAL_AP + 4 + 2)
        testTeamScore('team B gains +6 AP', userB1, PILLAR.Agriculture, INITIAL_AP + 4, 2)
    })

    describe('user A1 succeeds to score created dish (3 pairs) with both Team B and Team C', () => {
        beforeAllReturn(() => userA1.current!.mutationFeastScore({ id: 4 /* :fingers_crossed: */, qrList: [MEMBER_1, MEMBER_2, MEMBER_3, OTHER_1, OTHER_2, THIRD_1] }))
        test('team A has two used dishes', () => expectGraphqlQuery(userA1.current!.queryTeamFeasts(), { team: { feastTokens: [
            expect.objectContaining({ usedAt: INITIAL_TICK + 180 + 180 + 180 + 180 }),
            expect.objectContaining({ usedAt: INITIAL_TICK + 180 + 180 + 180 + 180 }),
        ] } } as QueryTeamFeasts.Response))
        testTeamScore('team A gains +2 AP', userA1, PILLAR.Agriculture, INITIAL_AP + 4 + 2 + 3)
        testTeamScore('team B gains +2 AP', userB1, PILLAR.Agriculture, INITIAL_AP + 4 + 4, 2)
        testTeamScore('team C gains +2 AP', userC1, PILLAR.Agriculture, INITIAL_AP + 2, 3)
    })

})

describe('FLAGS', () => {
    const INITIAL_TICK = 1024
    const INITIAL_MP = 0

    describe('initial state', () => {
        beforeAllReturn(() => global.clientAnonymous.mutationDebugSetTick({ tick: INITIAL_TICK }))
        test(`current tick is ${INITIAL_TICK}`, () => expect(global.clientAnonymous.currentTick()).resolves.toEqual(INITIAL_TICK))
        test(`user A1 has a character with id ${WRISTBAND_A1_2}`, () => expectGraphqlQuery(userA1.current!.queryUserCharacter(), { user: { character: { charcode: WRISTBAND_A1_2 } } } as QueryUserCharacter.Response))
        testTeamScore(`team A has ${INITIAL_MP} MP`, userA1, PILLAR.Military, INITIAL_MP, 2)
        test('flag A is raised as "Glory"', () => expectGraphqlQuery(userA1.current!.queryTeamFlag({ id: TEAM_A }), { team: expect.objectContaining({ flagStatusId: FLAG_STATUS.Glory, flagRaisedTime: 1, flagScoredTime: null }) } as QueryTeamFlag.Response))
        test('flag B is raised as "Glory"', () => expectGraphqlQuery(userA1.current!.queryTeamFlag({ id: TEAM_B }), { team: expect.objectContaining({ flagStatusId: FLAG_STATUS.Glory, flagRaisedTime: 1, flagScoredTime: null }) } as QueryTeamFlag.Response))
        test('flag C is raised as "Glory"', () => expectGraphqlQuery(userA1.current!.queryTeamFlag({ id: TEAM_C }), { team: expect.objectContaining({ flagStatusId: FLAG_STATUS.Glory, flagRaisedTime: 1, flagScoredTime: null }) } as QueryTeamFlag.Response))
    })

    describe('user A1 succeeds to score flag B (after 15min)', () => {
        beforeAllReturn(() => global.clientAnonymous.mutationDebugSetTick({ tick: INITIAL_TICK + 300 }))
        beforeAllReturn(() => global.clientAnonymous.mutationDebugSetTick({ tick: INITIAL_TICK + 300 + 300 }))
        beforeAllReturn(() => global.clientAnonymous.mutationDebugSetTick({ tick: INITIAL_TICK + 300 + 300 + 300 }))
        beforeAllReturn(() => userA3Marketplace.current!.mutationFlagScore({ qrCode: FLAG_B }))
        testTeamScore('team A gains +6 MP', userA1, PILLAR.Military, INITIAL_MP + 6)
        test('flag B is lowered as "Stolen"', () => expectGraphqlQuery(userA1.current!.queryTeamFlag({ id: TEAM_B }), { team: expect.objectContaining({ flagStatusId: FLAG_STATUS.Stolen, flagRaisedTime: 1, flagScoredTime: INITIAL_TICK + 300 + 300 + 300 }) } as QueryTeamFlag.Response))
    })

    describe('user A1 succeeds to score flag C', () => {
        beforeAllReturn(() => userA3Marketplace.current!.mutationFlagScore({ qrCode: FLAG_C }))
        testTeamScore('team A gains +6 MP', userA1, PILLAR.Military, INITIAL_MP + 6 + 6)
        test('flag C is lowered as "Stolen"', () => expectGraphqlQuery(userA1.current!.queryTeamFlag({ id: TEAM_C }), { team: expect.objectContaining({ flagStatusId: FLAG_STATUS.Stolen, flagRaisedTime: 1, flagScoredTime: INITIAL_TICK + 300 + 300 + 300 }) } as QueryTeamFlag.Response))
    })

    describe('user A1 succeeds to score their own flag A (after 20min)', () => {
        beforeAll(() => global.clientAnonymous.mutationDebugSetTick({ tick: INITIAL_TICK + 300 + 300 + 600 }))
        beforeAll(() => global.clientAnonymous.mutationDebugSetTick({ tick: INITIAL_TICK + 300 + 300 + 800 }))
        beforeAll(() => global.clientAnonymous.mutationDebugSetTick({ tick: INITIAL_TICK + 300 + 300 + 1000 }))
        beforeAll(() => global.clientAnonymous.mutationDebugSetTick({ tick: INITIAL_TICK + 300 + 300 + 1200 }))
        beforeAll(() => userA1.current!.mutationFlagScore({ qrCode: FLAG_A }))
        testTeamScore('team A gains +5 MP', userA1, PILLAR.Military, INITIAL_MP + 6 + 6 + 5)
        test('flag A is lowered as "Glory"', () => expectGraphqlQuery(userA1.current!.queryTeamFlag({ id: TEAM_A }), { team: expect.objectContaining({ flagStatusId: FLAG_STATUS.Glory, flagRaisedTime: 1, flagScoredTime: INITIAL_TICK + 300 + 300 + 1200 }) } as QueryTeamFlag.Response))
    })

})

describe('WORKSHOPS ACTIVATION', () => {
    const INITIAL_TICK = 2824
    const INITIAL_IP = 0
    const INITIAL_RESOURCE = new Map<RESOURCE_TYPE, number>([
        [RESOURCE_TYPE.Wood            ,  65],
        [RESOURCE_TYPE.Hides           ,  64],
        [RESOURCE_TYPE.Iron            ,  64],
        [RESOURCE_TYPE.Coins           ,  64],
        [RESOURCE_TYPE.Food_Honey      ,  59],
        [RESOURCE_TYPE.Food_Lavender   ,  65],
        [RESOURCE_TYPE.Food_Tomatoes   ,  59],
        [RESOURCE_TYPE.Food_Cucumbers  ,  59],
        [RESOURCE_TYPE.Food_Mushrooms  ,  60],
        [RESOURCE_TYPE.Food_Raspberries,  60],
    ])

    const TASK_TINY = expect.objectContaining({ created: INITIAL_TICK, startTime: INITIAL_TICK + 1, cancelled: null, finishTime: INITIAL_TICK + 25*60 + 1, priceDetails: expect.arrayContaining([
        expect.objectContaining({ type: RESOURCE_TYPE.Coins, amount: 1 }),
        expect.objectContaining({ type: RESOURCE_TYPE.Wood, amount: 1 }),
        expect.objectContaining({ type: RESOURCE_TYPE.Iron, amount: 1 }),
    ]) })
    const TASK_SMALL = expect.objectContaining({ created: INITIAL_TICK + 1 + 375 + 375 , startTime: INITIAL_TICK + 25*60 + 1 + 1, priceDetails: expect.arrayContaining([
        expect.objectContaining({ type: RESOURCE_TYPE.Coins, amount: 2 }),
        expect.objectContaining({ type: RESOURCE_TYPE.Wood, amount: 1 }),
        expect.objectContaining({ type: RESOURCE_TYPE.Iron, amount: 3 }),
    ]) })
    const TASK_MEDIUM = expect.objectContaining({ created: INITIAL_TICK + 1 + 375 + 375, startTime: INITIAL_TICK + 1 + 25*60 + 1 + 25*60 + 1, priceDetails: expect.arrayContaining([
        expect.objectContaining({ type: RESOURCE_TYPE.Coins, amount: 3 }),
        expect.objectContaining({ type: RESOURCE_TYPE.Wood, amount: 1 }),
        expect.objectContaining({ type: RESOURCE_TYPE.Food_Tomatoes, amount: 3 }),
        expect.objectContaining({ type: RESOURCE_TYPE.Food_Cucumbers, amount: 2 }),
        expect.objectContaining({ type: RESOURCE_TYPE.Food_Mushrooms, amount: 4 }),
    ]) })
    const TASK_MEDIUM_CANCELLED = expect.objectContaining({ created: INITIAL_TICK + 1 + 375 + 375, startTime: INITIAL_TICK + 1 + 25*60 + 1 + 25*60 + 1, cancelled: INITIAL_TICK + 1 + 375 + 375, priceDetails: expect.arrayContaining([
        expect.objectContaining({ type: RESOURCE_TYPE.Coins, amount: 3 }),
        expect.objectContaining({ type: RESOURCE_TYPE.Wood, amount: 1 }),
        expect.objectContaining({ type: RESOURCE_TYPE.Food_Tomatoes, amount: 3 }),
        expect.objectContaining({ type: RESOURCE_TYPE.Food_Cucumbers, amount: 2 }),
        expect.objectContaining({ type: RESOURCE_TYPE.Food_Mushrooms, amount: 4 }),
    ]) })
    const TASK_LARGE = expect.objectContaining({ created: INITIAL_TICK + 1 + 375 + 375, startTime: INITIAL_TICK + 1 + 25*60 + 1 + 25*60 + 1, priceDetails: expect.arrayContaining([
        expect.objectContaining({ type: RESOURCE_TYPE.Coins, amount: 4 }),
        expect.objectContaining({ type: RESOURCE_TYPE.Wood, amount: 14 }),
    ]) })
    const TASK_EXCESSIVE = expect.objectContaining({ created: INITIAL_TICK + 1 + 375 + 375, startTime: INITIAL_TICK + 1 + 25*60 + 1 + 25*60 + 1 + 25*60 + 1, priceDetails: expect.arrayContaining([
        expect.objectContaining({ type: RESOURCE_TYPE.Coins, amount: 5 }),
        expect.objectContaining({ type: RESOURCE_TYPE.Wood, amount: 10 }),
        expect.objectContaining({ type: RESOURCE_TYPE.Food_Tomatoes, amount: 4 }),
        expect.objectContaining({ type: RESOURCE_TYPE.Food_Cucumbers, amount: 3 }),
        expect.objectContaining({ type: RESOURCE_TYPE.Food_Mushrooms, amount: 5 }),
    ]) })


    describe('initial state', () => {
        beforeAllReturn(() => global.clientAnonymous.mutationDebugSetTick({ tick: INITIAL_TICK }))
        test(`current tick is ${INITIAL_TICK}`, () => expect(global.clientAnonymous.currentTick()).resolves.toEqual(INITIAL_TICK))
        test(`user A1 has a character with id ${WRISTBAND_A1_2}`, () => expectGraphqlQuery(userA1.current!.queryUserCharacter(), { user: { character: { charcode: WRISTBAND_A1_2 } } } as QueryUserCharacter.Response))
        test('team A has known amount of resources', () => expectGraphqlQuery(userA1.current!.queryTeamResources(), { team: { resources: expect.arrayContaining([
            ...[...INITIAL_RESOURCE.entries()].map(([id, available]) => expect.objectContaining({ id, available, locked: 0 })),
        ]) } } as QueryTeamResources.Response))
        testTeamScore('team A has initial IP', userA1, PILLAR.Industry, INITIAL_IP)
    })

    describe('team A activates tiny Iron workshop', () => {
        beforeAllReturn(() => userA1.current!.mutationTaskQueue({ input: { sizeId: 4, wood: 1, variant: RESOURCE_TYPE.Iron } }))
        test('team A has less 1 Wood, 1 Iron, 1 Coin', () => expectGraphqlQuery(userA1.current!.queryTeamResources(), { team: { resources: expect.arrayContaining([
            expect.objectContaining({ id: RESOURCE_TYPE.Wood, available: INITIAL_RESOURCE.get(RESOURCE_TYPE.Wood)! - 1 }),
            expect.objectContaining({ id: RESOURCE_TYPE.Iron, available: INITIAL_RESOURCE.get(RESOURCE_TYPE.Iron)! - 1 }),
            expect.objectContaining({ id: RESOURCE_TYPE.Coins, available: INITIAL_RESOURCE.get(RESOURCE_TYPE.Coins)! - 1 }),
        ]) } } as QueryTeamResources.Response))
        test('task has been queued and actived', () => expectGraphqlQuery(userA1.current!.queryTeamWorkshops(), { team: { workshopActivations: expect.arrayContaining([
            TASK_TINY,
        ]) } } as QueryTeamWorkshop.Response))
        testTeamScore('team A gains 0 IP so far', userA1, PILLAR.Industry, INITIAL_IP)
    })

    describe('team A receives 1st of 4 IP for the tiny Iron workshop over 25 min (6:15 since activation)', () => {
        beforeAllReturn(() => global.clientAnonymous.mutationDebugSetTick({ tick: INITIAL_TICK + 1 + 375 }))
        testTeamScore('team A gains +1 IP', userA1, PILLAR.Industry, INITIAL_IP + 1)
    })

    describe('team A receives 2nd of 4 IP for the tiny Iron workshop over 25 min (12:30 since activation)', () => {
        beforeAllReturn(() => global.clientAnonymous.mutationDebugSetTick({ tick: INITIAL_TICK + 1 + 375 + 375 }))
        testTeamScore('team A gains +1 IP', userA1, PILLAR.Industry, INITIAL_IP + 2)
    })

    describe('team A queues small Iron workshop', () => {
        beforeAllReturn(() => userA1.current!.mutationTaskQueue({ input: { sizeId: 5, wood: 1, variant: RESOURCE_TYPE.Iron } }))
        test('team A has less 1 Wood, 3 Iron, 2 Coins', () => expectGraphqlQuery(userA1.current!.queryTeamResources(), { team: { resources: expect.arrayContaining([
            expect.objectContaining({ id: RESOURCE_TYPE.Wood, available: INITIAL_RESOURCE.get(RESOURCE_TYPE.Wood)! - 1 - 1 }),
            expect.objectContaining({ id: RESOURCE_TYPE.Iron, available: INITIAL_RESOURCE.get(RESOURCE_TYPE.Iron)! - 1 - 3 }),
            expect.objectContaining({ id: RESOURCE_TYPE.Coins, available: INITIAL_RESOURCE.get(RESOURCE_TYPE.Coins)! - 1 - 2 }),
        ]) } } as QueryTeamResources.Response))
        test('task has been queued', () => expectGraphqlQuery(userA1.current!.queryTeamWorkshops(), { team: { workshopActivations: expect.arrayContaining([
            TASK_TINY,
            TASK_SMALL,
        ]) } } as QueryTeamWorkshop.Response))
    })

    describe('team A queues medium Food workshop', () => {
        beforeAllReturn(() => userA1.current!.mutationTaskQueue({ input: { sizeId: 6, wood: 1, variant: RESOURCE_TYPE.Food, food: [
            { variant: RESOURCE_TYPE.Food_Tomatoes, amount: 3 },
            { variant: RESOURCE_TYPE.Food_Cucumbers, amount: 2 },
            { variant: RESOURCE_TYPE.Food_Mushrooms, amount: 4 },
        ] } }))
        test('team A has less 1 Wood, 7 Iron, 3 Coins', () => expectGraphqlQuery(userA1.current!.queryTeamResources(), { team: { resources: expect.arrayContaining([
            expect.objectContaining({ id: RESOURCE_TYPE.Wood, available: INITIAL_RESOURCE.get(RESOURCE_TYPE.Wood)! - 1 - 1 - 1 }),
            expect.objectContaining({ id: RESOURCE_TYPE.Iron, available: INITIAL_RESOURCE.get(RESOURCE_TYPE.Iron)! - 1 - 3 }),
            expect.objectContaining({ id: RESOURCE_TYPE.Coins, available: INITIAL_RESOURCE.get(RESOURCE_TYPE.Coins)! - 1 - 2 - 3 }),
            expect.objectContaining({ id: RESOURCE_TYPE.Food_Tomatoes, available: INITIAL_RESOURCE.get(RESOURCE_TYPE.Food_Tomatoes)! - 3 }),
            expect.objectContaining({ id: RESOURCE_TYPE.Food_Cucumbers, available: INITIAL_RESOURCE.get(RESOURCE_TYPE.Food_Cucumbers)! - 2 }),
            expect.objectContaining({ id: RESOURCE_TYPE.Food_Mushrooms, available: INITIAL_RESOURCE.get(RESOURCE_TYPE.Food_Mushrooms)! - 4 }),
        ]) } } as QueryTeamResources.Response))
        test('task has been queued', () => expectGraphqlQuery(userA1.current!.queryTeamWorkshops(), { team: { workshopActivations: expect.arrayContaining([
            TASK_TINY,
            TASK_SMALL,
            TASK_MEDIUM,
        ]) } } as QueryTeamWorkshop.Response))
    })

    describe('team A cancels last task of workshop', () => {
        beforeAllReturn(() => userA1.current!.mutationTaskCancel())
        test('team A gets back 1 Wood, 7 Iron, 3 Coins', () => expectGraphqlQuery(userA1.current!.queryTeamResources(), { team: { resources: expect.arrayContaining([
            expect.objectContaining({ id: RESOURCE_TYPE.Wood, available: INITIAL_RESOURCE.get(RESOURCE_TYPE.Wood)! - 1 - 1 }),
            expect.objectContaining({ id: RESOURCE_TYPE.Iron, available: INITIAL_RESOURCE.get(RESOURCE_TYPE.Iron)! - 1 - 3 }),
            expect.objectContaining({ id: RESOURCE_TYPE.Coins, available: INITIAL_RESOURCE.get(RESOURCE_TYPE.Coins)! - 1 - 2 }),
        ]) } } as QueryTeamResources.Response))
        test('task has been cancelled', () => expectGraphqlQuery(userA1.current!.queryTeamWorkshops(), { team: { workshopActivations: expect.arrayContaining([
            TASK_TINY,
            TASK_SMALL,
            TASK_MEDIUM_CANCELLED,
        ]) } } as QueryTeamWorkshop.Response))
    })

    describe('team A queues large Iron workshop with all wood', () => {
        beforeAllReturn(() => userA1.current!.mutationTaskQueue({ input: { sizeId: 7, wood: 14, variant: RESOURCE_TYPE.Iron } }))
        test('team A has less 14 Wood, 0 Iron, 4 Coins', () => expectGraphqlQuery(userA1.current!.queryTeamResources(), { team: { resources: expect.arrayContaining([
            expect.objectContaining({ id: RESOURCE_TYPE.Wood, available: INITIAL_RESOURCE.get(RESOURCE_TYPE.Wood)! - 1 - 1 - 14 }),
            expect.objectContaining({ id: RESOURCE_TYPE.Iron, available: INITIAL_RESOURCE.get(RESOURCE_TYPE.Iron)! - 1 - 3 - 0 }),
            expect.objectContaining({ id: RESOURCE_TYPE.Coins, available: INITIAL_RESOURCE.get(RESOURCE_TYPE.Coins)! - 1 - 2 - 4 }),
        ]) } } as QueryTeamResources.Response))
        test('task has been queued', () => expectGraphqlQuery(userA1.current!.queryTeamWorkshops(), { team: { workshopActivations: expect.arrayContaining([
            TASK_TINY,
            TASK_SMALL,
            TASK_MEDIUM_CANCELLED,
            TASK_LARGE,
        ]) } } as QueryTeamWorkshop.Response))
    })

    describe('team A queues excessive Food workshop with 10 wood', () => {
        beforeAllReturn(() => userA1.current!.mutationTaskQueue({ input: { sizeId: 8, wood: 10, variant: RESOURCE_TYPE.Food, food: [
            { variant: RESOURCE_TYPE.Food_Tomatoes, amount: 4 },
            { variant: RESOURCE_TYPE.Food_Cucumbers, amount: 3 },
            { variant: RESOURCE_TYPE.Food_Mushrooms, amount: 5 },
        ] } }))
        test('team A has less 10 Wood, 3 Cucumbers, 4 Tomatoes, 5 Mushrooms, 5 Coins', () => expectGraphqlQuery(userA1.current!.queryTeamResources(), { team: { resources: expect.arrayContaining([
            expect.objectContaining({ id: RESOURCE_TYPE.Wood, available: INITIAL_RESOURCE.get(RESOURCE_TYPE.Wood)! - 1 - 1 - 14 - 10 }),
            expect.objectContaining({ id: RESOURCE_TYPE.Iron, available: INITIAL_RESOURCE.get(RESOURCE_TYPE.Iron)! - 1 - 3 - 0 }),
            expect.objectContaining({ id: RESOURCE_TYPE.Coins, available: INITIAL_RESOURCE.get(RESOURCE_TYPE.Coins)! - 1 - 2 - 4 - 5 }),
            expect.objectContaining({ id: RESOURCE_TYPE.Food_Tomatoes, available: INITIAL_RESOURCE.get(RESOURCE_TYPE.Food_Tomatoes)! - 4 }),
            expect.objectContaining({ id: RESOURCE_TYPE.Food_Cucumbers, available: INITIAL_RESOURCE.get(RESOURCE_TYPE.Food_Cucumbers)! - 3 }),
            expect.objectContaining({ id: RESOURCE_TYPE.Food_Mushrooms, available: INITIAL_RESOURCE.get(RESOURCE_TYPE.Food_Mushrooms)! - 5 }),
        ]) } } as QueryTeamResources.Response))
        test('task has been queued', () => expectGraphqlQuery(userA1.current!.queryTeamWorkshops(), { team: { workshopActivations: expect.arrayContaining([
            TASK_TINY,
            TASK_SMALL,
            TASK_MEDIUM_CANCELLED,
            TASK_LARGE,
            TASK_EXCESSIVE,
        ]) } } as QueryTeamWorkshop.Response))
    })

    describe('team A receives 3rd of 4 IP for the tiny Iron workshop over 25 min (18:45 since activation)', () => {
        beforeAllReturn(() => global.clientAnonymous.mutationDebugSetTick({ tick: INITIAL_TICK + 1 + 375 + 375 + 375 }))
        testTeamScore('team A gains +1 IP', userA1, PILLAR.Industry, INITIAL_IP + 3)
    })

    describe('team A receives 4th of 4 IP for the tiny Iron workshop over 25 min (25:00 since activation)', () => {
        beforeAllReturn(() => global.clientAnonymous.mutationDebugSetTick({ tick: INITIAL_TICK + 1 + 375 + 375 + 375 + 375 }))
        testTeamScore('team A gains +1 IP', userA1, PILLAR.Industry, INITIAL_IP + 4)
    })

    describe('team A receives 1st of 5 IP for the small Iron workshop over 25 min (5:00 since activation)', () => {
        beforeAllReturn(() => global.clientAnonymous.mutationDebugSetTick({ tick: INITIAL_TICK + 1 + 375 + 375 + 375 + 375 + 1 + 300 }))
        testTeamScore('team A gains +1 IP', userA1, PILLAR.Industry, INITIAL_IP + 5)
    })

    describe('team A receives 2nd of 5 IP for the small Iron workshop over 25 min (10:00 since activation)', () => {
        beforeAllReturn(() => global.clientAnonymous.mutationDebugSetTick({ tick: INITIAL_TICK + 1 + 375 + 375 + 375 + 375 + 1 + 300 + 300 }))
        testTeamScore('team A gains +1 IP', userA1, PILLAR.Industry, INITIAL_IP + 6)
    })

})

describe('CAMP UPGRADES', () => {
    const INITIAL_TICK = 5826
    const INITIAL_IP_A = 9
    const INITIAL_IP_B = 0
    const INITIAL_IP_C = 0

    describe('initial state', () => {
        beforeAllReturn(() => global.clientAnonymous.mutationDebugSetTick({ tick: INITIAL_TICK }))
        test(`current tick is ${INITIAL_TICK}`, () => expect(global.clientAnonymous.currentTick()).resolves.toEqual(INITIAL_TICK))
        test(`user A1 has a character with id ${WRISTBAND_A1_2}`, () => expectGraphqlQuery(userA1.current!.queryUserCharacter(), { user: { character: { charcode: WRISTBAND_A1_2 } } } as QueryUserCharacter.Response))
        test(`user B1 has a character with id ${WRISTBAND_B1_1}`, () => expectGraphqlQuery(userB1.current!.queryUserCharacter(), { user: { character: { charcode: WRISTBAND_B1_1 } } } as QueryUserCharacter.Response))
        test(`user C1 has a character with id ${WRISTBAND_C1_1}`, () => expectGraphqlQuery(userC1.current!.queryUserCharacter(), { user: { character: { charcode: WRISTBAND_C1_1 } } } as QueryUserCharacter.Response))
        testTeamScore('team A has initial IP', userA1, PILLAR.Industry, INITIAL_IP_A, 1)
        testTeamScore('team B has initial IP', userB1, PILLAR.Industry, INITIAL_IP_B, 2)
        testTeamScore('team C has initial IP', userC1, PILLAR.Industry, INITIAL_IP_C, 3)
        test('team A has one upgrade "MASHES" for camp 1', () => expectGraphqlQuery(userA1.current!.queryTeamUpgrades(), { team: { expansions: expect.arrayContaining([ expect.objectContaining({ code: 'MASHES', camp: expect.objectContaining({ id: 1 }), rewarded: null, showedAt: 2, owners: expect.objectContaining({ id: TEAM_A }) }) ]) } } as QueryTeamUpgrades.Response))
        test('team B has one upgrade "HOOHAS" for camp 2', () => expectGraphqlQuery(userB1.current!.queryTeamUpgrades(), { team: { expansions: expect.arrayContaining([ expect.objectContaining({ code: 'HOOHAS', camp: expect.objectContaining({ id: 2 }), rewarded: null, showedAt: 2, owners: expect.objectContaining({ id: TEAM_B }) }) ]) } } as QueryTeamUpgrades.Response))
        test('team C has one upgrade "STEMMA" for camp 3', () => expectGraphqlQuery(userC1.current!.queryTeamUpgrades(), { team: { expansions: expect.arrayContaining([ expect.objectContaining({ code: 'STEMMA', camp: expect.objectContaining({ id: 3 }), rewarded: null, showedAt: 2, owners: expect.objectContaining({ id: TEAM_C }) }) ]) } } as QueryTeamUpgrades.Response))
    })

    describe('team A provides correct reward "PERCENT" for its own upgrade "MASHES"', () => {
        beforeAllReturn(() => userA1.current!.mutationUpgradeReward({ code: 'PERCENT' }))
        testTeamScore('team A gains +4 IP', userA1, PILLAR.Industry, INITIAL_IP_A + 4)
        test('team A has one upgrade "MASHES" for camp 1', () => expectGraphqlQuery(userA1.current!.queryTeamUpgrades(), { team: { expansions: expect.arrayContaining([ expect.objectContaining({ code: 'MASHES', camp: expect.objectContaining({ id: 1 }), rewardedAt: INITIAL_TICK, showedAt: 2, owners: expect.objectContaining({ id: TEAM_A }), rewarded: expect.objectContaining({ id: TEAM_A }) }) ]) } } as QueryTeamUpgrades.Response))
    })

    describe('team B provides correct reward "SURFACE" for Team C upgrade "STEMMA"', () => {
        beforeAllReturn(() => userB1.current!.mutationUpgradeReward({ code: 'SURFACE' }))
        testTeamScore('team B gains +2 IP', userB1, PILLAR.Industry, INITIAL_IP_B + 2, 2)
        testTeamScore('team C gains +2 IP', userC1, PILLAR.Industry, INITIAL_IP_C + 2, 3)
        test('team B sees the upgrade', () => expectGraphqlQuery(userB1.current!.queryTeamUpgrades(), { team: { expansions: expect.arrayContaining([ expect.objectContaining({ code: 'STEMMA', camp: expect.objectContaining({ id: 3 }), rewardedAt: INITIAL_TICK, showedAt: 2, owners: expect.objectContaining({ id: TEAM_C }), rewarded: expect.objectContaining({ id: TEAM_B }) }) ]) } } as QueryTeamUpgrades.Response))
        test('team C sees the upgrade', () => expectGraphqlQuery(userC1.current!.queryTeamUpgrades(), { team: { expansions: expect.arrayContaining([ expect.objectContaining({ code: 'STEMMA', camp: expect.objectContaining({ id: 3 }), rewardedAt: INITIAL_TICK, showedAt: 2, owners: expect.objectContaining({ id: TEAM_C }), rewarded: expect.objectContaining({ id: TEAM_B }) }) ]) } } as QueryTeamUpgrades.Response))
    })


})

describe('SCORING 1ST SEASON', () => {
    const INITIAL_TICK = 5826
    const FORCED_NEXT_SEASON_TICK = 6001
    const INITIAL_SCORES_A = new Map<PILLAR, [number, number]>([
        [PILLAR.Military   , [17, 1]],
        [PILLAR.Agriculture, [9 , 1]],
        [PILLAR.Industry   , [13, 1]],
    ])
    const INITIAL_SCORES_B = new Map<PILLAR, [number, number]>([
        [PILLAR.Military   , [2 , 2]],
        [PILLAR.Agriculture, [8 , 2]],
        [PILLAR.Industry   , [2 , 2]],
    ])
    const INITIAL_SCORES_C = new Map<PILLAR, [number, number]>([
        [PILLAR.Military   , [0 , 3]],
        [PILLAR.Agriculture, [2 , 3]],
        [PILLAR.Industry   , [2 , 3]],
    ])

    const SCORES_A = new Map<PILLAR, [number, number]>([
        [PILLAR.Military   , [11, 1]],
        [PILLAR.Agriculture, [1 , 3]],
        [PILLAR.Industry   , [7 , 1]],
    ])
    const SCORES_B = new Map<PILLAR, [number, number]>([
        [PILLAR.Military   , [2 , 2]],
        [PILLAR.Agriculture, [2 , 1]],
        [PILLAR.Industry   , [0 , 3]],
    ])
    const SCORES_C = new Map<PILLAR, [number, number]>([
        [PILLAR.Military   , [0 , 3]],
        [PILLAR.Agriculture, [2 , 2]],
        [PILLAR.Industry   , [2 , 2]],
    ])

    describe('initial state', () => {
        beforeAllReturn(() => global.clientAnonymous.mutationDebugSetTick({ tick: INITIAL_TICK }))
        test(`current tick is ${INITIAL_TICK}`, () => expect(global.clientAnonymous.currentTick()).resolves.toEqual(INITIAL_TICK))
        test(`current season is ${1}`, () => expectGraphqlQuery(userA1.current!.querySeason({}), { season: { id: 1 } } as QuerySeason.Response))
        test(`user A1 has a character with id ${WRISTBAND_A1_2}`, () => expectGraphqlQuery(userA1.current!.queryUserCharacter(), { user: { character: { charcode: WRISTBAND_A1_2 } } } as QueryUserCharacter.Response))
        testTeamScores('team A has initial scores', userA1, INITIAL_SCORES_A)
        testTeamScores('team B has initial scores', userB1, INITIAL_SCORES_B)
        testTeamScores('team C has initial scores', userC1, INITIAL_SCORES_C)
        test('teams have no victory points', () => expectGraphqlQuery(userA1.current!.queryScoreVictorySummary(), { victoryPoints: { totals: expect.arrayContaining([
            expect.objectContaining({ team: expect.objectContaining({ id: TEAM_A }), points: 0 }),
            expect.objectContaining({ team: expect.objectContaining({ id: TEAM_B }), points: 0 }),
            expect.objectContaining({ team: expect.objectContaining({ id: TEAM_C }), points: 0 }),
        ]) } } as QueryScoreVictorySummary.Response))
    })

    describe('after season change', () => {
        beforeAllReturn(() => global.clientAnonymous.mutationDebugSetTick({ tick: FORCED_NEXT_SEASON_TICK + 1 }))
        test(`current tick is ${FORCED_NEXT_SEASON_TICK + 1}`, () => expect(global.clientAnonymous.currentTick()).resolves.toEqual(FORCED_NEXT_SEASON_TICK + 1))
        test(`current season is ${2}`, () => expectGraphqlQuery(userA1.current!.querySeason({}), { season: { id: 2, started: FORCED_NEXT_SEASON_TICK } } as QuerySeason.Response))
        test(`user A1 has a character with id ${WRISTBAND_A1_2}`, () => expectGraphqlQuery(userA1.current!.queryUserCharacter(), { user: { character: { charcode: WRISTBAND_A1_2 } } } as QueryUserCharacter.Response))
        test('team A has 2 new random upgrades', () => expectGraphqlQuery(userA1.current!.queryTeamUpgrades(), { team: { expansions: expect.arrayContaining([
            expect.objectContaining({ rewarded: null, showedAt: FORCED_NEXT_SEASON_TICK, owners: expect.objectContaining({ id: TEAM_A }) }),
            expect.objectContaining({ rewarded: null, showedAt: FORCED_NEXT_SEASON_TICK, owners: expect.objectContaining({ id: TEAM_A }) }),
        ]) } } as QueryTeamUpgrades.Response))
        test('team B has 2 new random upgrades', () => expectGraphqlQuery(userB1.current!.queryTeamUpgrades(), { team: { expansions: expect.arrayContaining([
            expect.objectContaining({ rewarded: null, showedAt: FORCED_NEXT_SEASON_TICK, owners: expect.objectContaining({ id: TEAM_B }) }),
            expect.objectContaining({ rewarded: null, showedAt: FORCED_NEXT_SEASON_TICK, owners: expect.objectContaining({ id: TEAM_B }) }),
        ]) } } as QueryTeamUpgrades.Response))
        test('team C has 2 new random upgrades', () => expectGraphqlQuery(userC1.current!.queryTeamUpgrades(), { team: { expansions: expect.arrayContaining([
            expect.objectContaining({ rewarded: null, showedAt: FORCED_NEXT_SEASON_TICK, owners: expect.objectContaining({ id: TEAM_C }) }),
            expect.objectContaining({ rewarded: null, showedAt: FORCED_NEXT_SEASON_TICK, owners: expect.objectContaining({ id: TEAM_C }) }),
        ]) } } as QueryTeamUpgrades.Response))
        testTeamScores('team A has initial scores', userA1, SCORES_A)
        testTeamScores('team B has initial scores', userB1, SCORES_B)
        testTeamScores('team C has initial scores', userC1, SCORES_C)
        test('teams have total victory points (A: 30 VP, B: 25 VP, C: 21 VP)', () => expectGraphqlQuery(userA1.current!.queryScoreVictorySummary(), { victoryPoints: { totals: expect.arrayContaining([
            expect.objectContaining({ team: expect.objectContaining({ id: TEAM_A }), points: 30 }),
            expect.objectContaining({ team: expect.objectContaining({ id: TEAM_B }), points: 25 }),
            expect.objectContaining({ team: expect.objectContaining({ id: TEAM_C }), points: 21 }),
        ]) } } as QueryScoreVictorySummary.Response))
    })

})

describe('GAME IS DISABLED', () => {
    describe ('fail all mutations after disable "switch-game_started"', () => {
        beforeAllReturn(() => global.clientAnonymous.admin!.gameEnabled(false))
        testGraphqlError('Game not started!', () => userA1.current!.mutationCharacterRevitalize())
        testGraphqlError('Game not started!', () => userA1.current!.mutationEquipmentAdd())
        testGraphqlError('Game not started!', () => userA1.current!.mutationEquipmentBuy())
        testGraphqlError('Game not started!', () => userA1.current!.mutationEquipmentRemove())
        testGraphqlError('Game not started!', () => userA1.current!.mutationCharacterRespawn({ code: 66557 }))
        testGraphqlError('Game not started!', () => userA1.current!.mutationCharacterTribute({ code: WRISTBAND_B1_1 }))
        // testGraphqlError('Game not started!', () => userA1.current!.mutationFeastScore({} as any))
        // testGraphqlError('Game not started!', () => userA1.current!.mutationFlagScore({} as any))
        // testGraphqlError('Game not started!', () => userA1.current!.mutationKitAdd({} as any))
        // testGraphqlError('Game not started!', () => userA1.current!.mutationKitRemove({} as any))
        // testGraphqlError('Game not started!', () => userA1.current!.mutationRecipeScore({} as any))
        // testGraphqlError('Game not started!', () => userA1.current!.mutationResourcesAddCoins({} as any))
        // testGraphqlError('Game not started!', () => userA1.current!.mutationResourcesAdd({} as any))
        // testGraphqlError('Game not started!', () => userA1.current!.mutationResourcesRemoveCoins({} as any))
        // testGraphqlError('Game not started!', () => userA1.current!.mutationResourcesSell({} as any))
        // testGraphqlError('Game not started!', () => userA1.current!.mutationTaskCancel({} as any))
        // testGraphqlError('Game not started!', () => userA1.current!.mutationTaskQueue({} as any))
        // testGraphqlError('Game not started!', () => userA1.current!.mutationUpgradeReward({} as any))
        // testGraphqlError('Game not started!', () => userA1.current!.mutationWorkshopCreate({} as any))
        // testGraphqlError('Game not started!', () => userA1.current!.mutationWorkshopDestroy({} as any))
    })
    describe ('succeed revitalize mutation after enable "switch-game_started"', () => {
        beforeAllReturn(() => global.clientAnonymous.admin!.gameEnabled(true))
        beforeAllReturn(() => userA1.current!.mutationCharacterRevitalize())
        test('user A2 has respawned', () => expectGraphqlQuery(userA1.current!.queryUserCharacter(), { user: { respawnCount: expect.any(Number) } } as QueryUserCharacter.Response))
    })
})
