import { ApolloClient, HttpLink, InMemoryCache, NormalizedCacheObject } from '@apollo/client'
import { MutationDebugSetTick, MutationTeamLogin, QueryRecipes, QueryScorePillar, QueryScorePillarSummary, QueryScorePillars, QueryScorePillarsBase, QueryScoreVictorySummary, QuerySeason, QuerySession, QuerySettings, QueryTeams, QueryTime, QueryWorkshops, TEAM_ID } from '@larts/api'
import fetch from 'cross-fetch'

import Admin from './Admin'
import ClientTeam from './ClientTeam'


export default class ClientAnonymous {
    readonly uri: string
    /* readonly */ client: ApolloClient<NormalizedCacheObject>
    readonly teams: ClientTeam[] = []
    admin: Admin | null = null

    constructor(uri: string) {
        this.uri = uri
        this.client = new ApolloClient({
            link: new HttpLink({ uri, fetch }),
            cache: new InMemoryCache(),
        })
    }

    teardown() {
        for (const device of this.teams) device.teardown()
        this.client.stop()
    }

    async loginTeam(teamId: TEAM_ID | null, name = 'testsuite name') {
        const { data, errors } = await this.client.mutate({ ...MutationTeamLogin, variables: { team: teamId, name } })
        if (!data) throw new Error(errors?.[0].message)
        const deviceToken = data.auth.deviceRegister
        const team = new ClientTeam(this.uri, deviceToken)

        if (this.admin) {
            await this.admin.authDevice(deviceToken)
            team.admin = this.admin
        }

        this.teams.push(team)
        return team
    }

    queryRecipes             (...args: QueryRecipes.Params             extends never ? [] : [variables: QueryRecipes.Params             ]) { return this.client.query({ ...QueryRecipes            , variables: (args as any)[0], fetchPolicy: 'no-cache' }) }
    queryScorePillarsBase    (...args: QueryScorePillarsBase.Params    extends never ? [] : [variables: QueryScorePillarsBase.Params    ]) { return this.client.query({ ...QueryScorePillarsBase   , variables: (args as any)[0], fetchPolicy: 'no-cache' }) }
    queryScorePillars        (...args: QueryScorePillars.Params        extends never ? [] : [variables: QueryScorePillars.Params        ]) { return this.client.query({ ...QueryScorePillars       , variables: (args as any)[0], fetchPolicy: 'no-cache' }) }
    queryScorePillarSummary  (...args: QueryScorePillarSummary.Params  extends never ? [] : [variables: QueryScorePillarSummary.Params  ]) { return this.client.query({ ...QueryScorePillarSummary , variables: (args as any)[0], fetchPolicy: 'no-cache' }) }
    queryScorePillar         (...args: QueryScorePillar.Params         extends never ? [] : [variables: QueryScorePillar.Params         ]) { return this.client.query({ ...QueryScorePillar        , variables: (args as any)[0], fetchPolicy: 'no-cache' }) }
    queryScoreVictorySummary (...args: QueryScoreVictorySummary.Params extends never ? [] : [variables: QueryScoreVictorySummary.Params ]) { return this.client.query({ ...QueryScoreVictorySummary, variables: (args as any)[0], fetchPolicy: 'no-cache' }) }
    querySeason              (...args: QuerySeason.Params              extends never ? [] : [variables: QuerySeason.Params              ]) { return this.client.query({ ...QuerySeason             , variables: (args as any)[0], fetchPolicy: 'no-cache' }) }
    querySession             (...args: QuerySession.Params             extends never ? [] : [variables: QuerySession.Params             ]) { return this.client.query({ ...QuerySession            , variables: (args as any)[0], fetchPolicy: 'no-cache' }) }
    querySettings            (...args: QuerySettings.Params            extends never ? [] : [variables: QuerySettings.Params            ]) { return this.client.query({ ...QuerySettings           , variables: (args as any)[0], fetchPolicy: 'no-cache' }) }
    queryTeams               (...args: QueryTeams.Params               extends never ? [] : [variables: QueryTeams.Params               ]) { return this.client.query({ ...QueryTeams              , variables: (args as any)[0], fetchPolicy: 'no-cache' }) }
    queryTime                (...args: QueryTime.Params                extends never ? [] : [variables: QueryTime.Params                ]) { return this.client.query({ ...QueryTime               , variables: (args as any)[0], fetchPolicy: 'no-cache' }) }
    queryWorkshops           (...args: QueryWorkshops.Params           extends never ? [] : [variables: QueryWorkshops.Params           ]) { return this.client.query({ ...QueryWorkshops          , variables: (args as any)[0], fetchPolicy: 'no-cache' }) }

    async currentTick() {
        const settings = await this.querySettings()
        const currentTickSetting = settings.data.baseData.settings.find((setting) => setting.code === 'current_tick')
        return Number(currentTickSetting?.value)
    }

    async mutationDebugSetTick(variables: MutationDebugSetTick.Params) {
        await this.client.mutate({ ...MutationDebugSetTick           , variables })
        const before = await this.currentTick()
        const logs = await this.admin?.tick()
        const after = await this.currentTick()
        if (after !== variables.tick) {
            const error = new Error(`Failed to set tick to ${variables.tick}, got to ${after} from ${before}.`)
            error.stack = error.message + '\n' + (await logs?.text())
            throw error
        }
    }
}
