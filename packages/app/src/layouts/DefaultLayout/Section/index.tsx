import React, { ComponentProps } from 'react'

import { Box, Divider, Typography } from '@material-ui/core'

import { IMyTheme, createFC, createStyles } from 'src/common'


const styles = (theme: IMyTheme) => createStyles({
    root: {
        padding: theme.spacing(2, 0, 4, 0),
    },
})


interface IProps extends ComponentProps<'section'> {
    heading: string
    overline?: string
}


export default createFC(import.meta.url, styles)<IProps>(function _({ children, classes, theme, ...props }) {
    const { heading, overline, ...sectionProps } = props

    const id = (overline ?? heading).replaceAll(' ', '-')

    return (
        <section
            id={id}
            className={classes.root}
            {...sectionProps}
        >
            {overline && (
                <>
                    <Typography variant='subtitle1'>{overline}</Typography>
                    <Divider sx={{ margin: theme.spacing(1, 0), borderColor: theme.palette.divider }} />
                </>
            )}
            <Typography variant='h2' sx={{ marginBottom: theme.spacing(4) }}>
                <a href={`#${id}`}>
                    <Box
                        component='span'
                        sx={{
                            display: 'inline-block',
                            height:16,
                            width: 16,
                            backgroundColor: theme.palette.secondary.main,
                            borderRadius: 8,
                            verticalAlign: 'middle',
                            '&:hover': {
                                backgroundColor: theme.palette.secondary.light,
                            },
                        }}
                    />
                </a>
                &nbsp;
                {heading}
            </Typography>
            {children}
        </section>
    )
}) /* =====+======================================================================================================== */
