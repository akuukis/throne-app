import React from 'react'

import { QuerySession, QueryTeamBase, QueryUserName } from '@larts/api'
import { BrowserRouter, Navigate, Route, Routes } from 'react-router-dom'

import { ROUTE } from 'src/constants'

import { createFC, useQuery } from './common'
import App from './containers/App'
import Contacts from './containers/Contacts'
import Equipment from './containers/Equipment'
import Feast from './containers/Feast'
import Feasts from './containers/Feasts'
import Flag from './containers/Flag'
import FridayFeast from './containers/FridayFeast'
import Home from './containers/Home'
import Leaderboard from './containers/Leaderboard'
import Market from './containers/Market'
import Profile from './containers/Profile'
import Public from './containers/Public'
import Recipes from './containers/Recipes'
import Resources from './containers/Resources'
import Respawn from './containers/Respawn'
import Setup from './containers/Setup'
import UnderConstruction from './containers/UnderConstruction'
import Upgrades from './containers/Upgrades'
import Workshops from './containers/Workshops'


interface IProps {
}


export default createFC(import.meta.url)<IProps>(function _({ children, theme }) {
    const queryTeamBase = useQuery(QueryTeamBase)
    const queryUser = useQuery(QueryUserName)
    const querySession = useQuery(QuerySession)

    const isLoading = queryTeamBase.loading || queryUser.loading
    const isMerchant = querySession.data?.baseData.isMarketDevice
    const isTeam = !!queryTeamBase.data?.team
    const isUser = !!queryUser.data?.user

    return (
        <BrowserRouter>
            <Routes>
                <Route path="*" element={<App />}>
                    {/* public mode */}
                    {(isLoading || !isTeam) && (
                        <>
                            <Route path={`${ROUTE.Public}/*`} element={<Public />} />
                            <Route path={`${ROUTE.Recipes}/*`} element={<Recipes />} />
                            <Route path={ROUTE.Setup} element={<Setup />} />
                        </>
                    )}
                    {(isLoading || isTeam) && (
                        <>
                            <Route path={`${ROUTE.Home}/*`} element={<Home />} />
                            <Route path={`${ROUTE.Resources}/*`} element={<Resources />} />

                            <Route path={`${ROUTE.Leaderboard}/*`} element={<Leaderboard />} />

                            {/* Industry */}
                            <Route path={`${ROUTE.Workshops}/*`} element={<Workshops />} />
                            <Route path={`${ROUTE.Upgrades}/*`} element={<Upgrades />} />

                            {/* Agriculture */}
                            <Route path={`${ROUTE.Recipes}/*`} element={<Recipes />} />
                            <Route path={`${ROUTE.Feasts}/*`} element={<Feasts />} />
                            <Route path={`${ROUTE.Feast}/*`} element={<Feast />} />

                            {/* Military */}
                            <Route path={`${ROUTE.Flag}/*`} element={<Flag />} />
                            <Route path={`${ROUTE.EquipmentPoints}/*`} element={<Equipment />} />

                            {/* External */}
                            <Route path={`${ROUTE.Market}/*`} element={<Market />} />
                        </>
                    )}

                    {/* Merchant specific */}
                    {(isLoading || isMerchant) && (
                        <Route path={`${ROUTE.Flag}/*`} element={<Flag />} />
                    )}

                    {/* User specific */}
                    {(isLoading || isUser) && (
                        <>
                            <Route path={`${ROUTE.Profile}/*`} element={<Profile />} />
                            <Route path={`${ROUTE.Respawn}/*`} element={<Respawn />} />
                        </>
                    )}


                    {/* hopefully later */}
                    <Route path={ROUTE.Logs} element={<UnderConstruction />} />
                    <Route path={`${ROUTE.Events}/*`} element={<UnderConstruction />} />
                    <Route path={`${ROUTE.Diplomacy}/*`} element={<UnderConstruction />} />
                    <Route path={`${ROUTE.Mercenaries}/*`} element={<UnderConstruction />} />


                    <Route path={`${ROUTE.Contacts}/*`} element={<Contacts />} />
                    <Route path={`${ROUTE.FridayFeast}/*`} element={<FridayFeast />} />

                    <Route path="*" element={<Navigate to={isTeam ? ROUTE.Home : ROUTE.Public} replace />} />
                </Route>
            </Routes>
        </BrowserRouter>
    )
})
