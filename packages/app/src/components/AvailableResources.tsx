import React from 'react'

import { FOOD_SUBTYPE, QueryTeamResources, RESOURCE_TYPE } from '@larts/api'
import { recordKeys } from '@larts/common'
import { Chip, Grid } from '@material-ui/core'

import { createFC, useQuery } from 'src/common'
import PageCard from 'src/components/PageCard'
import Resource from 'src/components/Resource'


interface Props {
    threshold: Partial<Record<RESOURCE_TYPE, number>>
}

export default createFC(import.meta.url)<Props>(function _({ children, theme, threshold }) {
    const queryTeamResources = useQuery(QueryTeamResources)
    const foodTotalAvailable = queryTeamResources.data?.team?.resources.reduce((sum, res) => res.type.id in FOOD_SUBTYPE ? res.available + sum : sum, 0) ?? 0

    return (
        <PageCard variant='standard' color='neutral' title='Available resources'>
            <Grid container spacing={1}>
                {queryTeamResources.data?.team?.resources
                    .filter((res) => recordKeys(threshold).includes(String(res.type.id) as any)) // keys cant be number :(
                    .sort((a, b) => b.available - a.available)
                    .map((res) => (
                        <Grid key={res.type.id} item>
                            <Chip
                                color={res.available >= threshold[String(res.type.id)] ? 'primary' : 'default'}
                                variant={res.available >= threshold[String(res.type.id)] ? 'filled' : 'outlined'}
                                label={<Resource amount={res.available} type={res.type.id} />}
                            />
                        </Grid>
                    ))

                }
                {threshold[RESOURCE_TYPE.Food] && (
                    <Grid key={RESOURCE_TYPE.Food} item>
                        <Chip
                            color={foodTotalAvailable >= threshold[RESOURCE_TYPE.Food] ? 'primary' : 'default'}
                            variant={foodTotalAvailable >= threshold[RESOURCE_TYPE.Food] ? 'filled' : 'outlined'}
                            label={<Resource amount={foodTotalAvailable} type={RESOURCE_TYPE.Food} />}
                        />
                    </Grid>
                )}
            </Grid>
        </PageCard>
    )
}) /* =============================================================================================================== */
