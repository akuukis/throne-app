import React from 'react'

import { CircularProgress, Grid } from '@material-ui/core'

import { createFC } from 'src/common'


export default createFC(import.meta.url)(function _({ children, theme }) {
    return (
        <Grid
            container
            sx={{
                padding: 4,
                position: 'absolute',
                height: '20%',
                width: '20%',
                top: '40%',
                bottom: '40%',
                left: '40%',
                right: '40%',
            }}
            alignItems='center'
            justifyItems='center'
            direction='column'
        >
            <CircularProgress variant='indeterminate' />
        </Grid>
    )
}) /* =============================================================================================================== */
