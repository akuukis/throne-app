import React, { ReactNode } from 'react'

import { Card, CardContent, CardHeader, CardProps, Grid, Typography } from '@material-ui/core'
import { SxProps } from '@material-ui/system'

import { IMyTheme, createFC, createStyles } from 'src/common'
import { borderColor } from 'src/common/colors'


interface Props extends Omit<CardProps, 'variant' | 'title'> {
    title?: ReactNode
    overline?: ReactNode
    variant?: 'standard' | 'outlined' | 'standardOutlined' | 'filled'
    color?: 'primary' | 'neutral'
    action?: ReactNode
    sx?: SxProps
}

const styles = (theme: IMyTheme) => createStyles({
    root: ({ variant = 'outlined', color = 'primary' }: Props) => ({
        '&.MuiPaper-root': {
            position: 'relative',
            backgroundColor: variant === 'outlined' ? theme.palette.background.default
                : (variant === 'standardOutlined' && color === 'primary') ? theme.palette.primary.background  // eslint-disable-line @typescript-eslint/indent
                : (variant === 'standard'         && color === 'primary') ? theme.palette.primary.background  // eslint-disable-line @typescript-eslint/indent
                : (variant === 'filled'           && color === 'primary') ? theme.palette.primary.dark  // eslint-disable-line @typescript-eslint/indent
                : (variant === 'standardOutlined' && color === 'neutral') ? theme.palette.background.paper  // eslint-disable-line @typescript-eslint/indent
                : (variant === 'standard'         && color === 'neutral') ? theme.palette.background.paper  // eslint-disable-line @typescript-eslint/indent
                : (variant === 'filled'           && color === 'neutral') ? theme.palette.text.secondary  // eslint-disable-line @typescript-eslint/indent
                : 'transparent' as never,  // eslint-disable-line @typescript-eslint/indent
            borderStyle: (variant === 'outlined' || variant === 'standardOutlined') ? 'solid' : 'none',
            borderColor: color === 'primary' ? theme.palette.primary.main : borderColor(theme.palette),
            margin: theme.spacing(2, 1),
            color: variant === 'outlined' ? undefined
                : (variant === 'standardOutlined' && color === 'primary') ? undefined  // eslint-disable-line @typescript-eslint/indent
                : (variant === 'standard'         && color === 'primary') ? undefined  // eslint-disable-line @typescript-eslint/indent
                : (variant === 'filled'           && color === 'primary') ? theme.palette.primary.contrastText  // eslint-disable-line @typescript-eslint/indent
                : (variant === 'standardOutlined' && color === 'neutral') ? undefined  // eslint-disable-line @typescript-eslint/indent
                : (variant === 'standard'         && color === 'neutral') ? undefined  // eslint-disable-line @typescript-eslint/indent
                : (variant === 'filled'           && color === 'neutral') ? theme.palette.primary.contrastText  // eslint-disable-line @typescript-eslint/indent
                : 'transparent' as never,  // eslint-disable-line @typescript-eslint/indent
        },
    }),
    overline: {
        color: theme.palette.text.secondary,
    },
    title: ({ variant = 'outlined', color = 'primary' }: Props) => ({
        color: variant === 'outlined' ? undefined
            : (variant === 'standardOutlined' && color === 'primary') ? undefined  // eslint-disable-line @typescript-eslint/indent
            : (variant === 'standard'         && color === 'primary') ? undefined  // eslint-disable-line @typescript-eslint/indent
            : (variant === 'filled'           && color === 'primary') ? theme.palette.primary.contrastText  // eslint-disable-line @typescript-eslint/indent
            : (variant === 'standardOutlined' && color === 'neutral') ? theme.palette.text.secondary  // eslint-disable-line @typescript-eslint/indent
            : (variant === 'standard'         && color === 'neutral') ? theme.palette.text.secondary  // eslint-disable-line @typescript-eslint/indent
            : (variant === 'filled'           && color === 'neutral') ? theme.palette.primary.contrastText  // eslint-disable-line @typescript-eslint/indent
            : 'transparent' as never,  // eslint-disable-line @typescript-eslint/indent
    }),
})


export default createFC(import.meta.url, styles)<Props>(function _({ children, theme, classes, variant, overline, title, action, ...cardProps }) {

    return (
        <Card className={classes.root} {...cardProps}>
            {(title || action) && (
                <CardHeader
                    disableTypography
                    title={(
                        <Grid container wrap='nowrap' justifyContent='space-between'>
                            <Grid item>
                                {overline && (
                                    <Typography className={classes.overline} gutterBottom variant='overline'>
                                        {overline}
                                    </Typography>
                                )}
                                <Typography variant='h3' className={classes.title}>{title}</Typography>
                            </Grid>
                            {action}
                        </Grid>
                    )}
                />
            )}
            {children && (
                <CardContent>
                    {children}
                </CardContent>
            )}
        </Card>
    )
}) /* =============================================================================================================== */
