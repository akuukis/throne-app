import React from 'react'

import { TEAM_COLOR } from '@larts/api'
import { ThemeProvider } from '@material-ui/core'
import { SxProps } from '@material-ui/system'
import { Box } from '@material-ui/system'

import { createFC, getTheme } from 'src/common'
import IconCastle from 'src/components/icons/IconCastle'


interface IProps {
    color?: TEAM_COLOR
    title?: string
    sx?: SxProps
}

export default createFC(import.meta.url)<IProps>(function _({ children, theme, color, title, sx }) {
    const subTheme = getTheme(color)

    return (
        <ThemeProvider theme={subTheme}>
            <Box component='span' sx={sx}>
                <IconCastle sx={{ color: subTheme.palette.primary.main, verticalAlign: 'middle' }} />
                {title && ` ${title}`}
            </Box>
        </ThemeProvider>
    )
}) /* ============================================================================================================== */
