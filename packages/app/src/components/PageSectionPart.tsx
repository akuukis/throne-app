import React, { ReactNode } from 'react'

import { Card, Grid, Typography } from '@material-ui/core'

import { IMyTheme, createFC, createStyles } from 'src/common'


interface Props {
    title?: ReactNode
    action?: ReactNode
    overline?: ReactNode
}

const styles = (theme: IMyTheme) => createStyles({
    root: {
        '&.MuiPaper-root': {
            position: 'relative',
            backgroundColor: 'transparent',
            borderRadius: 0,
            margin: theme.spacing(2, 1),
            '&:first-child': {
                marginTop: 0,
            },
            '&:last-child': {
                marginBottom: 0,
            },
        },
    },
    overline: {
        color: theme.palette.text.secondary,
    },
})


export default createFC(import.meta.url, styles)<Props>(function _({ children, theme, classes, overline, title, action }) {

    return (
        <Card className={classes.root}>
            {(title || overline || action) && (
                <Grid container wrap='nowrap' justifyContent='space-between' sx={{ marginBottom: 1 }} alignItems='center'>
                    <Grid item>
                        {overline && (
                            <Typography className={classes.overline} gutterBottom variant='overline'>
                                {overline}
                            </Typography>
                        )}
                        {title && (
                            <Typography variant='h3'>{title}</Typography>
                        )}
                    </Grid>
                    {action}
                </Grid>
            )}
            {children}
        </Card>
    )
}) /* =============================================================================================================== */
