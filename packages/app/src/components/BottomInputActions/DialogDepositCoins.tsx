import React, { useCallback } from 'react'

import { QueryTeamResources, RESOURCE_TYPE } from '@larts/api'
import { Button, Dialog, Grid, IconButton, Toolbar, Typography } from '@material-ui/core'
import IconAdd from '@material-ui/icons/Add'
import IconClose from '@material-ui/icons/Close'
import IconRemove from '@material-ui/icons/Remove'

import useMutationResourcesAddCoins from 'src/api/useMutationResourcesAddCoins'
import useMutationResourcesRemoveCoins from 'src/api/useMutationResourcesRemoveCoins'
import { createFC, useQuery } from 'src/common'
import PageCard from 'src/components/PageCard'
import PageSection from 'src/components/PageSection'
import PageSectionPart from 'src/components/PageSectionPart'
import Resource from 'src/components/Resource'


interface Props {
    open: boolean
    onClose: () => void
}

export default createFC(import.meta.url)<Props>(function _({ children, theme, open, onClose }) {
    const queryTeamResources = useQuery(QueryTeamResources)
    const available = queryTeamResources.data?.team?.resources.find((res) => res.type.id === RESOURCE_TYPE.Coins)?.available

    const [addCoins] = useMutationResourcesAddCoins()
    const handleAddCoin = useCallback(() => addCoins(1), [addCoins])

    const [removeCoins] = useMutationResourcesRemoveCoins()
    const handleRemoveCoin = useCallback(() => removeCoins(1), [removeCoins])

    return (
        <Dialog fullScreen open={open} onClose={onClose}>
            <Toolbar>
                <Grid item xs />
                <IconButton size='medium' onClick={onClose}>
                    <IconClose />
                </IconButton>
            </Toolbar>
            <PageSection sx={{ margin: theme.spacing(0) }}>
                <PageCard sx={{ '&.MuiPaper-root': { margin: theme.spacing(0, 1) } }} variant='standard' color='neutral' title='Swap coins from real life to app and back'>
                    <Typography paragraph variant='body2'>
                        To <strong>use coins in Throne Game App, deposit</strong> the physical coins in the out-of-game chest and note the deposited amount in the App.
                    </Typography>
                    <Typography paragraph variant='body2'>
                        To <strong>use coins in real life, withdraw</strong> them from the Throne Game App and take the physical coin tokens from your out-of-game chest.
                    </Typography>
                    <Typography gutterBottom variant='h3' color='textSecondary'>The physical coin chest</Typography>
                    <Typography paragraph variant='body2'>
                        Each Castle will have a physical chest for the physical coin tokens. Coins in the chest <strong>does not represent the amount of coins the Faction owns</strong>).
                    </Typography>
                    <Typography paragraph variant='body2'>
                        If the numbers in App and chest do not match - a <strong>GM will audit</strong> and find out any discrepancies and require the Faction to fill the chest as needed.
                    </Typography>
                </PageCard>
            </PageSection>
            <Grid item xs />
            <PageSection title='Swap coins'>
                <PageSectionPart>
                    <Typography paragraph variant="body1">
                        Coins available in Throne Game App: <Resource type={RESOURCE_TYPE.Coins} amount={available} />
                    </Typography>
                    <Typography paragraph variant="body1">
                        Physical coin tokens left in chest: {queryTeamResources.data?.team?.coinbox ?? '?'}
                    </Typography>
                    <Grid container justifyContent="space-between" wrap='nowrap'>
                        <Button variant="outlined" endIcon={<IconRemove />} onClick={handleRemoveCoin}>
                            Withdraw
                        </Button>
                        <Button variant="contained" startIcon={<IconAdd />} onClick={handleAddCoin}>
                            Deposit
                        </Button>
                    </Grid>
                </PageSectionPart>
            </PageSection>
            <Grid item xs />
            <PageSection>
                <PageSectionPart>
                    <Button onClick={onClose}>
                        Back
                    </Button>
                </PageSectionPart>
            </PageSection>
        </Dialog>
    )
}) /* =============================================================================================================== */
