import React from 'react'

import { createSvgIconFC } from 'src/common'


export default createSvgIconFC(<g>
    <path fillRule="evenodd" clipRule="evenodd" d="M17.0278 4H6.54416V5.53255L5.69954 4.87922L3 7.948L6.54416 10.6895V20H17.0278V16.576L21.9999 11.1237L17.9759 7.90295L17.0278 8.94266V4ZM19.5 11.2L15.5 15.5V18.5H8V10L5.5 8L6 7.5L8 9V5.5H15.5V12.5L18 10L19.5 11.2Z" fill="currentColor" />
</g>)
