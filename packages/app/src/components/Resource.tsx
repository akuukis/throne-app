import React from 'react'

import { FOOD_SUBTYPE, RESOURCE_TYPE } from '@larts/api'
import { SvgIconProps, Theme } from '@material-ui/core'
import { SxProps } from '@material-ui/system'
import { Box } from '@material-ui/system'

import { createFC, createStyles } from 'src/common'

import IconCoinColored from './icons/resource/IconCoinColored'
import IconCoinFilled from './icons/resource/IconCoinFilled'
import IconCoinOutlined from './icons/resource/IconCoinOutlined'
import IconCucumberColored from './icons/resource/IconCucumberColored'
import IconCucumberFilled from './icons/resource/IconCucumberFilled'
import IconCucumberOutlined from './icons/resource/IconCucumberOutlined'
import IconHidesColored from './icons/resource/IconHidesColored'
import IconHidesFilled from './icons/resource/IconHidesFilled'
import IconHidesOutlined from './icons/resource/IconHidesOutlined'
import IconHoneyColored from './icons/resource/IconHoneyColored'
import IconHoneyFilled from './icons/resource/IconHoneyFilled'
import IconHoneyOutlined from './icons/resource/IconHoneyOutlined'
import IconIronColored from './icons/resource/IconIronColored'
import IconIronFilled from './icons/resource/IconIronFilled'
import IconIronOutlined from './icons/resource/IconIronOutlined'
import IconLavenderColored from './icons/resource/IconLavenderColored'
import IconLavenderFilled from './icons/resource/IconLavenderFilled'
import IconLavenderOutlined from './icons/resource/IconLavenderOutlined'
import IconLeafColored from './icons/resource/IconLeafColored'
import IconLeafFilled from './icons/resource/IconLeafFilled'
import IconLeafOutlined from './icons/resource/IconLeafOutlined'
import IconMushroomColored from './icons/resource/IconMushroomColored'
import IconMushroomFilled from './icons/resource/IconMushroomFilled'
import IconMushroomOutlined from './icons/resource/IconMushroomOutlined'
import IconRaspberryColored from './icons/resource/IconRaspberryColored'
import IconRaspberryFilled from './icons/resource/IconRaspberryFilled'
import IconRaspberryOutlined from './icons/resource/IconRaspberryOutlined'
import IconTomatoColored from './icons/resource/IconTomatoColored'
import IconTomatoFilled from './icons/resource/IconTomatoFilled'
import IconTomatoOutlined from './icons/resource/IconTomatoOutlined'
import IconWoodColored from './icons/resource/IconWoodColored'
import IconWoodFilled from './icons/resource/IconWoodFilled'
import IconWoodOutlined from './icons/resource/IconWoodOutlined'


type Variant = 'filled' | 'outlined' | 'colored'

interface IProps {
    type: RESOURCE_TYPE
    variant?: Variant
    subtype?: FOOD_SUBTYPE | null
    amount?: number
    sx?: SxProps
    title?: boolean
}

const ICONS: Record<Variant, Record<RESOURCE_TYPE, React.FC<SvgIconProps>>> = {
    colored: {
        [RESOURCE_TYPE.Wood]: IconWoodColored,
        [RESOURCE_TYPE.Hides]: IconHidesColored,
        [RESOURCE_TYPE.Iron]: IconIronColored,
        [RESOURCE_TYPE.Coins]: IconCoinColored,
        [FOOD_SUBTYPE.Honey]: IconHoneyColored,
        [FOOD_SUBTYPE.Lavender]: IconLavenderColored,
        [FOOD_SUBTYPE.Tomatoes]: IconTomatoColored,
        [FOOD_SUBTYPE.Cucumbers]: IconCucumberColored,
        [FOOD_SUBTYPE.Mushrooms]: IconMushroomColored,
        [FOOD_SUBTYPE.Raspberries]: IconRaspberryColored,
        [RESOURCE_TYPE.Food]: IconLeafColored,
    },
    filled: {
        [RESOURCE_TYPE.Wood]: IconWoodFilled,
        [RESOURCE_TYPE.Hides]: IconHidesFilled,
        [RESOURCE_TYPE.Iron]: IconIronFilled,
        [RESOURCE_TYPE.Coins]: IconCoinFilled,
        [FOOD_SUBTYPE.Honey]: IconHoneyFilled,
        [FOOD_SUBTYPE.Lavender]: IconLavenderFilled,
        [FOOD_SUBTYPE.Tomatoes]: IconTomatoFilled,
        [FOOD_SUBTYPE.Cucumbers]: IconCucumberFilled,
        [FOOD_SUBTYPE.Mushrooms]: IconMushroomFilled,
        [FOOD_SUBTYPE.Raspberries]: IconRaspberryFilled,
        [RESOURCE_TYPE.Food]: IconLeafFilled,
    },
    outlined: {
        [RESOURCE_TYPE.Wood]: IconWoodOutlined,
        [RESOURCE_TYPE.Hides]: IconHidesOutlined,
        [RESOURCE_TYPE.Iron]: IconIronOutlined,
        [RESOURCE_TYPE.Coins]: IconCoinOutlined,
        [FOOD_SUBTYPE.Honey]: IconHoneyOutlined,
        [FOOD_SUBTYPE.Lavender]: IconLavenderOutlined,
        [FOOD_SUBTYPE.Tomatoes]: IconTomatoOutlined,
        [FOOD_SUBTYPE.Cucumbers]: IconCucumberOutlined,
        [FOOD_SUBTYPE.Mushrooms]: IconMushroomOutlined,
        [FOOD_SUBTYPE.Raspberries]: IconRaspberryOutlined,
        [RESOURCE_TYPE.Food]: IconLeafOutlined,
    },
}

/**
 * DB Fields "name" and "subtype" are unused, because fetching the names from DB is impractical as this is used all over the place.
 */
const TYPE_TITLE = {
    [RESOURCE_TYPE.Wood]: 'Wood',
    [RESOURCE_TYPE.Hides]: 'Hides',
    [RESOURCE_TYPE.Iron]: 'Iron',
    [RESOURCE_TYPE.Coins]: 'Coins',
    [RESOURCE_TYPE.Food]: 'Food',
}

/**
 * DB Fields "name" and "subtype" are unused, because fetching the names from DB is impractical as this is used all over the place.
 */
const FOOD_SUBTYPE_TITLE = {
    [FOOD_SUBTYPE.Honey]: 'Honey',
    [FOOD_SUBTYPE.Lavender]: 'Lavender',
    [FOOD_SUBTYPE.Tomatoes]: 'Tomatoes',
    [FOOD_SUBTYPE.Cucumbers]: 'Cucumbers',
    [FOOD_SUBTYPE.Mushrooms]: 'Mushrooms',
    [FOOD_SUBTYPE.Raspberries]: 'Raspberries',
}

const styles = (theme: Theme) => createStyles({
    root: {
    },
    icon: {
        verticalAlign: 'text-bottom',
    },
})

export default createFC(import.meta.url, styles)<IProps>(function _({ classes, type, variant = 'filled', title = false, amount, sx }) {
    const subtype = FOOD_SUBTYPE_TITLE[type]

    const Icon = ICONS[variant][type]
    return (
        <Box component='span' sx={sx}>
            {amount}
            <Icon className={classes.icon} fontSize='small' />
            {title && (
                <>
                    {amount === undefined && ` ${TYPE_TITLE[type] ?? ''}`}
                    {subtype && `Food: ${subtype}`}
                </>
            )}
        </Box>
    )
}) /* ============================================================================================================== */
