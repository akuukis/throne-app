import { MutationHookOptions, OperationVariables, useMutation as useMutationBase } from '@apollo/client'
import { ApiBaseMutationOptions } from '@larts/api/src/mutations/common'


export const useMutation = <TData = any, TVariables = OperationVariables>(apiQuery: ApiBaseMutationOptions<TData, TVariables>, options?: MutationHookOptions<TData, TVariables>) => {
    const { mutation, ...baseOptions } = apiQuery
    return useMutationBase<TData, TVariables>(mutation, { ...baseOptions, ...options })
}
