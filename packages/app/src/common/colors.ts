import { PaletteMode, darken, lighten } from '@material-ui/core'
import { Palette, PaletteColor } from '@material-ui/core/styles/createPalette'

/**
 * In MUI design there's a notion of "Dark Text" (in context of light theme) but no appropriate value in pallete.
 * It is derived from the main color:
 * - 60% darker in light theme
 * - 60% lighter in dark theme.
 *
 * See also `foregroundShade`.
 */
export const foreground = (type: PaletteMode, mainColor: string) => {
    return type === 'light' ? darken(mainColor, 0.6) : lighten(mainColor, 0.6)
}

/**
 * In MUI design there's a notion of "Light Background" (in context of light theme) but no appropriate value in pallete.
 * It is derived from the main color:
 * - 90% lighter in light theme
 * - 90% darker in dark theme
 *
 * See also `backgroundShade`.
 *
 * @deprecated - Use the custom color `primary.background` instead.
 */
export const background = (type: PaletteMode, mainColor: string) => {
    return type === 'light' ? lighten(mainColor, 0.90) : darken(mainColor, 0.90)
}

/**
 * Returns `dark` in light theme but `light` in dark theme. Useful for e.g. contained buttons.
 *
 * See also `foreground`.
 */
export const foregroundShade = (type: PaletteMode, paletteColor: PaletteColor) => {
    return type === 'light' ? paletteColor.dark : paletteColor.light
}

/**
 * Returns `light` in light theme but `dark` in dark theme. Useful for e.g. contained buttons.
 *
 * See also `background`.
 */
export const backgroundShade = (type: PaletteMode, paletteColor: PaletteColor) => {
    return type === 'light' ? paletteColor.light : paletteColor.dark
}

export const borderColor = (palette: Palette) => palette.mode === 'light' ? palette.grey[400] : palette.grey[700]
