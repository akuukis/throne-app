/* eslint-disable import/export */
/* eslint-disable @typescript-eslint/ban-types */

import {
    FC,
    ForwardRefExoticComponent,
    ForwardRefRenderFunction,
    FunctionComponent,
    MemoExoticComponent,
    PropsWithChildren,
    PropsWithoutRef,
    Ref,
    RefAttributes,
    useMemo,
} from 'react'

import { ClassKeyOfStyles, ClassNameMap, StyledComponentProps, StyledProps, Styles, makeStyles, useTheme } from '@material-ui/styles'
import classnames from 'classnames'
import { observer } from 'mobx-react-lite'
import { basename, dirname, sep } from 'path-browserify'

import { IMyTheme } from './myTheme'


const getDebugName = (fullPath: string) => {
    const dirs = dirname(fullPath).split(sep)
    if (dirs[0] === '.') {  // No path, thus custom string.
        return fullPath
    } else {
        const filename = basename(fullPath)
        const prefix = dirs.slice(filename === 'index.tsx' ? -2 : -1).join('-')
        return `${prefix}-${filename}`
    }
}

/**
 * Standard way to create a react function component. Features:
 * - wraps in `React.memo`.
 * - passes `theme` as props.
 * - *if* provided `styles`, *then* exposes `className` too and merge it into `classes.root`, like MUI does.
 * - *in development* sets name for both react tree and css prefix.
 * - correct types for everything above.
 *
 * ## Boilerplate (minimal):
 * ```
 * import React from "react";
 * import { createFC } from "@coinmetro/shared/common/createFC";
 *
 * export default createFC(import.meta.url)(() => (
 *   <div>Hello World</div>
 * )
 * ```
 *
 * ## Boilerplate (inline):
 * ```
 * const inline = createFC("debugNameForThis")(() => (
 *   <div>Hello World</div>
 * ))
 * ```
 *
 * ## Boilerplate (full):
 * ```
 * import React from "react";
 * import { Theme, createStyles } from "@material-ui/core";
 * import { createFC } from "@coinmetro/shared/common/createFC";
 *
 * const styles = (theme: Theme) => createStyles({
 *   root: {
 *   },
 * });
 *
 * interface MyProps {
 * }
 *
 * export default createFC(import.meta.url, styles)<MyProps>(function _({classes, theme, ...props}) {
 *   return (
 *     <div className={classes.root}>Hello World</div>
 *   )
 * })
 * ```
 *
 * ## paramaterized props
 * Definition (add to the full boilerplate above)
 * ```
 * interface MyProps<T extends string> {
 *   special: T
 * }
 * export type MyComponentParameterized<T extends string> = createFC.NEFC<MyProps<T>, keyof ReturnType<typeof styles>>
 * ```
 * Usage
 * ```
 * import { MyComponent as MyComponentGeneric, MyComponentParameterized } from "@coinmetro/app/components/MyComponent"
 * const MyComponent = MyComponentGeneric as MyComponentParameterized<"foo" | "bar">;
 * ```
 */
export const createFC = <TClasses extends string = never>(
    __filename = 'anonymous',
    styles?: Styles<IMyTheme, Record<string, unknown>, TClasses>,
) => {
    // Note: One function would be simpler, but two functions are required due TS limitations (https://stackoverflow.com/a/60378737/4817809).
    return <TProps extends object>(
        fc: createFC.Inner<TProps, TClasses>,
    ): createFC.NEFC<TProps, TClasses> => {
        const name = process.env.NODE_ENV === 'production' ? undefined : getDebugName(__filename)
        const useStyles = styles && makeStyles(styles as Styles<IMyTheme, Record<string, unknown>, TClasses | 'root'>, { name })

        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        const wrapperFC = ((props: any) => {
            const theme = useTheme<IMyTheme>()

            if (useStyles) {
                // Merge classes with overrides.
                const { className, ...propsWithoutClassName } = props
                const classes = useStyles(propsWithoutClassName)
                const classesWithClassName = useMemo(() => ({ ...classes, root: classnames(classes.root, className) }), [className, classes])
                return fc({ ...propsWithoutClassName, theme, classes: classesWithClassName })
            } else {
                return fc({ ...props, theme })
            }
        }) as FunctionComponent<createFC.NEFCProps<TProps, TClasses>>
        wrapperFC.displayName = name

        return observer<createFC.NEFCProps<TProps, TClasses>>(wrapperFC)
    }
}

/**
 * Forwards ref but doesn't memo.
 *
 * @see createFC
 */
export const createForwardRefFC = <TClasses extends string = never>(
    __filename = 'anonymous',
    styles?: Styles<IMyTheme, Record<string, unknown>, TClasses>,
) => {
    return <TProps extends object, TRef extends unknown = never>(
        fc: createFC.InnerWithRef<TProps, TClasses, TRef>,
    ): createFC.FREFC<TProps, TClasses, TRef> => {
        const name = process.env.NODE_ENV === 'production' ? undefined : getDebugName(__filename)
        const useStyles = styles && makeStyles(styles as Styles<IMyTheme, Record<string, unknown>, TClasses | 'root'>, { name })

        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        const wrapperFC = ((props: any, ref: Ref<any>) => {
            const theme = useTheme<IMyTheme>()

            if (useStyles) {
                // Merge classes with overrides.
                const { className, ...propsWithoutClassName } = props
                const classes = useStyles(propsWithoutClassName)
                const classesWithClassName = useMemo(() => ({ ...classes, root: classnames(classes.root, className) }), [className, classes])
                return fc({ ...propsWithoutClassName, theme, classes: classesWithClassName }, ref)
            } else {
                return fc({ ...props, theme }, ref)
            }
        }) as ForwardRefRenderFunction<TRef, createFC.NEFCProps<TProps, TClasses>>
        wrapperFC.displayName = name

        return observer<createFC.NEFCProps<TProps, TClasses>, TRef>(wrapperFC, { forwardRef: true })
    }
}


export declare namespace createFC {

    /**
   * Type for FC at **definition time**. Goes together with `createFC.InnerProps`.
   */
    export type Inner<
        TProps extends object = object,
        TClassKey extends string = never,
    > = FC<InnerProps<TProps, TClassKey>>

    /**
   * Type for FC props at **definition time**. Goes together with `createFC.Inner`.
   */
    export type InnerProps<
        TProps extends object = object,
        TClassKey extends string = never,
        TTest extends boolean = TClassKey extends never ? true : false,
    > = PropsWithChildren<TProps & { theme: IMyTheme } & Pick<{ classes: ClassNameMap<ClassKeyOfStyles<TClassKey | 'root'>> }, TTest extends false ? 'classes' : never>>

    /**
   * Type for FC at **usage time**. Goes together with `createFC.NEFCProps`.
   */
    export type NEFC<
        TProps extends object = object,
        TClasses extends string = never,
    > = FunctionComponent<NEFCProps<TProps, TClasses>>

    /**
   * Type for FC props at **usage time**. Goes together with `createFC.NEFC`.
   */
    export type NEFCProps<
        TProps extends object = object,
        TClassKey extends string = never,
        TTest extends boolean = TClassKey extends never ? true : false,
    > = PropsWithChildren<TProps & Pick<Partial<StyledProps> & StyledComponentProps<TClassKey | 'root'>, TTest extends false ? 'classes' | 'className' : never>>


    /**
   * Type for createForwardRefFC at **definition time**. Goes together with `createFC.InnerProps`.
   */
    export type InnerWithRef<
        TProps extends object = object,
        TClassKey extends string = never,
        TRef extends unknown = never,
    > = ForwardRefRenderFunction<TRef, InnerProps<TProps, TClassKey>>

    /**
   * Type for createForwardRefFC at **usage time**. Goes together with `createFC.NEFCProps`.
   */
    export type FREFC<
        TProps extends object = object,
        TClasses extends string = never,
        TRef extends unknown = never,
    > = MemoExoticComponent<ForwardRefExoticComponent<PropsWithoutRef<NEFCProps<TProps, TClasses>> & RefAttributes<TRef>>>

}
