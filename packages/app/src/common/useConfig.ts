import { CONFIG, QuerySettings } from '@larts/api'

import { useQuery } from '.'


export default <T extends unknown = string>(id: CONFIG, transform?: (v: string) => T): T | null | undefined => {
    const querySettings = useQuery(QuerySettings, {
        fetchPolicy: 'cache-and-network',
        pollInterval: 60000,
    })

    if (!querySettings.data) return undefined

    const config = querySettings.data?.baseData.settings.find((setting) => setting.code === id)
    if (!config) return null

    return transform ? transform(config.value) : config.value as T
}
