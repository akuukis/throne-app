import { MutationEquipmentUnequip } from '@larts/api/src/mutations'

import { useMutation } from 'src/common'
import useApiCallback from 'src/common/useApiCallback'
import useNotify from 'src/common/useNotify'


export default () => {
    const notify = useNotify()
    const [equipmentUnequip, mutation] = useMutation(MutationEquipmentUnequip)

    return [
        useApiCallback(async () => {
            await equipmentUnequip()
            notify('🎉 Amulet of Phoenix unequipped!')
        }, [equipmentUnequip, notify]),
        mutation,
    ] as const
}
