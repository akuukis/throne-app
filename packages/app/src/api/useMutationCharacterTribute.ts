import { MutationCharacterTribute } from '@larts/api'

import { useMutation } from 'src/common'
import useApiCallback from 'src/common/useApiCallback'
import useNotify from 'src/common/useNotify'


export default () => {
    const notify = useNotify()
    const [characterTribute, mutation] = useMutation(MutationCharacterTribute)

    return [
        useApiCallback(async (code: number) => {
            await characterTribute({ variables: { code } })
            notify('🎉 1 MP for killing someone.')
        }, [characterTribute, notify]),
        mutation,
    ] as const
}
