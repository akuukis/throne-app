import { useState } from 'react'

import { MutationCharacterRespawn, MutationCharacterRevitalize } from '@larts/api'

import { useMutation } from 'src/common'
import useApiCallback from 'src/common/useApiCallback'
import useNotify from 'src/common/useNotify'


export default () => {
    const notify = useNotify()
    const [selected, setSelected] = useState<'respawn'|'revitalize'>('respawn')
    const [characterRespawn, mutationRespawn] = useMutation(MutationCharacterRespawn)
    const [characterRevitalize, mutationRevitalize] = useMutation(MutationCharacterRevitalize)

    return [
        useApiCallback(async (newWristband?: number) => {
            if (newWristband) {
                setSelected('respawn')
                await characterRespawn({ variables: { code: newWristband } })
            } else {
                setSelected('revitalize')
                await characterRevitalize()
            }
            notify('🎉 Respawned!')
        }, [characterRespawn, notify]),
        (selected === 'respawn' ? mutationRespawn : mutationRevitalize),
    ] as const
}
