import { MutationEquipmentAdd } from '@larts/api/src/mutations'

import { useMutation } from 'src/common'
import useApiCallback from 'src/common/useApiCallback'
import useNotify from 'src/common/useNotify'


export default () => {
    const notify = useNotify()
    const [equipmentAdd, mutation] = useMutation(MutationEquipmentAdd)

    return [
        useApiCallback(async () => {
            await equipmentAdd()
            notify('🎉 EP added')
        }, [equipmentAdd, notify]),
        mutation,
    ] as const
}
