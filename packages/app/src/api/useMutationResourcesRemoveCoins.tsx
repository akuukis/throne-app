import React from 'react'

import { MutationResourcesRemoveCoins, RESOURCE_TYPE } from '@larts/api'

import { useMutation } from 'src/common'
import useApiCallback from 'src/common/useApiCallback'
import useNotify from 'src/common/useNotify'
import Resource from 'src/components/Resource'


export default () => {
    const notify = useNotify()
    const [removeCoins, mutation] = useMutation(MutationResourcesRemoveCoins)

    return [
        useApiCallback(async (amount: number) => {
            await removeCoins({ variables: { amount } })
            notify(<>🎉 <Resource type={RESOURCE_TYPE.Coins} amount={amount} /> withdrawn from Throne App</>)
        }, [removeCoins, notify]),
        mutation,
    ] as const
}
