import { MutationRecipeScore } from '@larts/api'

import { useMutation } from 'src/common'
import useApiCallback from 'src/common/useApiCallback'
import useNotify from 'src/common/useNotify'


export default () => {
    const notify = useNotify()
    const [recipeScore, mutation] = useMutation(MutationRecipeScore)

    return [
        useApiCallback(async (id: number, title?: string) => {
            await recipeScore({ variables: { id, title } })
            notify(title ? `🎉 Invented & cooked a recipe "${title}"` : '🎉 Copied & cooked a recipe.')
        }, [recipeScore, notify]),
        mutation,
    ] as const
}
