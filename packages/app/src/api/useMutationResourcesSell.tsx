import React from 'react'

import { MutationResourcesSell, RESOURCE_TYPE } from '@larts/api'

import { useMutation } from 'src/common'
import useApiCallback from 'src/common/useApiCallback'
import useNotify from 'src/common/useNotify'
import Resource from 'src/components/Resource'


export default () => {
    const notify = useNotify()
    const [resourceSell, mutation] = useMutation(MutationResourcesSell)

    return [
        useApiCallback(async (resourceType: RESOURCE_TYPE, amount: number) => {
            await resourceSell({ variables: { id: resourceType, amount } })
            notify(<>🎉 Sold <Resource type={resourceType} amount={amount} /> for <Resource type={RESOURCE_TYPE.Coins} amount={amount} /></>)
        }, [resourceSell, notify]),
        mutation,
    ] as const
}
