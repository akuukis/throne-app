import { ReactNode, createContext } from 'react'


// eslint-disable-next-line unused-imports/no-unused-vars-ts
export const NotifyContext = createContext((text: ReactNode) => undefined as void)
