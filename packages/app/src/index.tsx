/* eslint-disable import/no-unassigned-import */
/* eslint-disable import/no-unused-modules */
import 'core-js/stable'
import 'regenerator-runtime/runtime'

import React from 'react'

import { ApolloClient, ApolloProvider, InMemoryCache } from '@apollo/client'
import { CssBaseline } from '@material-ui/core'
import { ThemeProvider } from '@material-ui/core'
import { configure } from 'mobx'
import { render } from 'react-dom'

import { getTheme } from './common'
import Routes from './Routes'


// enable MobX strict mode
configure({ enforceActions: 'always' })

const client = new ApolloClient({
    uri: process.env.NODE_ENV === 'development' ? process.env.API_URL : 'https://api.tronis.lv',
    cache: new InMemoryCache(),
    credentials: 'include',
})

render((
    <ApolloProvider client={client}>
        <ThemeProvider theme={getTheme()}>
            <CssBaseline />
            <Routes />
        </ThemeProvider>
    </ApolloProvider>
),
document.getElementById('root'),
)
