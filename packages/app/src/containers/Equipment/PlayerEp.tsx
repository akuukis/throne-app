import React, { useCallback } from 'react'

import { QuerySettings, QueryTeamEquipment, QueryTeamResources, QueryUserEquipment } from '@larts/api'
import { Button, Checkbox, FormControlLabel, FormGroup, Grid, Link, Typography } from '@material-ui/core'
import IconAdd from '@material-ui/icons/Add'
import IconRemove from '@material-ui/icons/Remove'

import useMutationEquipmentAdd from 'src/api/useMutationEquipmentAdd'
import useMutationEquipmentEquip from 'src/api/useMutationEquipmentEquip'
import useMutationEquipmentRemove from 'src/api/useMutationEquipmentRemove'
import useMutationEquipmentUnequip from 'src/api/useMutationEquipmentUnequip'
import { createFC, useQuery } from 'src/common'
import PageCard from 'src/components/PageCard'
import PageSection from 'src/components/PageSection'
import PageSectionPart from 'src/components/PageSectionPart'
import ProgressFullscreen from 'src/components/ProgressFullscreen'


import { pricePhoenix } from './common'
import EpPricelist from './EpPricelist'


export default createFC(import.meta.url)(function _({ children, theme }) {
    const [dialog, setDialog] = React.useState(false)

    const querySettings = useQuery(QuerySettings)
    const queryTeamResources = useQuery(QueryTeamResources)
    const queryTeamEP = useQuery(QueryTeamEquipment)
    const queryMyEP = useQuery(QueryUserEquipment)

    const [handleAdd] = useMutationEquipmentAdd()
    const [handleRemove] = useMutationEquipmentRemove()
    const [handleEquip] = useMutationEquipmentEquip()
    const [handleUnequip] = useMutationEquipmentUnequip()

    const handleOpenDialog = useCallback((e: React.MouseEvent<HTMLAnchorElement>) => {e.preventDefault(); setDialog(true)}, [setDialog])
    const handleCloseDialog = useCallback(() => setDialog(false), [setDialog])

    if (querySettings.loading) return <ProgressFullscreen />
    if (queryTeamResources.loading) return <ProgressFullscreen />
    if (queryTeamEP.loading) return <ProgressFullscreen />
    if (queryMyEP.loading) return <ProgressFullscreen />

    if (!queryMyEP?.data?.user) return null

    const total = queryMyEP?.data?.user?.equipmentPoints
    const phoenixCost = pricePhoenix(total)
    const isPhoenix = queryMyEP?.data?.user?.equipmentHasPhoenix
    const remaining = total - (isPhoenix ? phoenixCost : 0)


    return (
        <PageSection>
            <PageCard variant='standard'>
                <PageSectionPart>
                    <Typography variant='h3' gutterBottom>{`Personal EP: ${queryMyEP?.data?.user?.equipmentPoints}`}</Typography>
                    <Typography paragraph>
                        Take from or put back to team's EP pool.
                    </Typography>
                    <Grid container spacing={1}>
                        <Grid item>
                            <Button
                                variant='outlined'
                                startIcon={<IconRemove />}
                                onClick={handleRemove}
                                disabled={queryMyEP?.data?.user?.equipmentPoints === 0}
                            >
                                Remove
                            </Button>
                        </Grid>
                        <Grid item>
                            <Button
                                variant='contained'
                                startIcon={<IconAdd />}
                                onClick={handleAdd}
                                disabled={queryTeamEP.data?.team?.availableEq === 0}
                            >
                                Add
                            </Button>
                        </Grid>
                    </Grid>
                </PageSectionPart>
                <PageSectionPart>
                    <Typography variant='h3' gutterBottom>Virtual Items</Typography>
                    <FormGroup>
                        <FormControlLabel
                            control={<Checkbox checked={isPhoenix} onClick={isPhoenix ? handleUnequip : handleEquip} />}
                            label={`Amulet of Phoenix (${phoenixCost} EP)`}
                        />
                    </FormGroup>
                </PageSectionPart>
                <PageSectionPart>
                    <Typography variant='h3' gutterBottom>Physical Items</Typography>
                    <Typography paragraph>
                        You have <strong>{remaining}</strong> remaining EP for physical items.
                    </Typography>
                    <Typography paragraph>
                        <Link onClick={handleOpenDialog}>What can I afford to wear?</Link>
                    </Typography>
                </PageSectionPart>
            </PageCard>
            <EpPricelist open={dialog} ep={queryMyEP?.data?.user?.equipmentPoints ?? 0} onClose={handleCloseDialog} />
        </PageSection>
    )
}) /* =============================================================================================================== */
