import React from 'react'

import { QueryTeamUpgrades } from '@larts/api'

import { createFC, useQuery } from 'src/common'
import PageSection from 'src/components/PageSection'
import ProgressFullscreen from 'src/components/ProgressFullscreen'

import SubmittedCamp from './SubmittedCamp'


export default createFC(import.meta.url)(function _({ children, theme }) {
    const queryTeamUpgrades = useQuery(QueryTeamUpgrades)

    if (queryTeamUpgrades.loading) return <ProgressFullscreen />

    const submittedUpgrades = queryTeamUpgrades.data?.team?.expansions?.filter((upgrade) => !!upgrade.rewarded) ?? []

    return (
        <PageSection title={`Redeemed camp upgrades (${submittedUpgrades.length})`}>
            {submittedUpgrades.map((upgrade) => (
                <SubmittedCamp key={upgrade.code} upgrade={upgrade} />
            ))}
        </PageSection>
    )
}) /* =============================================================================================================== */
