import React from 'react'

import { Button, Typography } from '@material-ui/core'

import { createFC } from 'src/common'
import PageCard from 'src/components/PageCard'
import PageSection from 'src/components/PageSection'
import PageSectionPart from 'src/components/PageSectionPart'


interface IProps {
    onContinue: () => void
}


export default createFC(import.meta.url)<IProps>(function _({ children, theme, onContinue }) {
    return (
        <div>
            <PageSection>
                <PageCard variant='standard' color='neutral'>
                    <Typography paragraph>
                        🚨 If your enemy did not take a tribute, coins or resource tokens from you, all should be trashed!
                    </Typography>
                    <Typography paragraph>
                        <strong>
                            Put the wristband (and tokens) in your Castle's token trash bag.
                        </strong>
                    </Typography>
                    <Typography paragraph>
                        When you have done it, come back and continue respawn process.
                    </Typography>
                </PageCard>
            </PageSection>
            <PageSection>
                <PageSectionPart>
                    <Button variant='contained' fullWidth onClick={onContinue}>
                        Done, continue →
                    </Button>
                </PageSectionPart>
            </PageSection>
        </div>
    )
}) /* =============================================================================================================== */
