import React, { useCallback } from 'react'

import { FOOD_SUBTYPE, QueryWorkshops, RESOURCE_TYPE, WorkshopActivateFoodInput } from '@larts/api'
import { recordEntries } from '@larts/common'
import { Step, StepLabel, Stepper } from '@material-ui/core'

import useMutationTaskQueue from 'src/api/useMutationTaskQueue'
import { IMyTheme, createFC, createStyles } from 'src/common'
import AvailableResources from 'src/components/AvailableResources'
import PageCard from 'src/components/PageCard'

import Wizard01Workshop from './Wizard01Workshop'
import Wizard02Resources from './Wizard02Resources'
import Wizard03Confirm from './Wizard03Confirm'
import WorkshopSize from './WorkshopSize'


const styles = (theme: IMyTheme) => createStyles({
    root: {
    },
})

interface Props {
    onClose: () => void
}

export default createFC(import.meta.url, styles)<Props>(function _({ children, theme, classes, onClose }) {
    const [queueTask] = useMutationTaskQueue()

    const [step, setStep] = React.useState<0 | 1 | 2>(0)
    const [selectedWorkshop, setSelectWorkshop] = React.useState<QueryWorkshops.Response['workshops'][0] | null>(null)
    const [selectedResources, setSelectResources] = React.useState<RESOURCE_TYPE | null>(null)
    const [selectedWood, setSelectWood] = React.useState<number>(1)
    const [selectedFoodVariants, setSelectFoodVariants] = React.useState<Partial<Record<FOOD_SUBTYPE, number>>>({})

    const handleQueueTask = useCallback(async () => {
        console.log('asdf')
        try {
            if (!selectedWorkshop) throw new Error('No workshop selected.')
            if (!selectedResources) throw new Error('No resource selected.')
            const foodParam = selectedResources === RESOURCE_TYPE.Food
                ? recordEntries(selectedFoodVariants).map(([variant, amount]) => ({ variant: Number(variant), amount } as WorkshopActivateFoodInput))
                : undefined
            await queueTask(selectedWorkshop.id, selectedResources, selectedWood, foodParam)
        } catch (err) {
            console.error(err)
        }
        onClose()
    }, [queueTask, selectedWorkshop, selectedResources, selectedWood, selectedFoodVariants])

    return (
        <PageCard className={classes.root}>
            <Stepper activeStep={step}>
                {STEPS.map((label) => (
                    <Step key={label}>
                        <StepLabel>{label}</StepLabel>
                    </Step>
                ))}
            </Stepper>
            <AvailableResources threshold={{
                [RESOURCE_TYPE.Wood]: 1,
                [RESOURCE_TYPE.Hides]: 1,
                [RESOURCE_TYPE.Iron]: 1,
                [RESOURCE_TYPE.Coins]: 1,
                [RESOURCE_TYPE.Food]: 1,
            }}
            />
            {step !== 0 && (
                <PageCard variant='standard' color='neutral' title='Selected task'>
                    {selectedWorkshop && (
                        <WorkshopSize
                            workshop={selectedWorkshop}
                            wood={selectedWood}
                            resource={selectedResources}
                            foodVariants={selectedFoodVariants}
                        />
                    )}
                </PageCard>
            )}
            {step === 0 && (
                <Wizard01Workshop
                    onClose={onClose}
                    setStep={setStep}
                    selectedWorkshop={selectedWorkshop}
                    setSelectWorkshop={setSelectWorkshop}
                />
            )}
            {step === 1 && (
                <Wizard02Resources
                    setStep={setStep}
                    selectedWorkshop={selectedWorkshop!}
                    selectedResources={selectedResources}
                    setSelectResources={setSelectResources}
                    selectedWood={selectedWood}
                    setSelectWood={setSelectWood}
                    selectedFoodVariants={selectedFoodVariants}
                    setSelectFoodVariants={setSelectFoodVariants}
                />
            )}
            {step === 2 && <Wizard03Confirm setStep={setStep} queueTask={handleQueueTask} />}
        </PageCard>
    )
}) /* =============================================================================================================== */

const STEPS = [
    'Task size',
    'Resources',
    'Confirm',
]
