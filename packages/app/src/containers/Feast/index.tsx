import React, { useEffect, useState } from 'react'

import { QueryTeamFeasts } from '@larts/api'
import { Alert, AlertTitle, Typography } from '@material-ui/core'

import { createFC, useQuery } from 'src/common'
import useSubpaths from 'src/common/useSubpaths'
import FeastCard from 'src/components/FeastCard'
import PageHeader from 'src/components/PageHeader'
import PageSection from 'src/components/PageSection'
import PageSectionPart from 'src/components/PageSectionPart'
import ProgressFullscreen from 'src/components/ProgressFullscreen'

import AddParticipants from './AddParticipants'


export default createFC(import.meta.url)(function _({ children, theme }) {
    const subpath = useSubpaths()
    const id = Number(subpath)
    const [participants, setParticipants] = useState<null | (null | string)[]>(null)

    const queryFeasts = useQuery(QueryTeamFeasts)

    const feast = queryFeasts.data?.team?.feastTokens.find((feast) => feast.id === id)

    useEffect(() => {
        if (feast) setParticipants(new Array(6).fill(null))
    }, [feast])


    if (queryFeasts.loading) return <ProgressFullscreen />

    if (!feast) return (
        <PageSection>
            <PageSectionPart>
                <Alert severity='error'>
                    <AlertTitle>Feast not found.</AlertTitle>
                </Alert>
            </PageSectionPart>
        </PageSection>
    )

    return (
        <div>
            <PageHeader title="Feast">
                <Typography paragraph>
                    Feast is ~5 min long group activity with group dances (danči) <strong>between at least two different Factions</strong>, happening at one’s Yard. It generates Agriculture points (AP).
                </Typography>
                <Typography gutterBottom variant='h3'>
                    A feast requires
                </Typography>
                <ul>
                    <Typography component='li'>
                        couple(s) of players that are of opposite sex and from different Factions
                    </Typography>
                    <Typography component='li'>
                        a feast token
                    </Typography>
                </ul>
                <Typography gutterBottom variant='h3'>
                    Guests get more AP than hosts
                </Typography>
                <Typography paragraph>
                    In feast, Agriculture Points (AP) are split:
                </Typography>
                <ul>
                    <Typography component='li'>
                        1 AP to host faction per couple
                    </Typography>
                    <Typography component='li'>
                        2 AP to guest faction per couple
                    </Typography>
                </ul>
            </PageHeader>
            <PageSection>
                <FeastCard feast={feast} action={false} />
            </PageSection>
            {participants !== null && <AddParticipants feast={feast} participants={participants} setParticipants={setParticipants} />}
        </div>
    )
}) /* =============================================================================================================== */
