import React, { useCallback, useState } from 'react'

import { QueryTeamFeasts, QueryUserOther } from '@larts/api'
import { Button, Grid, Typography } from '@material-ui/core'
import { useNavigate } from 'react-router'

import useMutationFeastScore from 'src/api/useMutationFeastScore'
import { createFC, useNavigateToHref, useQuery } from 'src/common'
import useNotify from 'src/common/useNotify'
import DialogScanQrCode from 'src/components/DialogScanQrCode'
import PageSection from 'src/components/PageSection'
import PageSectionPart from 'src/components/PageSectionPart'
import { ROUTE, makeRoute } from 'src/constants'

import PlayerBox from './PlayerBox'


interface Props {
    feast: NonNullable<QueryTeamFeasts.Response['team']>['feastTokens'][0]
    participants: (null | string)[]
    setParticipants: (participants: (null | string)[]) => void
}

export default createFC(import.meta.url)<Props>(function _({ children, theme, feast, participants, setParticipants }) {
    const queryUserOther = useQuery(QueryUserOther, { skip: true })

    const navigateToHref = useNavigateToHref()
    const navigate = useNavigate()
    const notify = useNotify()
    const [open, setOpen] = useState<{id: number, sex: 'male' | 'female'} | null>(null)
    const handleClose = useCallback(() => setOpen(null), [])
    const handleAdd = useCallback((id: number, sex: 'male' | 'female') => setOpen({ id, sex }), [])

    const handleScan = useCallback(async (qrCode: string) => {
        if (!open) return  // typeguard
        if (participants.includes(qrCode)) return notify('Already added.')
        const user = await queryUserOther.refetch({ qrCode })
        if (user.data.user?.sex !== open.sex) return notify(`Required ${open.sex}, but got ${user.data.user?.sex}.`)

        notify(`${user.data.user?.name} joined the feast.`)
        handleClose()
        const participantsNew = [...participants.slice(0, open.id), qrCode, ...participants.slice(open.id + 1, participants.length)] as (string | null)[]
        setParticipants(participantsNew)
    }, [setParticipants, participants, open])

    const handleRemove = useCallback((id: number) => {
        const participantsNew = [...participants.slice(0, id), null, ...participants.slice(id + 1, participants.length)] as (string | null)[]
        setParticipants(participantsNew)
    }, [setParticipants, participants])

    const [feastScore] = useMutationFeastScore()

    const handleFeast = useCallback(async () => {
        const error = await feastScore(feast.id, participants.filter((v) => !!v) as string[])
        if (!error) {
            navigate(makeRoute(ROUTE.Feasts))
        }
    }, [feastScore, feast.id, participants])

    return (
        <PageSection title="Add participants">
            <PageSectionPart>
                <Typography paragraph>
                    2-6 participants: 1-3 host players + 1-3 guest players.
                    (1-3 couples of players that are of opposite sex.)
                </Typography>
                <Typography paragraph>
                    Scan their <strong>Player QR</strong> codes!
                </Typography>
            </PageSectionPart>
            <PageSectionPart>
                <Grid container>
                    {participants.map((participant, i) => (
                        <Grid key={i} item xs={6}>
                            <PlayerBox
                                id={i}
                                qrCode={participant}
                                onAdd={handleAdd}
                                onRemove={handleRemove}
                            />
                        </Grid>
                    ))}
                </Grid>
            </PageSectionPart>
            <PageSectionPart>
                <Grid container justifyContent='space-between'>
                    <Button variant='text' href={makeRoute(ROUTE.Feasts)} onClick={navigateToHref}>Back</Button>
                    <Button variant='contained' onClick={handleFeast} disabled={participants.filter((v) => !!v).length < 2}>Start feast</Button>
                </Grid>
            </PageSectionPart>
            <DialogScanQrCode open={!!open} onClose={handleClose} onScan={handleScan} />
        </PageSection>
    )
}) /* =============================================================================================================== */
