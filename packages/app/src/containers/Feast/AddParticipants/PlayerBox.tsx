import React, { useCallback } from 'react'

import { QueryUserOther, TEAM_COLOR } from '@larts/api'
import { Grid, ThemeProvider, Typography } from '@material-ui/core'

import { IMyTheme, createStyles, useQuery } from 'src/common'
import { createFC, getTheme } from 'src/common'
import IconFemale from 'src/components/icons/IconFemale'
import IconMale from 'src/components/icons/IconMale'
import PageCard from 'src/components/PageCard'
import PageSectionPart from 'src/components/PageSectionPart'


const styles = (theme: IMyTheme) => createStyles({
    root: {
        '&:hover': {
            cursor: 'pointer',
            backgroundColor: theme.palette.background.paper,
        },
    },
})

interface Props {
    id: number
    qrCode: string | null
    onRemove: (id: number) => void
    onAdd: (id: number, sex: 'male' | 'female') => void
}

export default createFC(import.meta.url, styles)<Props>(function _({ children, theme, classes, id, qrCode, onAdd, onRemove }) {
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    const queryUser = useQuery(QueryUserOther, { skip: !qrCode, variables: { qrCode: qrCode! } })
    const sex= id % 2 === 0 ? 'male' : 'female'

    const Icon = sex === 'male' ? IconMale : IconFemale

    const handleClick = useCallback(() => qrCode ? onRemove(id) : onAdd(id, sex), [onRemove, qrCode])

    return (
        <ThemeProvider theme={getTheme(queryUser.data?.user?.team.color as TEAM_COLOR | undefined)}>
            <PageCard className={classes.root} onClick={handleClick}>
                <PageSectionPart>
                    <Grid container justifyContent='center'>
                        <Icon fontSize='large' color={!qrCode ? 'disabled' : 'primary'} />
                    </Grid>
                </PageSectionPart>
                <PageSectionPart>
                    <Typography align='center' color='textSecondary'>
                        {queryUser.data?.user?.name ?? `Add ${sex}`}</Typography>
                </PageSectionPart>
            </PageCard>
        </ThemeProvider>
    )
}) /* =============================================================================================================== */
