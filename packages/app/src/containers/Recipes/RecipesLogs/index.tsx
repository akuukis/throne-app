import React from 'react'

import { QueryRecipes, RESOURCE_TYPE } from '@larts/api'
import { Chip, Divider, Grid, Typography } from '@material-ui/core'

import { IMyTheme, createFC, createStyles, formatTimeAbsolute, useQuery } from 'src/common'
import IconFeast from 'src/components/icons/IconFeast'
import PageSection from 'src/components/PageSection'
import PageSectionPart from 'src/components/PageSectionPart'
import ProgressFullscreen from 'src/components/ProgressFullscreen'
import Resource from 'src/components/Resource'


const styles = (theme: IMyTheme) => createStyles({
    root: {
    },
    feast: {
        verticalAlign: 'text-bottom',
    },
})


interface Props {
}

export default createFC(import.meta.url, styles)<Props>(function _({ children, theme, classes }) {
    const queryRecipes = useQuery(QueryRecipes)

    const available = queryRecipes.data?.recipes
        .map(({ cookLog, ...recipe }) => cookLog.map((logEntry) => ({ ...recipe, ...logEntry })))
        .flat()
        .sort((a, b) => b.created - a.created)

    if (queryRecipes.loading) return <ProgressFullscreen />

    return (
        <div>
            {available?.map((logEntry) => (
                <PageSection key={logEntry.id} color='paper'>
                    <PageSectionPart>
                        <Grid container spacing={1} sx={{ marginBottom: 1 }} alignItems='flex-start'>
                            <Grid item>
                                <Typography>{formatTimeAbsolute(logEntry.created)}</Typography>
                            </Grid>
                            <Grid item>
                                <Divider orientation='vertical' sx={{ height: 20 }} />
                            </Grid>
                            <Grid item>
                                <Typography>{logEntry.team.title}</Typography>
                            </Grid>
                            <Grid item>
                                <Divider orientation='vertical' sx={{ height: 20 }} />
                            </Grid>
                            <Grid item>
                                <Typography>{logEntry.firstCookedBy?.id === logEntry.team.id ? 'invented recipe' : 'copied recipe'}</Typography>
                            </Grid>
                            <Grid item xs />
                        </Grid>
                        <Typography gutterBottom>
                            {logEntry.resources.length} AP
                            {logEntry.firstCookedBy?.id === logEntry.team.id && (
                                <>
                                    {' '}
                                    & Feast token {new Array(logEntry.resources.length - 2).fill(null).map((_, i) => (<IconFeast key={i} className={classes.feast} />))}
                                </>
                            )}
                        </Typography>
                        <Typography gutterBottom><em>{logEntry.title}</em></Typography>
                        <Grid container spacing={1}>
                            {[{ id: RESOURCE_TYPE.Coins }, ...logEntry.resources]
                                .map((res) => (
                                    <Grid key={res.id} item>
                                        <Chip
                                            color="default"
                                            variant='filled'
                                            label={<Resource amount={res.id === RESOURCE_TYPE.Coins ? 2 : 1} type={res.id} />}
                                        />
                                    </Grid>
                                ))
                            }
                        </Grid>
                    </PageSectionPart>
                </PageSection>
            ))}
        </div>
    )
}) /* =============================================================================================================== */
