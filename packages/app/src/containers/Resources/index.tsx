import React from 'react'

import { RESOURCE_TYPE } from '@larts/api'
import { Typography } from '@material-ui/core'
import { Navigate, Route, Routes } from 'react-router'
import { Link } from 'react-router-dom'

import { createFC } from 'src/common'
import BottomInputActions from 'src/components/BottomInputActions'
import PageHeader from 'src/components/PageHeader'
import PageTabs from 'src/components/PageTabs'
import Resource from 'src/components/Resource'
import { ROUTE, SUBROUTE_RESOURCES, makeRoute } from 'src/constants'


import InputStats from './InputStats'
import ResourcesList from './ResourcesList'
import ResourcesLogs from './ResourcesLogs'


const TABS = {
    [SUBROUTE_RESOURCES.List]: 'My Resources',
    [SUBROUTE_RESOURCES.Input]: 'Input Status',
    [SUBROUTE_RESOURCES.Logs]: 'Logs',
}


export default createFC(import.meta.url)(function _({ children, theme }) {
    return (
        <div>
            <PageHeader title='Resources & Coins'>
                <Typography gutterBottom variant='h3'>
                    Main uses for resources:
                </Typography>
                <Typography paragraph>
                    <Resource type={RESOURCE_TYPE.Iron} title /> (from camps): buy <Link to={makeRoute(ROUTE.EquipmentPoints)}>Equipment Points (EP)</Link>
                </Typography>
                <Typography paragraph>
                    <Resource type={RESOURCE_TYPE.Wood} title /> (from camps): fuel the <Link to={makeRoute(ROUTE.Workshops)}>Workshops</Link> for Industry Points (IP)
                </Typography>
                <Typography paragraph>
                    <Resource type={RESOURCE_TYPE.Hides} title /> (from Hounds): sell for 50% more Coins than other recources at Merchant or Local Market
                </Typography>
                <Typography paragraph>
                    <Resource type={RESOURCE_TYPE.Food} title /> (from camps): combine unique recipes <Link to={makeRoute(ROUTE.Recipes)}>Recipes</Link> for Agriculture Points (AP)
                </Typography>
                <Typography paragraph>
                    <Resource type={RESOURCE_TYPE.Coins} title /> are used in most actions. Does not require hauling.
                </Typography>
                <Typography paragraph>
                    All resource types can be used in Workshops.
                </Typography>
                <Typography paragraph>
                    Resources are deposited into the Throne App by scanning their QR code. There’s <strong>no withdrawing</strong> (except for coins, see below).
                </Typography>
                <Typography gutterBottom variant='h3'>
                    Locked in resources
                </Typography>
                <Typography paragraph>
                    Resources are locked in if they are reserved or in use in actions that take time, for example, tasks in Workshops or cooking dishes in Recipes.
                </Typography>
                <Typography gutterBottom variant='h3'>
                    The physical coin chest
                </Typography>
                <Typography paragraph>
                    When Faction deposits or withdraws coins via the Throne Game App, it leaves or takes the coins in the <strong>out-of-game chest</strong>.
                </Typography>
                <Typography paragraph>
                    Each Castle will have a physical chest for the physical coin tokens (that <strong>do not represent the amount of coins the Faction owns</strong>).
                </Typography>
                <Typography paragraph>
                    If the numbers in app and chest do not match - a <strong>GM will audit</strong> and find out any discrepancies and require the Faction to fill the chest as needed.
                </Typography>
            </PageHeader>
            <PageTabs tabs={TABS} />
            <Routes>
                <Route path={SUBROUTE_RESOURCES.Input} element={<InputStats />} />
                <Route path={SUBROUTE_RESOURCES.List} element={<ResourcesList />} />
                <Route path={SUBROUTE_RESOURCES.Logs} element={<ResourcesLogs />} />
                <Route path="*" element={<Navigate to={SUBROUTE_RESOURCES.List} replace />} />
            </Routes>
            <BottomInputActions />
        </div>
    )
}) /* =============================================================================================================== */
