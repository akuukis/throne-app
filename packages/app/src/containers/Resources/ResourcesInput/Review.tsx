import React from 'react'

import { CircularProgress, Table, TableBody, TableCell, TableHead, TableRow } from '@material-ui/core'

import { createFC } from 'src/common'
import { codeToType } from 'src/common/qrCodeToType'
import PageSection from 'src/components/PageSection'
import PageSectionPart from 'src/components/PageSectionPart'
import Resource from 'src/components/Resource'


interface Props {
    recentScans: Set<string>
}

export default createFC(import.meta.url)<Props>(function _({ children, theme, recentScans }) {
    return (
        <PageSection title={`Input status (${recentScans.size}):`}>
            <PageSectionPart>
                <Table>
                    <TableHead>
                        <TableRow>
                            <TableCell>Item</TableCell>
                            <TableCell>Code No.</TableCell>
                            <TableCell>Status</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {[...recentScans.values()].map((code) => (
                            <TableRow>
                                <TableCell><Resource type={codeToType(code)} /></TableCell>
                                <TableCell>{code}</TableCell>
                                <TableCell><CircularProgress size={32} variant='indeterminate' /></TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </PageSectionPart>
        </PageSection>
    )
}) /* =============================================================================================================== */
