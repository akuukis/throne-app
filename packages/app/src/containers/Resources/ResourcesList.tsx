import React from 'react'

import { QueryTeamResources, RESOURCE_TYPE } from '@larts/api'
import { Table, TableBody, TableCell, TableHead, TableRow } from '@material-ui/core'

import { createFC, useQuery } from 'src/common'
import PageSection from 'src/components/PageSection'
import PageSectionPart from 'src/components/PageSectionPart'
import ProgressFullscreen from 'src/components/ProgressFullscreen'
import Resource from 'src/components/Resource'


const BorderlessRow = createFC('BorderlessRow')(function ({ children }) {
    return (
        // TODO: add row-level borders.
        <TableRow sx={{
            'td, th': { border: 0 },
        }}
        >{children}</TableRow>
    )
})

export default createFC(import.meta.url)(function _({ children, theme }) {
    const queryTeamResources = useQuery(QueryTeamResources)

    const food = (queryTeamResources.data?.team?.resources ?? [])
        .filter(({ type }) => type?.id as number >= 5)  // A handy shortcut, because food subtypes are last in the DB.
        .reduce((sum, { available, locked }) => ({ available: sum.available + available, locked: sum.locked + locked }), { available: 0, locked: 0 })

    if (queryTeamResources.loading) return <ProgressFullscreen />

    return (
        <div>
            <PageSection>
                <PageSectionPart>
                    <Table>
                        <TableHead>
                            <BorderlessRow>
                                <TableCell>Resource Type</TableCell>
                                <TableCell>Available</TableCell>
                                <TableCell>Locked in</TableCell>
                            </BorderlessRow>
                        </TableHead>
                        <TableBody>
                            {queryTeamResources.data?.team?.resources
                                .filter(({ type }) => type?.id as number < 5)  // A handy shortcut, because food subtypes are last in the DB.
                                .map(({ type, available, locked }) => (
                                    <BorderlessRow key={type?.id}>
                                        <TableCell><Resource type={type?.id as RESOURCE_TYPE} title /></TableCell>
                                        <TableCell>{available}</TableCell>
                                        <TableCell>{locked}</TableCell>
                                    </BorderlessRow>
                                ))
                            }
                            <BorderlessRow>
                                <TableCell><Resource type={RESOURCE_TYPE.Food} title /></TableCell>
                                <TableCell>{food.available}</TableCell>
                                <TableCell>{food.locked}</TableCell>
                            </BorderlessRow>
                        </TableBody>
                    </Table>
                </PageSectionPart>
                <PageSectionPart>
                    <Table>
                        <TableHead>
                            <BorderlessRow>
                                <TableCell>Food Variant</TableCell>
                                <TableCell>Available</TableCell>
                                <TableCell>Locked in</TableCell>
                            </BorderlessRow>
                        </TableHead>
                        <TableBody>
                            {queryTeamResources.data?.team?.resources
                                .filter(({ type }) => type?.id as number >= 5)  // A handy shortcut, because food subtypes are last in the DB.
                                .map(({ type, available, locked }) => (
                                    <BorderlessRow key={type?.id}>
                                        <TableCell><Resource type={type?.id as RESOURCE_TYPE} title /></TableCell>
                                        <TableCell>{available}</TableCell>
                                        <TableCell>{locked}</TableCell>
                                    </BorderlessRow>
                                ))
                            }
                        </TableBody>
                    </Table>
                </PageSectionPart>
            </PageSection>
        </div>
    )
}) /* =============================================================================================================== */
