import React, { useState } from 'react'
import { useCallback } from 'react'

import { TEAM_ID } from '@larts/api'
import { recordEntries } from '@larts/common'
import { Button, Grid, MenuItem, TextField, Typography } from '@material-ui/core'

import useMutationTeamLogin from 'src/api/useMutationTeamLogin'
import { createFC } from 'src/common'
import PageSection from 'src/components/PageSection'
import PageSectionPart from 'src/components/PageSectionPart'


interface IProps {
    register: ReturnType<typeof useMutationTeamLogin>[0]
}


export default createFC(import.meta.url)<IProps>(function _({ children, theme, register }) {

    const [team, setTeam] = useState<TEAM_ID | 'gm' | undefined>(undefined)
    const [name, setName] = useState('')

    const handleChangeTeam = useCallback((event) => setTeam(event.target.value), [setTeam])
    const handleChangeName = useCallback((event) => setName(event.target.value), [setName])
    const handleRequest = () => register(team === 'gm' ? null : team!, name)

    return (
        <>
            <Grid item flexBasis={0}>
                <PageSection>
                    <PageSectionPart>
                        <Typography variant='h2' gutterBottom>Select your Team:</Typography>
                        <TextField
                            select
                            id="device-team"
                            label="Device Team"
                            value={team ?? ''}
                            onChange={handleChangeTeam}
                            fullWidth
                        >
                            {recordEntries(TEAM_ID).filter(([name]) => name.length >= 2).map(([name, id]) => (
                                <MenuItem key={id} value={id}>{name}</MenuItem>
                            ))}
                            <MenuItem value='gm'>GM</MenuItem>
                        </TextField>
                    </PageSectionPart>
                    <PageSectionPart>
                        <Typography variant='h2' gutterBottom>Name this device:</Typography>
                        <TextField
                            id="device-name"
                            label="Device Name"
                            variant="outlined"
                            value={name}
                            onChange={handleChangeName}
                            fullWidth
                            helperText='Something that identifies the device (device model, color, etc)'
                        />
                    </PageSectionPart>
                </PageSection>
            </Grid>
            <Grid item flexBasis={0}>
                <PageSection>
                    <PageSectionPart>
                        <Button
                            variant='contained'
                            fullWidth
                            onClick={handleRequest}
                            disabled={team === undefined || name.length < 3}
                        >
                            Request Access
                        </Button>
                    </PageSectionPart>
                </PageSection>
            </Grid>
        </>
    )
}) /* =============================================================================================================== */
