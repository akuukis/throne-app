import React from 'react'

import { createFC } from 'src/common'
import BottomInputActions from 'src/components/BottomInputActions'
import SectionTime from 'src/components/SectionTime'

import SectionAvailableResources from './SectionAvailableResources'
import SectionRank from './SectionRank'
import SectionTodo from './SectionTodo'


export default createFC(import.meta.url)(function _({ children, theme }) {
    return (
        <div>
            <SectionTime />
            <SectionRank />
            <SectionAvailableResources />
            <SectionTodo />
            <BottomInputActions />
        </div>
    )
}) /* =============================================================================================================== */
