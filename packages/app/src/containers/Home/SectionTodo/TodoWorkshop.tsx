import React, { useEffect } from 'react'

import { QueryTeamWorkshop, WorkshopActivation } from '@larts/api'
import { Alert, AlertTitle, Button } from '@material-ui/core'

import { createFC, useNavigateToHref, useQuery } from 'src/common'
import PageSectionPart from 'src/components/PageSectionPart'
import { ROUTE, makeRoute } from 'src/constants'

import useTickGame from '../../../common/useTickGame'


interface Props {
    onTodo: (name: string, value: boolean) => void
}

export default createFC(import.meta.url)<Props>(function _({ children, theme, onTodo }) {
    const navigateToHref = useNavigateToHref()
    const queryTeamWorkshops = useQuery(QueryTeamWorkshop)
    const tickGame = useTickGame()

    const visible = !!queryTeamWorkshops.data?.team && !isActive(tickGame, queryTeamWorkshops.data?.team)
    useEffect(() => {
        onTodo('workshop', visible)
    }, [visible, onTodo])


    if (!visible) return null

    return (
        <PageSectionPart>
            <Alert severity='info' variant='outlined' action={<Button href={makeRoute(ROUTE.Workshops)} onClick={navigateToHref}>Workshops</Button>}>
                <AlertTitle>Workshop idle</AlertTitle>
            </Alert>
        </PageSectionPart>
    )
}) /* =============================================================================================================== */

const isActive = (currentTime: number, team: {workshopActivations: Pick<WorkshopActivation, 'finishTime' | 'cancelled'>[]}) => {
    return team.workshopActivations.some((task) => !task.cancelled && task.finishTime > currentTime)
}
