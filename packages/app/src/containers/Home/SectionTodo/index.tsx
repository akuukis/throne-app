import React from 'react'
import { useCallback } from 'react'
import { useState } from 'react'

import { recordValues } from '@larts/common'

import { createFC } from 'src/common'
import NoItemsHere from 'src/components/NoItemsHere'
import PageSection from 'src/components/PageSection'

import TodoEquipment from './TodoEquipment'
import TodoFeast from './TodoFeast'
import TodoFlag from './TodoFlag'
import TodoRecipe from './TodoRecipe'
import TodoTournament from './TodoTournament'
import TodoWorkshop from './TodoWorkshop'


export default createFC(import.meta.url)(function _({ children, theme }) {
    const [todos, setTodos] = useState({})
    const count = recordValues(todos).filter(Boolean).length

    const handleTodos = useCallback((name: string, value: boolean) => {
        setTodos((todos) => ({
            ...todos,
            [name]: value,
        }))
    }, [setTodos])

    return (
        <PageSection color='paper' title={`TODOs (${count})`}>
            <TodoTournament onTodo={handleTodos} />
            <TodoEquipment onTodo={handleTodos} />
            <TodoFlag onTodo={handleTodos} />
            <TodoWorkshop onTodo={handleTodos} />
            <TodoFeast onTodo={handleTodos} />
            <TodoRecipe onTodo={handleTodos} />
            {count === 0 && <NoItemsHere />}
        </PageSection>
    )
}) /* =============================================================================================================== */
