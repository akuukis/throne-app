import React from 'react'

import { QueryTeamEquipment, QueryUserCharacter, QueryUserEquipment, QueryUserName } from '@larts/api'
import { Button, Typography } from '@material-ui/core'

import useMutationUserLogout from 'src/api/useMutationUserLogout'
import { createFC, useNavigateToHref, useQuery } from 'src/common'
import PageCard from 'src/components/PageCard'
import PageSection from 'src/components/PageSection'
import PageSectionPart from 'src/components/PageSectionPart'
import ProgressFullscreen from 'src/components/ProgressFullscreen'
import { ROUTE, makeRoute } from 'src/constants'


interface IProps {
}


export default createFC(import.meta.url)<IProps>(function _({ children, theme }) {
    const navigateToHref = useNavigateToHref()

    const queryUserName = useQuery(QueryUserName)
    const queryUserEquipment = useQuery(QueryUserEquipment)
    const queryUserCharacter = useQuery(QueryUserCharacter)

    const queryTeamEquipment = useQuery(QueryTeamEquipment)

    const [userLogout] = useMutationUserLogout()

    if (queryUserName.loading) return <ProgressFullscreen />
    if (queryUserEquipment.loading) return <ProgressFullscreen />
    if (queryUserCharacter.loading) return <ProgressFullscreen />
    if (queryTeamEquipment.loading) return <ProgressFullscreen />


    return (
        <div>
            <PageSection>
                <PageSectionPart>
                    <Typography paragraph color='textSecondary'>
                        {queryUserName.data?.user?.name} profile
                    </Typography>
                    <Typography paragraph color='textSecondary'>
                        This is your life No. {queryUserCharacter.data?.user?.respawnCount}
                    </Typography>
                </PageSectionPart>
                <PageCard
                    title='Respawn'
                    action={(
                        <Button variant='contained' href={makeRoute(ROUTE.Respawn)} onClick={navigateToHref}>
                            Respawn
                        </Button>
                    )}
                >
                    <Typography paragraph>
                        Current life wristband No: {queryUserCharacter.data?.user?.character?.charcode ?? '-'}
                        {(queryUserCharacter.data?.user?.respawnCount === 0 || queryUserCharacter.data?.user?.character?.isScored) && (
                            <strong>
                                {' '}(you are dead)
                            </strong>
                        )}
                    </Typography>
                </PageCard>
                <PageCard
                    title={`Your EP: ${queryUserEquipment.data?.user?.equipmentPoints}`}
                    action={(
                        <Button href={makeRoute(ROUTE.EquipmentPoints)} onClick={navigateToHref}>
                            Manage EP
                        </Button>
                    )}
                >
                    <Typography gutterBottom>
                        Your faction's unused EP: {queryTeamEquipment.data?.team?.availableEq}
                    </Typography>
                    {/* <Typography variant='caption' color='primary'>
                        what can I afford? →
                    </Typography> */}
                </PageCard>
            </PageSection>
            <PageSection>
                <PageSectionPart>
                    <Button variant='contained' fullWidth onClick={userLogout}>
                        Log Out
                    </Button>
                </PageSectionPart>
            </PageSection>
        </div>
    )
}) /* =============================================================================================================== */
