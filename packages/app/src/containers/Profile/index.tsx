import React from 'react'

import { QueryUserName } from '@larts/api'
import { Navigate, Route, Routes } from 'react-router'

import { createFC, useQuery } from 'src/common'
import PageHeader from 'src/components/PageHeader'
import PageTabs from 'src/components/PageTabs'
import ProgressFullscreen from 'src/components/ProgressFullscreen'
import { SUBROUTE_PROFILE } from 'src/constants'

import Logs from './Logs'
import Profile from './Profile'


const TABS = {
    [SUBROUTE_PROFILE.Profile]: 'Profile',
    [SUBROUTE_PROFILE.Logs]: 'Logs',
}

export default createFC(import.meta.url)(function _({ children, theme }) {
    const queryUserName = useQuery(QueryUserName)

    if (queryUserName.loading) return <ProgressFullscreen />

    return (
        <div>
            <PageHeader title={queryUserName.data?.user?.name} />
            <PageTabs tabs={TABS} />
            <Routes>
                <Route path={SUBROUTE_PROFILE.Profile} element={<Profile />} />
                <Route path={SUBROUTE_PROFILE.Logs} element={<Logs />} />
                <Route path="*" element={<Navigate to={SUBROUTE_PROFILE.Profile} replace />} />
            </Routes>
        </div>
    )
}) /* =============================================================================================================== */
