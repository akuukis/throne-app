import React from 'react'

import { Box, Grid, Typography } from '@material-ui/core'
import { useOutlet } from 'react-router'

import emoji_construction from 'assets/emoji_construction.png'

import { createFC } from 'src/common'


export default createFC(import.meta.url)(function _({ children, theme }) {
    const outlet = useOutlet()
    if (outlet) return outlet

    return (
        <Grid container sx={{ padding: 4, paddingTop: '30vh' }} alignItems='center'  direction='column'>
            <Box component='img' src={emoji_construction} width={100} />
            <Typography variant='h3'>Šī lapa vēl nav gatava...</Typography>
            <Typography paragraph>
                Lūdzu uzgaidi mazliet! Mēs cītīgi strādājam, lai drīz ievietotu saturu arī šajā lapā.
            </Typography>
        </Grid>
    )
}) /* =============================================================================================================== */
