import React, { useState } from 'react'
import { useCallback } from 'react'

import { CONFIG, TEAM_COLOR } from '@larts/api'
import { FLAG_STATUS } from '@larts/api'
import { Team } from '@larts/api'
import { Button, Chip, Grid, Typography } from '@material-ui/core'

import useMutationFlagScore from 'src/api/useMutationFlagScore'
import { createFC, formatTimeRelative } from 'src/common'
import useConfig from 'src/common/useConfig'
import useTickGame from 'src/common/useTickGame'
import DialogScanQrCode from 'src/components/DialogScanQrCode'
import Flag from 'src/components/Flag'
import PageCard from 'src/components/PageCard'


interface Props {
    flagTeam: Team
}

export default createFC(import.meta.url)<Props>(function _({ children, theme, flagTeam }) {
    const [open, setOpen] = useState(false)
    const handleOpen = useCallback(() => setOpen(true), [setOpen])
    const handleClose = useCallback(() => setOpen(false), [setOpen])
    const flagLength = useConfig(CONFIG.FlagHomeScoringAt, Number) ?? 0
    const tickGame = useTickGame()

    const [flagScore] = useMutationFlagScore()
    const handleScore = useCallback(async (qrCode: string) => {
        await flagScore(qrCode)
        handleClose()
    }, [flagScore, handleClose])

    const status = !flagTeam.flagRaisedTime ? 'lowered'
        : !flagTeam.flagScoredTime ? 'raised'
            : flagTeam.flagStatusId === FLAG_STATUS.Stolen ? 'captured'
                : 'defended'

    const action = ({
        lowered: <Button variant='contained' onClick={handleOpen}>Raise flag</Button>,
        // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
        raised: ((tickGame - flagTeam.flagRaisedTime!) < flagLength*60) ? <Chip color='primary' label={`Raised: ${Math.floor((tickGame - flagTeam.flagRaisedTime!) / 60)}min`} /> : <Button variant='contained' onClick={handleOpen}>Score flag</Button>,
        captured: <Chip color='primary' label="captured" />,
        defended: <Chip color='primary' label="defended" />,
    })[status]

    const text = ({
        lowered: `Defend for ${formatTimeRelative(flagLength * 60)} during this season and score 5 MP`,
        raised: `Defend for ${formatTimeRelative(flagLength * 60)} during this season and score 5 MP`,
        captured: 'Can\'t score this season anymore.',
        defended: 'Can\'t score this season anymore.',
    })[status]

    return (
        <PageCard
            title={<Flag color={flagTeam.color as TEAM_COLOR} title={flagTeam.title} />}
            action={action}
            variant='standardOutlined'
        >
            <Grid container alignItems='flex-end'>
                <Grid item xs>
                    <Typography paragraph variant='body2'>
                        {text}
                    </Typography>
                </Grid>
                <Grid item>
                    <Chip variant='outlined' size='small' label={FLAG_STATUS_TITLE[flagTeam.flagStatusId]} />
                </Grid>
            </Grid>
            <DialogScanQrCode open={open} onClose={handleClose} onScan={handleScore} />
        </PageCard>
    )
}) /* =============================================================================================================== */

const FLAG_STATUS_TITLE = {
    [FLAG_STATUS.Stolen]: 'stolen',
    [FLAG_STATUS.Shamed]: 'shamed',
    [FLAG_STATUS.Glory]: 'glory',
}
