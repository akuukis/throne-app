import React from 'react'

import { CONFIG } from '@larts/api'
import { Alert, AlertTitle, Button, Grid } from '@material-ui/core'

import logo from 'assets/logo.svg'

import { createFC } from 'src/common'
import useConfig from 'src/common/useConfig'
import PageHeader from 'src/components/PageHeader'
import PageSection from 'src/components/PageSection'
import PageSectionPart from 'src/components/PageSectionPart'


interface IProps {
}


export default createFC(import.meta.url)<IProps>(function _({ children, theme }) {
    const emergencyShutdwown = useConfig(CONFIG.EmergencyShutdown, Number)

    if (!emergencyShutdwown) return <>{children}</>

    return (
        <Grid
            container
            sx={{ height: '100%', backgroundImage: `url(${logo})`, backgroundSize: 'contain', backgroundRepeat: 'no-repeat', backgroundPosition: 'center' }}
            direction='column'
            justifyContent='space-between'
            wrap='nowrap'
        >
            <Grid item flexBasis={0}>
                <PageHeader title='Throne VIII' />
            </Grid>
            <Grid item flexBasis={0}>
                <PageSection>
                    <PageSectionPart>
                        <Alert severity='error' sx={{ width: '100%' }}>
                            <AlertTitle>Emergency Shutdown</AlertTitle>
                            Right now GMs are fixing the App and it will be back online soon.
                            If this persists longer than 15 minutes, please contact GMs.
                        </Alert>
                    </PageSectionPart>
                </PageSection>
            </Grid>
            <Grid item flexBasis={0}>
                <PageSection>
                    <PageSectionPart>
                        <Button variant='contained' fullWidth href='tel:+37122118827'>
                            Call GM Kalvis
                        </Button>
                    </PageSectionPart>
                </PageSection>
            </Grid>
        </Grid>
    )
}) /* =============================================================================================================== */
