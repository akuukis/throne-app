import React from 'react'

import { Button } from '@material-ui/core'

import { createFC, useNavigateToHref } from 'src/common'
import IconPlayer from 'src/components/icons/IconPlayer'
import { ROUTE, makeRoute } from 'src/constants'


interface IProps {
    username: string
}


export default createFC(import.meta.url)<IProps>(function _({ children, theme, username }) {
    const navigateToHref = useNavigateToHref()

    return (
        <Button
            href={makeRoute(ROUTE.Profile)}
            onClick={navigateToHref}
            variant='contained'
            startIcon={<IconPlayer />}
        >
            {username}
        </Button>
    )
}) /* =============================================================================================================== */
