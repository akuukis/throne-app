import React from 'react'

import { QueryTeamBase, QueryUserName } from '@larts/api'
import { AppBar, Divider } from '@material-ui/core'

import { createFC, useQuery } from 'src/common'

import MyMenu from './MyMenu'
import MyToolbar from './MyToolbar'


interface IProps {
}


export default createFC(import.meta.url)<IProps>(function _({ children, theme }) {
    const queryUserName = useQuery(QueryUserName)
    const queryTeamBase = useQuery(QueryTeamBase)

    return (
        <AppBar sx={{ justifyContent: 'center', maxWidth: theme.breakpoints.values.sm, right: 'unset' }}>
            <MyMenu />
            <Divider flexItem orientation="vertical" />
            <MyToolbar
                teamName={queryTeamBase.data?.team?.title}
                username={queryUserName.data?.user?.name}
            />
        </AppBar>
    )
}) /* =============================================================================================================== */
