import React from 'react'
import { useCallback } from 'react'

import { ListItem, ListItemText } from '@material-ui/core'
import { useNavigate } from 'react-router'

import { createFC } from 'src/common'


interface IProps {
    href: string
    title: string
    onClick?: (e: React.MouseEvent<HTMLAnchorElement>) => void
}


export default createFC(import.meta.url)<IProps>(function _({ children, theme, ...props }) {
    const { title, href, onClick } = props
    const navigate = useNavigate()

    // useNavigateToHref doesn't work because ListItem doesn't apply onClick on the a element.
    const handleClick = useCallback((e: React.MouseEvent<HTMLAnchorElement>) => {
        e.preventDefault()
        navigate(href)
        onClick?.(e)
    }, [href])

    return (
        <ListItem
            component="a"
            button
            href={href}
            onClick={handleClick}
        >
            <ListItemText primary={title} />
        </ListItem>
    )
}) /* =============================================================================================================== */
