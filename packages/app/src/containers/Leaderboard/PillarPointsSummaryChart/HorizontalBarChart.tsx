import React from 'react'

import { QueryScorePillarSummary, TEAM_COLOR } from '@larts/api'
import { Box } from '@material-ui/core'
import { Bar, BarChart, ResponsiveContainer, XAxis, YAxis } from 'recharts'

import { createFC, getTheme } from 'src/common'


interface IProps {
    dataSorted: NonNullable<NonNullable<QueryScorePillarSummary.Response['pillar']>['allSeasons'][0]>['totalScores']
    teamId?: number
    target: number
}


const height = 100

export default createFC(import.meta.url)<IProps>(function _({ children, theme, dataSorted, teamId, target }) {

    // TODO: solve duplicate form transform here and at parent.
    const dataMap = dataSorted.reduce((o, datum) => ({ ...o, [datum.team?.id]: datum.score }), {}) as Record<number, number>
    return (
        <Box sx={{ borderStyle: 'solid', borderWidth: 1, overflow: 'hidden' }}>
            <Box sx={{ marginTop: '-2px', marginBottom: '-53px' }}>
                <ResponsiveContainer width='100%' height={height}>
                    <BarChart
                        data={[dataMap]}
                        layout="vertical"
                        margin={{ bottom: 0, left: 0, right: 0, top: 0 }}
                        barGap={0}
                        barCategoryGap={0}
                        barSize={height}
                        maxBarSize={height}
                    >
                        <XAxis hide type="number" domain={[0, target]} />
                        <YAxis hide type="category" />
                        {dataSorted.map(({ team }) => (
                            <Bar
                                key={team.id}
                                dataKey={team.id}
                                stackId="s"
                                label={{
                                    valueAccessor: (props: typeof dataMap) => props[team.id] === 0 ? '' : props[team.id],
                                    ...theme.typography.body2,
                                    fill: (teamId && team.id === teamId) ? getTheme(team.color as TEAM_COLOR).palette.primary.contrastText : undefined,
                                    fontWeight: (teamId && team.id === teamId) ? 'bold' : undefined,
                                } as any}
                                fill={getTheme(team.color as TEAM_COLOR).palette.primary[(teamId && team.id === teamId) ? 'main' : 'light']}
                            />
                        ))}
                    </BarChart>
                </ResponsiveContainer>
            </Box>
        </Box>
    )
}) /* =============================================================================================================== */
