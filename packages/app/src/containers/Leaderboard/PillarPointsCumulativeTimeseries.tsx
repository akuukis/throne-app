import React, { useMemo } from 'react'

import { CONFIG, PILLAR_REAL, QueryScorePillar, QueryTeamBase, TEAM_COLOR } from '@larts/api'
import { CircularProgress, Typography } from '@material-ui/core'
import { Area, AreaChart, CartesianGrid, ReferenceArea, ReferenceDot, ReferenceLine, ResponsiveContainer, XAxis, YAxis } from 'recharts'

import { createFC, getTheme, useQuery } from 'src/common'
import useConfig from 'src/common/useConfig'


interface IProps {
    pillarId: PILLAR_REAL
    seasonId: number
}


export default createFC(import.meta.url)<IProps>(function _({ children, theme, seasonId, pillarId }) {
    const seasonMin = useConfig(CONFIG.SeasonMin, Number) ?? 0
    const seasonMax = useConfig(CONFIG.SeasonMax, Number) ?? 0
    const seasonAvg = useConfig(CONFIG.SeasonAvg, Number) ?? 0

    const queryTeam = useQuery(QueryTeamBase)
    const team = queryTeam.data?.team

    const queryScore = useQuery(QueryScorePillar, { variables: { id: pillarId } })
    const pillarOfSeason = queryScore.data?.pillar?.allSeasons?.find(({ season }) => season.id === seasonId)
    const started = pillarOfSeason?.season.started
    const scores = pillarOfSeason?.scores

    const data = useMemo(() => {
        if (!started || !scores) return []

        let cumulativeScore = 0
        return scores
            .slice()
            .sort((a, b) => a.scored - b.scored)
            .map((score) => {
                cumulativeScore = cumulativeScore + score.reason?.amount
                return ({
                    elapsed: (score.scored - started),
                    total: cumulativeScore,
                })
            })
    }, [started, scores])

    if (!started || !pillarOfSeason || !scores || data.length === 0 || !seasonMin || !seasonMax || !seasonAvg) {
        return <CircularProgress sx={{ display: 'block', margin: 'auto' }} />
    }

    return (
        <>
            <ResponsiveContainer width="100%" height={200}>
                <AreaChart
                    data={data}
                    margin={{ bottom: 24, left: 8, right: 16, top: 0 }}
                >
                    <CartesianGrid strokeDasharray="3 3" />
                    <XAxis
                        type='number'
                        dataKey="elapsed"
                        height={10}
                        ticks={new Array(Math.ceil(seasonMax / 7.5) + 1).fill(null).map((_, i) => i * 7.5 * 60)}
                        domain={[0, seasonAvg*60]}
                        tickFormatter={(raw) => `${Math.floor(raw/60/60)}:${Math.floor(raw/60%60).toString().padStart(2, '0')}`}
                        tick={{
                            ...theme.typography.caption,
                        } as any}
                    />
                    <YAxis
                        domain={[0, Math.round(pillarOfSeason.target * 1.5)]}
                        ticks={[0, pillarOfSeason.target]}
                        tickFormatter={(raw) => raw === pillarOfSeason.target ? `target:\u00a0${raw}` : raw}
                        tick={{
                            ...theme.typography.caption,
                        } as any}
                    />
                    <Area
                        type="linear"
                        dataKey="total"
                        stackId="1"
                        stroke={theme.palette.text.primary}
                        strokeWidth={4}
                        fill={theme.palette.action.selected}
                    />
                    <ReferenceDot
                        x={data[data.length-1].elapsed}
                        y={data[data.length-1].total}
                        r={4}
                        fill={theme.palette.text.primary}
                    />
                    <ReferenceLine alwaysShow strokeDasharray="3 3" stroke={theme.palette.text.secondary} y={pillarOfSeason.target} />
                    <ReferenceLine alwaysShow strokeDasharray="3 3" stroke={theme.palette.text.secondary} x={seasonMin*60} />
                    <ReferenceLine alwaysShow strokeDasharray="3 3" stroke={theme.palette.text.secondary} x={seasonMax*60} />
                    <ReferenceArea alwaysShow fill={getTheme(team?.color as TEAM_COLOR | undefined).palette.primary.light} x1={seasonMin*60} x2={seasonMax*60} y1={pillarOfSeason.target} y2={pillarOfSeason.target*1.5} />
                    <ReferenceArea alwaysShow fill={getTheme(team?.color as TEAM_COLOR | undefined).palette.primary.light} x1={seasonMax*60} x2={(seasonMax+10)*60} y1={0} y2={pillarOfSeason.target*1.5} />
                </AreaChart>
            </ResponsiveContainer>
            <Typography paragraph variant='caption' sx={{ paddingLeft: 2 }}>
                Pillar is complete when total score reaches the marked zone.
            </Typography>
        </>
    )
}) /* =============================================================================================================== */
