import React, { useMemo } from 'react'

import { CONFIG, PILLAR_REAL, QueryScorePillar, QueryTeams, TEAM_ID } from '@larts/api'
import { CircularProgress } from '@material-ui/core'

import { createFC, useQuery } from 'src/common'
import useConfig from 'src/common/useConfig'

import MultiLineChart from './MultiLineChart'


interface IProps {
    pillarId: PILLAR_REAL
    seasonId: number
}


export default createFC(import.meta.url)<IProps>(function _({ children, theme, seasonId, pillarId }) {
    const seasonMax = useConfig(CONFIG.SeasonMax, Number) ?? 0

    const queryTeams = useQuery(QueryTeams)
    const teams = queryTeams.data?.teams

    const queryScore = useQuery(QueryScorePillar, { variables: { id: pillarId } })
    const pillarOfSeason = queryScore.data?.pillar?.allSeasons?.find(({ season }) => season.id === seasonId)
    const started = pillarOfSeason?.season.started
    const scores = pillarOfSeason?.scores

    const data = useMemo(() => {
        if (!started || !scores || !teams) return []

        const cumulativeScore = teams.reduce((obj, team) => ({ ...obj, [team.id]: 0 }), {}) as Record<TEAM_ID, number>
        return scores
            .slice()
            .sort((a, b) => a.scored - b.scored)
            .map((score) => {
                cumulativeScore[score.team.id] = cumulativeScore[score.team.id] + score.reason?.amount
                return ({
                    elapsed: (score.scored - started),
                    ...cumulativeScore,
                })
            })
    }, [started, scores, teams])

    if (data.length === 0) return <CircularProgress sx={{ display: 'block', margin: 'auto' }} />

    return (
        <MultiLineChart
            historyData={data}
            ticks={new Array(Math.ceil(seasonMax / 7.5) + 1).fill(null).map((_, i) => i * 7.5 * 60)}
            tickFormatter={(raw) => `${Math.floor(raw/60/60)}:${Math.floor(raw/60%60).toString().padStart(2, '0')}`}
            target={pillarOfSeason?.target}
        />
    )
}) /* =============================================================================================================== */
