import React from 'react'

import { QueryTeams, TEAM_COLOR, TEAM_ID } from '@larts/api'
import { alpha } from '@material-ui/core'
import { CartesianGrid, Line, LineChart, ReferenceDot, ReferenceLine, ResponsiveContainer, XAxis, YAxis } from 'recharts'

import { createFC, getTheme, recordValues, useQuery } from 'src/common'


interface IProps {
    historyData: (Partial<Record<TEAM_ID, number>> & {elapsed: number})[],  // TODO: remove Partial once we have full mock/real data.
    ticks: number[]
    target?: number
    tickFormatter?: (raw: number) => string
}


export default createFC(import.meta.url)<IProps>(function _({ children, theme, historyData, ticks, target, tickFormatter }) {
    const queryTeams = useQuery(QueryTeams)

    const scoreHistoryWithTotal = historyData.map(({ elapsed, ...teams }) => ({
        elapsed,
        ...teams,
        total: recordValues({ ...teams }).reduce((sum: number, team) => sum + (team ?? 0), 0),
    }))

    return (
        <ResponsiveContainer width="100%" height={300}>
            <LineChart
                data={scoreHistoryWithTotal}
                margin={{ bottom: 24, left: -8, right: 16, top: 8 }}
            >
                <CartesianGrid strokeDasharray="3 3" />
                <XAxis
                    type='number'
                    dataKey="elapsed"
                    height={10}
                    ticks={ticks}
                    domain={[target ? 0 : 1, ticks[ticks.length - 1]]}
                    tickFormatter={tickFormatter}
                    tick={{
                        ...theme.typography.caption,
                    } as any}
                />
                <YAxis
                    domain={[0, 'auto']}
                    tickCount={9}
                    tick={{
                        ...theme.typography.caption,
                    } as any}
                />
                {queryTeams.data?.teams.map((team) => (
                    <Line
                        key={team.id}
                        type="monotone"
                        dot={target ? false : undefined}
                        dataKey={team.id}
                        stroke={alpha(getTheme(team.color as TEAM_COLOR).palette.primary.main, 0.6)}
                        strokeWidth={4}
                    />
                ))}
                {target && queryTeams.data?.teams.map((team) => (
                    <ReferenceDot
                        key={team.id}
                        x={scoreHistoryWithTotal[scoreHistoryWithTotal.length-1].elapsed}
                        y={scoreHistoryWithTotal[scoreHistoryWithTotal.length-1][team.id]}
                        r={4}
                        fill={getTheme(team.color as TEAM_COLOR).palette.primary.main}
                    />
                ))}
                {target && <ReferenceLine alwaysShow strokeDasharray="3 3" stroke={theme.palette.text.secondary} y={target/(queryTeams.data?.teams.length ?? 1)} />}
            </LineChart>
        </ResponsiveContainer>
    )
}) /* =============================================================================================================== */
